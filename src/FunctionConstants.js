import { SECTION_IMAGES } from './imagesConstant.js';
import { GROUP_PRIVACY, QUESTION_VISIBILITY, QUESITON_STATUS } from './constants.js';

export const randomInt = (min, max) => {
    return min + Math.floor((max - min) * Math.random());
}

export const removeElementInArrayByValue = (arr, item) => {
    let index = arr.indexOf(item);

    if (index !== -1) {
        arr.splice(index, 1);
    }
}

export const getFontAwsome = (fieldId) => {
    if (fieldId === 0) {
        return "fas fa-globe-americas"
    }

    for (let i = 0; i < SECTION_IMAGES.length; ++i) {
        if (SECTION_IMAGES[i].field_id === fieldId) {
            return SECTION_IMAGES[i].fontAwesome;
        }
    }
}

export const getNameOfField = (fieldId) => {
    if (fieldId === 0) {
        return "Tất cả"
    }

    for (let i = 0; i < SECTION_IMAGES.length; ++i) {
        if (SECTION_IMAGES[i].field_id === fieldId) {
            return SECTION_IMAGES[i].title;
        }
    }
}

export const getImageOfField = (fieldId) => {
    for (let i = 0; i < SECTION_IMAGES.length; ++i) {
        if (SECTION_IMAGES[i].field_id === fieldId) {
            return SECTION_IMAGES[i].icon;
        }
    }
}

export const getPrivacyOfGroup = (privacyId) => {
    switch (privacyId) {
        case GROUP_PRIVACY.private:
            return 'PRIVATE';
        case GROUP_PRIVACY.protected:
            return 'PROTECTED';
        case GROUP_PRIVACY.public:
            return 'PUBLIC';
        default:
            return 'PRIVATE';
    }
}

export const getQuestionVisibility = (visibilityId) => {
    switch (visibilityId) {
        case QUESTION_VISIBILITY.OPEN:
            return 'Mở';
        case QUESTION_VISIBILITY.ONLY_ME:
            return 'Chỉ mình tôi';
        default:
            return 'Mở';
    }
}

export const getQuestionStatus = (statusId) => {
    switch (statusId) {
        case QUESITON_STATUS.OPEN:
            return 'Mở';
        case QUESITON_STATUS.RESOLVED:
            return 'Đã trả lời';
        case QUESITON_STATUS.CANCEL:
            return 'Đã hủy';
        default:
            return 'Mở';
    }
}