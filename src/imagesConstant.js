import { BASE_URL_IMAGE } from './constants.js';

export const SECTION_IMAGES = [
    {
        field_id: 1,
        icon: BASE_URL_IMAGE + '/icon/triangle.svg',
        title: 'Toán',
        type: 'math',
        fontAwesome: 'fas fa-calculator'
    },
    {
        field_id: 2,
        icon: BASE_URL_IMAGE + '/icon/blackboard.svg',
        title: 'Vật lý',
        type: 'physic',
        fontAwesome: 'fas fa-atom'
    },
    {

        field_id: 3,
        icon: BASE_URL_IMAGE + '/icon/atom.svg',
        title: 'Hóa học',
        type: 'chemistry',
        fontAwesome: 'fas fa-flask'
    },
    {
        field_id: 4,
        icon: BASE_URL_IMAGE + '/icon/gdp.svg',
        title: 'Kinh tế',
        type: 'economic',
        fontAwesome: 'fas fa-search-dollar'
    },
    {

        field_id: 5,
        icon: BASE_URL_IMAGE + '/icon/double-decker.svg',
        title: 'Tiếng anh',
        type: 'english',
        fontAwesome: 'fas fa-globe'
    },
    {
        field_id: 6,
        icon: BASE_URL_IMAGE + '/icon/worldwide.svg',
        title: 'Ngôn ngữ',
        type: 'language',
        fontAwesome: 'fas fa-language'
    },
    {
        field_id: 7,
        icon: BASE_URL_IMAGE + '/icon/marketing-strategy.svg',
        title: 'Back end',
        type: 'backend',
        fontAwesome: 'fas fa-code-branch'
    },
    {
        field_id: 8,
        icon: BASE_URL_IMAGE + '/icon/browser.svg',
        title: 'Front end',
        type: 'frontend',
        fontAwesome: 'fas fa-code'
    },
    {
        field_id: 9,
        icon: BASE_URL_IMAGE + '/icon/star.svg',
        title: 'Khác',
        type: 'other',
        fontAwesome: 'fas fa-rss'
    }
];

export const ICONS = [
    {
        name: 'atom',
        path: '/icon/atom.svg'
    },
    {
        name: 'blackboard',
        path: '/icon/blackboard.svg'
    },
    {
        name: 'browser',
        path: '/icon/browser.svg'
    },
    {
        name: 'double-decker',
        path: '/icon/double-decker.svg'
    },
    {
        name: 'facebook',
        path: '/icon/facebook.svg'
    },
    {
        name: 'gdp',
        path: '/icon/gdp.svg'
    }, {
        name: 'gears',
        path: '/icon/gears.svg'
    },
    {
        name: 'iconfinder_bookshelf',
        path: '/icon/iconfinder_bookshelf.svg'
    },
    {
        name: 'icons8-tag-window-64',
        path: '/icon/icons8-tag-window-64.png'
    }, {
        name: 'instagram',
        path: '/icon/instagram.svg'
    },
    {
        name: 'marketing-strategy',
        path: '/icon/marketing-strategy.svg'
    },
    {
        name: 'math-icon',
        path: '/icon/math-icon.png'
    }, {
        name: 'maxresdefault',
        path: '/icon/maxresdefault.jpg'
    },
    {
        name: 'star',
        path: '/icon/star.svg'
    },
    {
        name: 'team',
        path: '/icon/team.svg'
    }, {
        name: 'triangle',
        path: '/icon/triangle.svg'
    },
    {
        name: 'worldwide',
        path: '/icon/worldwide.svg'
    },
]