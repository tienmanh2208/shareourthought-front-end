import React from "react";
import SignIn from "./signin";
import Register from "./register";

const TYPE_AUTH = {
    LOGIN: "login",
    LOGOUT: "logout",
    FORGET_PASSWORD: "forgetpassword",
    REGISTER: "register"
};

class Auth extends React.Component {
    render() {
        return (
            <div style={{height: '100%'}}>
                {this.getComponent(this.props.type)}
            </div>
        );
    }

    getComponent(type) {
        switch (type) {
            case TYPE_AUTH.LOGIN:
                return <SignIn />;
            case TYPE_AUTH.REGISTER:
                return <Register />;
            default:
                return <SignIn />;
        }
    }
}

export default Auth;