import React from "react";
// import ParticlesBg from "particles-bg";
import './css/auth.css';
import { connect } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import { BASE_URL_IMAGE, USER_SERVICE_DOMAIN, API_SIGN_UP } from '../../constants.js';
import FormLoading from "../General/FormLoading";
import FormNotification from "../General/FormNotification";
import Axios from "axios";

class RegisterComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            userName: '',
            dateOfBirth: '',
            mail: '',
            password: '',
            confirmationPassword: '',
            invitedUser: '',
            isLoading: false,
            isDisplayForm: false,
            formContent: {
                title: '',
                content: '',
                callBack: '',
            },
            goToLogIn: false,
        };
        this.changeGoToLogin = this.changeGoToLogin.bind(this);
        this.closeForm = this.closeForm.bind(this);
        this.callRegister = this.callRegister.bind(this);
        this.displayForm = this.displayForm.bind(this);
    }

    render() {
        if (this.state.goToLogIn) {
            return <Redirect to={'/login'} />;
        }

        return (
            <div className={'tm-rt'}>
                {/*<ParticlesBg type="circle" bg={true}/>*/}
                <div className={'auth-background'} style={{ backgroundImage: 'url("' + BASE_URL_IMAGE + '/background.png")' }} />
                <div className={'tm-rt-frame'}>
                    <div className={'tm-rt-top'}>
                        <div className={'tm-text-rt-logo'}>Share Knowledge</div>
                        <div className={'tm-text-rt-deco'} />
                        <div className={'tm-text-rt-sublogo tm-textalign-right'}>CHÀO MỪNG BẠN ĐẾN VỚI CỘNG ĐỒNG CHIA SẺ KIẾN THỨC</div>
                    </div>
                    <div className={'tm-rt-bottom'}>
                        <div className={'tm-rt-form tm-flex-column'}>
                            <div className={'tm-rt-input'}>
                                <div className={'tm-rt-ip-title'}>Tên</div>
                                <input value={this.state.firstName} onChange={this.handleInputFirstName.bind(this)} placeholder={'Tên'} className={'tm-rt-ip tm-width-70'} />
                            </div>
                            <div className={'tm-rt-input'}>
                                <div className={'tm-rt-ip-title'}>Họ</div>
                                <input value={this.state.lastName} onChange={this.handleInputLastName.bind(this)} placeholder={'Họ và tên đệm'} className={'tm-rt-ip tm-width-70'} />
                            </div>
                            <div className={'tm-rt-input'}>
                                <div className={'tm-rt-ip-title'}>Username</div>
                                <input value={this.state.userName} onChange={this.handleInputUserName.bind(this)} placeholder={'Username của bạn'} className={'tm-rt-ip tm-width-70'} />
                            </div>
                            <div className={'tm-rt-input'}>
                                <div className={'tm-rt-ip-title'}>Date of birth</div>
                                <input value={this.state.dateOfBirth} onChange={this.handleInputDateOfBirth.bind(this)} placeholder={'Ngày sinh'} className={'tm-rt-ip tm-width-70'} type='date' />
                            </div>
                            <div className={'tm-rt-input'}>
                                <div className={'tm-rt-ip-title'}>Mail</div>
                                <input value={this.state.mail} onChange={this.handleInputMail.bind(this)} placeholder={'yourmail@example.com'} className={'tm-rt-ip  tm-width-70'} />
                            </div>
                            <div className={'tm-rt-input'}>
                                <div className={'tm-rt-ip-title'}>Password</div>
                                <input value={this.state.password} onChange={this.handleInputPassword.bind(this)} placeholder={'Mật khẩu'} className={'tm-rt-ip tm-width-70'} type={'password'} />
                            </div>
                            <div className={'tm-rt-input'}>
                                <div className={'tm-rt-ip-title'}>Confirm Password</div>
                                <input value={this.state.confirmationPassword} onChange={this.handleInputConfirmationPassword.bind(this)} placeholder={'Nhập lại mật khẩu'} className={'tm-rt-ip tm-width-70'} type={'password'} />
                            </div>
                            <div className={'tm-rt-input'}>
                                <div className={'tm-rt-ip-title'}>Mã mời</div>
                                <input value={this.state.invitedUser} onChange={this.handleInputInvitedUser.bind(this)} placeholder={'Username của người đã mời bạn tham gia'} className={'tm-rt-ip tm-width-70'} />
                            </div>
                            <div className={'tm-flex-row-center tm-float-clear'}>
                                <button onClick={this.handleRegister.bind(this)} className={'tm-btn tm-margin-20 tm-btn-green'}>Register</button>
                            </div>
                            <Link to={'/login'} className={'tm-si-register'}>Already had account? Sign in</Link>
                            {this.state.isLoading ? <FormLoading /> : ''}
                            {this.state.isDisplayForm ? <FormNotification title={this.state.formContent.title} content={this.state.formContent.content} action={this.state.formContent.callBack} /> : ''}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    handleRegister() {
        this.setState({
            isLoading: true
        })

        this.callRegister(this.displayForm);
    }

    callRegister(callBack) {
        Axios.post(USER_SERVICE_DOMAIN + API_SIGN_UP, {
            username: this.state.userName,
            first_name: this.state.firstName,
            last_name: this.state.lastName,
            email: this.state.mail,
            date_of_birth: this.state.dateOfBirth,
            password: this.state.password,
            password_confirmation: this.state.confirmationPassword,
            invited_user: this.state.invitedUser
        }).then(res => {
            if (res.data.code === 201) {
                callBack('Thông báo', res.data.message, 'GoToLogin');
            } else {
                callBack('Chú ý', res.data.message, 'CloseForm');
            }
        })
    }

    displayForm(title, content, type) {
        if (type === 'GoToLogin') {
            this.setState({
                isLoading: false,
                isDisplayForm: true,
                formContent: {
                    title: title,
                    content: content,
                    callBack: this.changeGoToLogin,
                },
            });
        } else {
            this.setState({
                isLoading: false,
                isDisplayForm: true,
                formContent: {
                    title: title,
                    content: content,
                    callBack: this.closeForm,
                },
            });
        }
    }

    changeGoToLogin() {
        this.setState({
            goToLogIn: true,
        });
    }

    closeForm() {
        this.setState({
            isDisplayForm: false
        });
    }

    handleInputFirstName(event) {
        this.setState({
            firstName: event.target.value
        })
    }

    handleInputLastName(event) {
        this.setState({
            lastName: event.target.value
        })
    }

    handleInputUserName(event) {
        this.setState({
            userName: event.target.value
        })
    }

    handleInputDateOfBirth(event) {
        this.setState({
            dateOfBirth: event.target.value
        })
    }

    handleInputMail(event) {
        this.setState({
            mail: event.target.value
        })
    }

    handleInputPassword(event) {
        this.setState({
            password: event.target.value
        })
    }

    handleInputConfirmationPassword(event) {
        this.setState({
            confirmationPassword: event.target.value
        })
    }

    handleInputInvitedUser = (event) => {
        this.setState({
            invitedUser: event.target.value
        })
    }
}

const mapStateToProps = (state) => {
    return {
        authenticated: state.authenticated,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const Register = connect(mapStateToProps, mapDispatchToProps)(RegisterComponent);

export default Register;