import React from 'react';
// import ParticlesBg from 'particles-bg';
import './css/auth.css';
import Axios from 'axios';
import { connect } from 'react-redux';
import { LOGIN } from '../../Reducers/actions';
import { PAGE_NAME } from '../../Reducers/variables';
import { Link, Redirect } from 'react-router-dom';
import { BASE_URL_IMAGE, USER_SERVICE_DOMAIN, API_LOG_IN, API_GET_BASIC_INFO } from '../../constants.js';
import FormLoading from '../General/FormLoading';
import FormNotification from '../General/FormNotification';

class Signin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input_email: '',
            input_password: '',
            isLoading: false,
            isDisplayForm: false,
            formContent: {
                title: '',
                content: ''
            },
            isAdmin: false,
        };
        this.displayForm = this.displayForm.bind(this);
        this.signIn = this.signIn.bind(this);
        this.checkAuth = this.checkAuth.bind(this);
        this.updateAdminRole = this.updateAdminRole.bind(this);
    }

    handleInputEmail(event) {
        this.setState({
            input_email: event.target.value,
        })
    }

    handleInputPassword(event) {
        this.setState({
            input_password: event.target.value,
        })
    }

    handleRedirectToRegister() {
        this.props.changePage(PAGE_NAME.register);
    }

    handleSignIn() {
        this.setState({
            isLoading: true,
        });

        this.signIn(this.state.input_email, this.state.input_password, this.props.changeAuth, this.displayForm, this.updateAdminRole);
    }

    displayForm(title, content) {
        this.setState({
            isLoading: false,
            isDisplayForm: true,
            formContent: {
                title: title,
                content: content
            }
        });
    }

    updateAdminRole = () => {
        this.setState({
            isAdmin: true,
        })
    }

    signIn(email, password, callback, callBackWhenFail, updateAdminRole) {
        Axios.post(USER_SERVICE_DOMAIN + API_LOG_IN, {
            email: email,
            password: password,
        }).then(res => {
            if (res.data.code === 200) {
                localStorage.setItem('sot-token', res.data.data.token);
                if (res.data.data.role === 2) {
                    updateAdminRole();
                }
                callback(LOGIN);
            } else if (res.data.code === 403) {
                callBackWhenFail('Warning', 'Email or password is incorrect');
            } else {
                callBackWhenFail('Warning', res.data.message);
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail('Warning', 'Server error');
        });
    }

    closeForm() {
        this.setState({
            isDisplayForm: false,
            formContent: {
                title: '',
                content: ''
            }
        });
    }

    checkAuth() {
        Axios.get(USER_SERVICE_DOMAIN + API_GET_BASIC_INFO, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                this.props.changeAuth(LOGIN);
            }
        }).catch(error => {
            localStorage.removeItem('sot-token');
        });
    }

    render() {
        if (this.state.isAdmin) {
            return <Redirect to={'/admin'} />;
        }

        if (this.props.authenticated) {
            return <Redirect to={'/dashboard'} />;
        }

        if (localStorage.getItem('sot-token') !== null) {
            this.props.changeAuth(LOGIN);
            // this.checkAuth();
        }

        return (
            <div className={'tm-signin'}>
                {/*<ParticlesBg type='circle' bg={true}/>*/}
                <div className={'auth-background'} style={{ backgroundImage: "url('" + BASE_URL_IMAGE + "/background.png')" }} />
                <div className={'tm-signin-frame tm-flex-row-spacebtw-nowrap'}>
                    <div className={'tm-signin-left'}>
                        <div className={'tm-text-si-logo'}>Share<br className={'tm-text-si-br'} />Knowledge</div>
                        <div className={'tm-text-si-deco'} />
                        <div className={'tm-text-si-sublogo'}>CỘNG ĐỒNG CHIA SẺ KIẾN THỨC</div>
                    </div>
                    <div className={'tm-signin-right'}>
                        <div className={'tm-signin-form'}>
                            <div className={'tm-signin-dn'}>Đăng nhập</div>
                            <div className={'tm-signin-input'}>
                                <div className={'tm-si-ip-title'}>Email</div>
                                <input value={this.state.input_email} className={'tm-si-ip'}
                                    onChange={this.handleInputEmail.bind(this)} />
                            </div>
                            <div className={'tm-signin-input'}>
                                <div className={'tm-si-ip-title'}>Password</div>
                                <input value={this.state.input_password} className={'tm-si-ip'}
                                    onChange={this.handleInputPassword.bind(this)} type={'password'} />
                            </div>
                            <div className={'tm-flex-row-center'}>
                                <button onClick={this.handleSignIn.bind(this)}
                                    className={'tm-btn tm-margin-10 tm-btn-gray'}>Sign in
                                </button>
                            </div>
                            <Link to={'/register'} className={'tm-si-register'}>
                                Doesn't have account? Register now
                            </Link>
                        </div>
                    </div>
                </div>
                {this.state.isLoading ? <FormLoading /> : ""}
                {this.state.isDisplayForm ? <FormNotification title={this.state.formContent.title} content={this.state.formContent.content} action={this.closeForm.bind(this)} /> : ''}
            </div>
        );
    }
}

const changeAuthAction = (action) => {
    return {
        type: action
    }
};

const mapStateToProps = (state) => {
    return {
        authenticated: state.authenticated,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        changeAuth: (action) => {
            dispatch(changeAuthAction(action))
        }
    }
};

const SignIn = connect(mapStateToProps, mapDispatchToProps)(Signin);

export default SignIn;
