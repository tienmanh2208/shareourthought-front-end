import React from 'react';
import { BASE_URL_IMAGE } from '../../../constants';

class PersonalJoinedGroup extends React.Component {
    render() {
        return (
            <div className={'tm-pyg-frame'}>
            <div className={'tm-pyg-setting-frame'}>
                <img className={'tm-pyg-img-avatar'} src={BASE_URL_IMAGE + '/icon/atom.svg'} alt={'Avatar group'} />
            </div>
            <div className={'tm-pyg-info-frame'}>
                <div className={'tm-pyg-groupname'}>Group name</div>
                <div className={'tm-pyg-bonusinfo'}>
                    <div className={'tm-pyg-creator'}><i class="fas fa-user-tie"></i>  Creator: <span className={'tm-pjg-creator-name'}>Phạm Phương Anh</span></div>
                    <div className={'tm-pyg-privacy'}><i class="fas fa-unlock-alt"></i> Protected</div>
                </div>
                <div className={'tm-pyg-description'}>This is description</div>
            </div>
        </div>
        )
    }
}

export default PersonalJoinedGroup;