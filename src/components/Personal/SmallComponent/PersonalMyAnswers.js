import React from 'react';
import MyAnswer from '../../General/MyAnswer';

class PersonalMyAnswers extends React.Component {
    render() {
        let answers = [];

        for (let i = 0; i < 5; ++i) {
            answers.push(<MyAnswer keyIndex={i} data={responseData}/>)
        }

        return (
            <div style={{ margin: '10px 0 20px 0', padding: '0 50px'}}>
                <div className={'tm-group-area-title'}>Câu trả lời của bạn</div>
                {answers}
            </div>
        )
    }
}

export default PersonalMyAnswers;

let responseData = {
    question: {
        title: 'Làm thế nào để hết béo',
        content: 'Mình ăn lắm quá, không kiềm chế được. Suốt ngày chỉ có ăn và ăn. Mỗi ngày mình hốc 3 hộp sữa chua, 4 quả chuối, 5 quả đu đủ, 5 bát cơm, và hốc cả người yêu của mình nữa. Vậy cho mình hỏi làm sao để hết béo vậyyyyyy',
        userName: 'Phạm Hoàng Hải',
        postedDate: '2020-03-05',
        upvote: 568,
    },
    answer: {
        content: 'Bạn không thể nào gầy được đâu, cứ ăn tiếp đi. Chẳng có ai có thể cứu được bạn nữa rồi. Ăn lắm vào, :)',
        status: 1,
        viewed: 1
    }
};