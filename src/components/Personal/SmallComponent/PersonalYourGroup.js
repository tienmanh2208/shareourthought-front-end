import React from 'react';
import { BASE_URL_IMAGE } from '../../../constants';

class PersonalYourGroup extends React.Component {
    render() {
        return (
            <div className={'tm-pyg-frame'}>
                <div className={'tm-pyg-setting-frame'}>
                    <img className={'tm-pyg-img-avatar'} src={BASE_URL_IMAGE + '/icon/instagram.svg'} alt={'Avatar group'} />
                    <button className={'tm-btn tm-btn-warning'} style={{width: '80%'}}>Setting</button>
                </div>
                <div className={'tm-pyg-info-frame'}>
                    <div className={'tm-pyg-groupname'}>Group name</div>
                    <div className={'tm-pyg-bonusinfo'}>
                        <div className={'tm-pyg-members'}><i class="fas fa-user-friends"></i> 255 Members</div>
                        <div className={'tm-pyg-privacy'}><i class="fas fa-unlock-alt"></i> Protected</div>
                        <div className={'tm-pyg-question'}><i class="far fa-question-circle"></i> 21 Questions</div>
                    </div>
                    <div className={'tm-pyg-description'}>This is description</div>
                </div>
            </div>
        )
    }
}

export default PersonalYourGroup;