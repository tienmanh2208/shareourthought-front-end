import React from 'react';
import LineChart from '../../Chart/LineChart';
import ColumnChart from '../../Chart/ColumnChart';

class PersonalChartDashboard extends React.Component {
    render() {
        return (
            <div className={'tm-flex-row-spacebtw-nowrap'} style={{ padding: '20px 50px' }}>
                <div className={'tm-personal-dashboard-chart'}>
                    <LineChart dataToDrawLineChart={dataToDrawLineChart} />
                </div>
                <div className={'tm-personal-dashboard-chart'}>
                    <ColumnChart dataToDraw={dataToDrawColumnChart} />
                </div>
            </div>
        )
    }
}

export default PersonalChartDashboard;

const dataToDrawColumnChart = {
    titleChart: 'Số câu trả lời theo tháng',
    dataToDraw: [
        { label: "Apple", y: randomInt(10, 50) },
        { label: "Orange", y: randomInt(10, 50) },
        { label: "Banana", y: randomInt(10, 50) },
        { label: "Mango", y: randomInt(10, 50) },
        { label: "Grape", y: randomInt(10, 50) }
    ]
};

const dataToDrawLineChart = [
    {
        "x": new Date(1483228800000),
        "y": randomInt(100, 10000)
    },
    {
        "x": new Date(1485907200000),
        "y": randomInt(100, 10000)
    },
    {
        "x": new Date(1488326400000),
        "y": randomInt(100, 10000)
    },
    {
        "x": new Date(1491004800000),
        "y": randomInt(100, 10000)
    },
    {
        "x": new Date(1493596800000),
        "y": randomInt(100, 10000)
    },
    {
        "x": new Date(1496275200000),
        "y": randomInt(100, 10000)
    },
    {
        "x": new Date(1498867200000),
        "y": randomInt(100, 10000)
    },
    {
        "x": new Date(1501545600000),
        "y": randomInt(100, 10000)
    },
    {
        "x": new Date(1504224000000),
        "y": randomInt(100, 10000)
    },
    {
        "x": new Date(1506816000000),
        "y": randomInt(100, 10000)
    },
    {
        "x": new Date(1509494400000),
        "y": randomInt(100, 10000)
    },
    {
        "x": new Date(1512086400000),
        "y": randomInt(100, 10000)
    }
]

function randomInt(min, max) {
    return min + Math.floor((max - min) * Math.random());
}