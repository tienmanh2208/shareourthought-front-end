import React from 'react'
import Axios from 'axios';
import { USER_SERVICE_DOMAIN, API_BUY_COIN, API_GET_COIN_INFO } from '../../../constants';

class PersonalCoinInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputCoin: '',
            coin_remain: 0,
            total_income: 0,
            total_outcome: 0,
            total_held_coin: 0,
            isLoading: false
        }
        this.callToBuyCoin = this.callToBuyCoin.bind(this);
        this.callToGetCoinInfo = this.callToGetCoinInfo.bind(this);
        this.displayWarning = this.displayWarning.bind(this);
    }

    render() {
        return (
            <div className={'tm-flex-row-spacebtw-nowrap'}>
                <div className={'tm-flex-column-center'} style={{ width: '45%' }}>
                    <div className={'tm-personal-coininfo-title'}>Số coin của bạn</div>
                    <div className={'tm-personal-coininfo-chart'}>
                        <div className={'tm-personal-coininfo-amount'}>{this.state.coin_remain}</div>
                        <div className={'tm-personal-coininfo-detail tm-flex-column-spacebtw'}>
                            {/* <div className={'tm-personal-coininfo-subdetail'} style={{backgroundColor: '#ffcc6f'}}> */}
                            <div className={'tm-personal-coininfo-subdetail'}>
                                <i class="far fa-arrow-alt-circle-down tm-personal-ci-icon-income"></i>
                                <div className={'tm-personal-coininfo-income'}>{this.state.total_income}</div>
                            </div>
                            {/* <div className={'tm-personal-coininfo-subdetail'} style={{backgroundColor: '#b18cd6'}}> */}
                            <div className={'tm-personal-coininfo-subdetail'}>
                                <i class="far fa-arrow-alt-circle-up tm-personal-ci-icon-outcome"></i>
                                <div className={'tm-personal-coininfo-outcome'}>{this.state.total_outcome}</div>
                            </div>
                            <div className={'tm-personal-coininfo-subdetail'}>
                                <i class="far fa-arrow-alt-circle-up tm-personal-ci-icon-outcome"></i>
                                <div className={'tm-personal-coininfo-outcome'}>{this.state.total_held_coin}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={'tm-flex-column-center'} style={{ width: '45%' }}>
                    <div className={'tm-personal-coininfo-title'}>Nạp coin</div>
                    <input value={this.state.inputCoin} onChange={this.handleInputCoin.bind(this)} className={'tm-personal-coininfo-input'} type={'number'} placeholder={'Số coin muốn nạp'} />
                    <div className={'tm-personal-coininfo-convert'}>{this.state.inputCoin}</div>
                    <button onClick={this.handleBuyCoin.bind(this)} className={'tm-btn tm-btn-success'}>Xác nhận</button>
                </div>
            </div>
        )
    }

    componentDidMount = () => {
        this.callToGetCoinInfo(this.updateCoinInfo, this.displayWarning);
    }

    callToGetCoinInfo = (callBack, callBackWhenFail) => {
        Axios.get(USER_SERVICE_DOMAIN + API_GET_COIN_INFO, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                callBack(res.data.data);
            } else {
                callBackWhenFail();
            }
        }).catch(error => {
            callBackWhenFail();
        })
    }

    handleBuyCoin = () => {
        this.callToBuyCoin(this.triggerUpdateCoinInfo, this.displayWarning);
    }

    callToBuyCoin = (callBack, callBackWhenFail) => {
        Axios.post(USER_SERVICE_DOMAIN + API_BUY_COIN, {
            coin: this.state.inputCoin
        }, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 203) {
                callBack();
            } else {
                callBackWhenFail();
            }
        }).catch(error => {
            callBackWhenFail();
        })
    }

    triggerUpdateCoinInfo = () => {
        this.callToGetCoinInfo(this.updateCoinInfo, this.displayWarning)
    }

    displayWarning = () => {
        console.log('error');
    }

    updateCoinInfo = (data) => {
        this.setState({
            inputCoin: '',
            coin_remain: data.coin_remain,
            total_income: data.total_income,
            total_outcome: data.total_outcome,
            total_held_coin: data.total_held_coin
        })
    }

    handleInputCoin = (event) => {
        this.setState({
            inputCoin: event.target.value
        })
    }
}

export default PersonalCoinInfo;
