import React from 'react';
import { ACTION_TRANSACTION_USER } from '../../../constants';

class PersonalHistoryTransaction extends React.Component {
    render() {
        return (
            <div className={'tm-pcm-hisTransaction-frame'}>
                <i class="far fa-arrow-alt-circle-down tm-pcm-ht-icon"></i>
                <div className={'tm-pcm-ht-reason'}>{this.props.data.reason}</div>
                <div className={'tm-pcm-ht-amount'}>{this.props.data.amount}</div>
                <div className={'tm-pcm-ht-reference'}>
                    {this.props.data.action === ACTION_TRANSACTION_USER.receiving ? 'Received from' : 'Spent to'}
                    <span> {this.props.data.user_name}</span>
                </div>
                <div className={'tm-pcm-ht-time'}>{this.props.data.time}</div>
            </div>
        )
    }
}

export default PersonalHistoryTransaction;

// Template data
// const data = {
//     reason: '',
//     amount: -40000,
//     action: ACTION_TRANSACTION_USER.receiving,
//     user_name: '',
//     time: ''
// }