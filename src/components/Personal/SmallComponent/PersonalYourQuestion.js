import React from 'react';
import Question from '../../Dashboard/Question';

class PersonalYourQuestion extends React.Component {
    render() {
        let content = [];

        for (let i = 0; i < 6; ++i) {
            content.push(<Question keyIndex={i} questionInfo={questionInfo} style={{ width: '30%', margin: '10px' }} />)
        }

        return (
            <div style={{ margin: '10px 0 20px 0', padding: '0 50px'}}>
                <div className={'tm-group-area-title'}>Câu hỏi mới nhất</div>
                <div className={'tm-flex-row-spacebtw-wrap'}>
                    {content}
                </div>
            </div>
        )
    }
}

export default PersonalYourQuestion;

const questionInfo = {
    "_id": "5ec8ae112c539a3b0d473dc3",
    "user_id": 1,
    "price": 5000,
    "visibility": 1,
    "status": 1,
    "upvotes": 0,
    "downvotes": 0,
    "field_id": 1,
    "group": {
        "group_id": null,
        "group_section_id": null
    },
    "tags": [
        "Math"
    ],
    "comments": [],
    "question_content": {
        "title": "Cách giải phương trình bậc 4 trùng phương",
        "content": "<p>Cách giải tổng quát phương trình bậc 4 trùng phương là gì ?</p>"
    },
    "updated_at": "2020-05-23T05:01:05.637000Z",
    "created_at": "2020-05-23T05:01:05.637000Z"
};