import React from 'react';
import { Link } from 'react-router-dom';
import { PERSONAL_CATEGORIES } from '../../../constants.js';

class PersonalCategory extends React.Component {
    constructor(props) {
        super(props);
        this.checkIsActive = this.checkIsActive.bind(this);
    }

    checkIsActive = (categoryName) => {
        if (this.props.categoryName === categoryName) {
            return true;
        }

        return false;
    }

    render() {
        return (
            <div className={'tm-personal-category-frame'}>
                <Link to={'/personal/dashboard'}
                    className={this.checkIsActive(PERSONAL_CATEGORIES.dashboard) ? 'tm-personal-category-active' : 'tm-personal-category-nonactive'}>
                    Dashboard
                </Link>
                <Link to={'/personal/coin-management'}
                    className={this.checkIsActive(PERSONAL_CATEGORIES.coinManagement) ? 'tm-personal-category-active' : 'tm-personal-category-nonactive'}>
                    Coin management
                </Link>
                <Link to={'/personal/question-and-answer'}
                    className={this.checkIsActive(PERSONAL_CATEGORIES.questionAndAnswer) ? 'tm-personal-category-active' : 'tm-personal-category-nonactive'}>
                    Câu hỏi và trả lời
                </Link>
                <Link to={'/personal/group-management'}
                    className={this.checkIsActive(PERSONAL_CATEGORIES.groupManagement) ? 'tm-personal-category-active' : 'tm-personal-category-nonactive'}>
                    Group management
                </Link>
                <Link to={'/personal/edit'}
                    className={this.checkIsActive(PERSONAL_CATEGORIES.editInfo) ? 'tm-personal-category-active' : 'tm-personal-category-nonactive'}>
                    Edit info
                </Link>
            </div>
        )
    }
}

export default PersonalCategory;