import React from 'react';
import PersonalHeader from './PersonalHeader';
import PersonalContent from './PersonalContent';
import Footer from '../Dashboard/Footer';
import '../../css/personal.css';
import { useParams } from 'react-router';

class Personal extends React.Component {
    render() {
        return (
            <div>
                <PersonalHeader />
                <GetCategoryName />
                <Footer />
            </div>
        )
    }
}

export default Personal;

let GetCategoryName = () => {
    let { categoryName } = useParams();
    return <PersonalContent categoryName={categoryName} />
}