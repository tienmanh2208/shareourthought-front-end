import React from 'react';
import PersonalYourQuestion from './SmallComponent/PersonalYourQuestion';
import PersonalMyAnswers from './SmallComponent/PersonalMyAnswers';

class PersonalQuestionAndAnswer extends React.Component {
    render() {
        return (
            <div>
                <PersonalYourQuestion />
                <PersonalMyAnswers />
            </div>
        )
    }
}

export default PersonalQuestionAndAnswer;