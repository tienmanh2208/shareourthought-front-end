import React from 'react';
import PersonalCoinInfo from './SmallComponent/PersonalCoinInfo';
import { randomInt } from '../../FunctionConstants';
import ColumnChart from '../Chart/ColumnChart';
import StackedColumnChart from '../Chart/StackedColumnChart';
import PersonalHistoryTransaction from './SmallComponent/PersonalHistoryTransaction';
import { ACTION_TRANSACTION_USER } from '../../constants';

class PersonalCoinManagement extends React.Component {
    render() {
        let historyTransactions = historyTransactionData.map((data) => {
            return <PersonalHistoryTransaction data={data} />
        });

        // for (let i = 0; i < 10; ++i) {
        //     historyTransactions.push(<PersonalHistoryTransaction />);
        // }

        return (
            <div style={{ padding: '20px 50px' }}>
                <PersonalCoinInfo />
                <div style={{ textAlign: 'center', fontSize: '30px', marginTop: '30px' }}>Thống kê</div>
                <div className={'tm-psn-cm-chart-frame'}>
                    <div className={'tm-psn-cm-chart'}>
                        <StackedColumnChart dataToDraw={dataToDrawCoinTransaction} />
                    </div>
                    <div className={'tm-psn-cm-chart'}>
                        <ColumnChart dataToDraw={dataToDrawSpendingCoins} />
                    </div>
                </div>
                <div style={{ textAlign: 'center', fontSize: '30px', margin: '30px 0 20px 0' }}>History Transactions</div>
                {historyTransactions}
            </div>
        )
    }
}

export default PersonalCoinManagement;

const historyTransactionData = [
    {
        reason: 'Nhận tiền từ câu trả lời đúng',
        amount: 150000,
        action: ACTION_TRANSACTION_USER.receiving,
        user_name: 'Phạm Văn Trung',
        time: '2020-04-01 20:00:00'
    },
    {
        reason: 'Nhận tiền từ câu trả lời đúng',
        amount: 230000,
        action: ACTION_TRANSACTION_USER.receiving,
        user_name: 'Nguyen Thu Huong',
        time: '2020-04-01 20:00:00'
    },
    {
        reason: 'Mat tien do dat cau hoi',
        amount: -40000,
        action: ACTION_TRANSACTION_USER.spending,
        user_name: 'Nguyen Truong Son',
        time: '2020-04-01 20:00:00'
    },
    {
        reason: 'Nhận tiền từ câu hỏi',
        amount: 20000,
        action: ACTION_TRANSACTION_USER.receiving,
        user_name: 'Vu Quang Trung',
        time: '2020-04-01 20:00:00'
    },
];

const dataToDrawSpendingCoins = {
    titleChart: 'Spending coins',
    dataToDraw: [
        { label: "Tháng 1", y: randomInt(10, 50) },
        { label: "Tháng 2", y: randomInt(10, 50) },
        { label: "Tháng 3", y: randomInt(10, 50) },
        { label: "Tháng 4", y: randomInt(10, 50) },
        { label: "Tháng 5", y: randomInt(10, 50) }
    ]
};

const dataToDrawCoinTransaction = {
    titleChart: 'Coin transactions',
    unit: {
        title: 'Coin',
        prefix: '',
        subfix: '',
    },
    data: [
        {
            type: "stackedColumn",
            name: "Receiving Coins",
            showInLegend: true,
            yValueFormatString: "",
            dataPoints: [
                { label: "Tháng 1", y: randomInt(10, 50) },
                { label: "Tháng 2", y: randomInt(10, 50) },
                { label: "Tháng 3", y: randomInt(10, 50) },
                { label: "Tháng 4", y: randomInt(10, 50) },
                { label: "Tháng 5", y: randomInt(10, 50) }
            ]
        },
        {
            type: "stackedColumn",
            name: "Spending Coins",
            showInLegend: true,
            yValueFormatString: "",
            dataPoints: [
                { label: "Tháng 1", y: randomInt(10, 50) },
                { label: "Tháng 2", y: randomInt(10, 50) },
                { label: "Tháng 3", y: randomInt(10, 50) },
                { label: "Tháng 4", y: randomInt(10, 50) },
                { label: "Tháng 5", y: randomInt(10, 50) }
            ]
        }]
}