import React from 'react';

class PersonalInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editable: false,
        }
    }

    openEditableAbility = () => {
        this.setState({
            editable: true
        })
    }

    cancelEditableAbility = () => {
        this.setState({
            editable: false
        })
    }

    render() {
        let styleBorder = {
            borderColor: 'darkgray'
        }

        let styleBorderTextArea = {
            borderColor: 'darkgray',
            resize: 'vertical'
        }

        return (
            <div style={{ padding: '0 50px' }}>
                <div style={{ fontSize: '30px', marginBottom: '20px' }}>Thông tin cá nhân</div>
                <div className={'tm-pi-row'}>
                    <div className={'tm-pi-input-title'}>First name: </div>
                    <input className={'tm-pi-input'} value={'First Name'} disabled={this.state.editable ? '' : 'disabled'} style={this.state.editable ? styleBorder : {}} />
                </div>
                <div className={'tm-pi-row'}>
                    <div className={'tm-pi-input-title'}>Last name: </div>
                    <input className={'tm-pi-input'} value={'Last Name'} disabled={this.state.editable ? '' : 'disabled'} style={this.state.editable ? styleBorder : {}} />
                </div>
                <div className={'tm-pi-row'}>
                    <div className={'tm-pi-input-title'}>Date of birth: </div>
                    <input className={'tm-pi-input'} value={'1997-05-06'} type={'date'} disabled={this.state.editable ? '' : 'disabled'} style={this.state.editable ? styleBorder : {}} />
                </div>
                <div className={'tm-pi-row'}>
                    <div className={'tm-pi-input-title'}>Mail: </div>
                    <input className={'tm-pi-input'} value={'nguyentienmanh6@gmail.com'} type={'email'} disabled={this.state.editable ? '' : 'disabled'} style={this.state.editable ? styleBorder : {}} />
                </div>
                <div className={'tm-pi-row'}>
                    <div className={'tm-pi-input-title'}>Facebook: </div>
                    <input className={'tm-pi-input'} value={'tienmanh2208'} disabled={this.state.editable ? '' : 'disabled'} style={this.state.editable ? styleBorder : {}} />
                </div>
                <div className={'tm-pi-row'}>
                    <div className={'tm-pi-input-title'}>Instagram: </div>
                    <input className={'tm-pi-input'} value={'tienmanh_tm'} disabled={this.state.editable ? '' : 'disabled'} style={this.state.editable ? styleBorder : {}} />
                </div>
                <div className={'tm-pi-row'}>
                    <div className={'tm-pi-input-title'}>Instagram: </div>
                    <textarea className={'tm-pi-input'} value={'tienmanh_tm'} disabled={this.state.editable ? '' : 'disabled'} style={this.state.editable ? styleBorderTextArea : { resize: 'vertical' }} />
                </div>
                {this.state.editable ?
                    <div>
                        <button onClick={this.cancelEditableAbility.bind(this)} className={'tm-btn tm-btn-cancel'}>Cancel</button>
                        <button onClick={this.cancelEditableAbility.bind(this)} className={'tm-btn tm-btn-success'}>Save</button>
                    </div>
                    : <div><button onClick={this.openEditableAbility.bind(this)} className={'tm-btn tm-btn-warning'}>Edit info</button></div>}
            </div>
        )
    }
}

export default PersonalInfo;