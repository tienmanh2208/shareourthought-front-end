import React from 'react';
import {
    BASE_URL_IMAGE,
    USER_SERVICE_DOMAIN,
    API_USER_GET_DETAIL_INFO
} from '../../constants.js';
import { Link } from 'react-router-dom';
import Axios from 'axios';

class PersonalHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetching: true,
            userInfo: null,
        }
    }

    render() {
        let questionCount = 0;
        let answerCount = 0;
        let rightRate = 0;

        if (this.state.userInfo !== null) {
            questionCount = this.state.userInfo.quetion_info.open + this.state.userInfo.quetion_info.resolved + this.state.userInfo.quetion_info.cancel;
            answerCount = this.state.userInfo.answer_info.waiting + this.state.userInfo.answer_info.considering + this.state.userInfo.answer_info.accepted + this.state.userInfo.answer_info.rejected;
            rightRate = this.state.userInfo.answer_info.accepted / (this.state.userInfo.answer_info.accepted + this.state.userInfo.answer_info.rejected);
        }

        return (
            <div style={{ position: 'relative' }}>
                <div className={'tm-personal-header-background'} style={{ backgroundImage: 'url("' + BASE_URL_IMAGE + '/background/personal1.jpg")' }}>
                    <Link className={'tm-personal-header-signout tm-background-white-blur'} to={'/login'}>
                        <i className="fas fa-sign-out-alt"></i>
                    </Link>
                    <Link to={'/dashboard'} className={'tm-personal-header-homepage tm-background-white-blur'}>Home Page</Link>
                </div>
                <div className={'tm-personal-header-info-frame tm-flex-row-spacebtw-nowrap'}>
                    <div className={'tm-personal-header-avatar'}>
                        <img className={'tm-personal-img-avatar'} src={BASE_URL_IMAGE + '/avatar/user.svg'} alt='avatar' />
                    </div>
                    <div className={'tm-personal-header-info tm-flex-column-left'}>
                        <div className={'tm-personal-header-name-frame tm-flex-column-reverse-left'}>
                            <div className={'tm-personal-header-name tm-background-white-blur-no-hover'}>{this.state.isFetching ? 'Loading ...' : this.state.userInfo.last_name + ' ' + this.state.userInfo.first_name}</div>
                        </div>
                        <div className={'tm-personal-header-bonus-info-frame'}>
                            <div className={'tm-personal-header-bonus-info'}>
                                Số câu hỏi: <span className={'tm-personal-header-bi-statistic'}>{this.state.isFetching ? 'Loading ...' : questionCount}</span>
                                Số câu trả lời: <span className={'tm-personal-header-bi-statistic'}>{this.state.isFetching ? 'Loading ...' : answerCount}</span>
                                Tỉ lệ trả lời đúng: <span className={'tm-personal-header-bi-statistic'}>{this.state.isFetching ? 'Loading ...' : rightRate * 100} %</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    componentDidMount = () => {
        Axios.get(USER_SERVICE_DOMAIN + API_USER_GET_DETAIL_INFO, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                this.setState({
                    isFetching: false,
                    userInfo: res.data.data
                })
            } else {
                console.log(res);
            }
        }).catch(error => {
            console.log(error);
        })
    }
}

export default PersonalHeader;

// const exampleResponse = {
//     "id": 1,
//     "username": "tienmanh",
//     "first_name": "Mạnh",
//     "date_of_birth": "1997-08-22",
//     "last_name": "Nguyễn",
//     "email": "nguyentienmanh6@gmail.com",
//     "role": 1,
//     "account_status": 1,
//     "coin_remain": 18000,
//     "created_at": "2020-03-18 15:57:48",
//     "updated_at": "2020-05-29 07:47:55",
//     "user_id": 1,
//     "facebook_account": null,
//     "instagram": null,
//     "introduction": null,
//     "total_upvotes": 0,
//     "total_downvotes": 0,
//     "total_comments": 0,
//     "total_answers": 2,
//     "total_questions": 3,
//     "quetion_info": {
//         "open": 2,
//         "resolved": 0,
//         "cancel": 0,
//         "deleted": 0
//     },
//     "answer_info": {
//         "waiting": 0,
//         "considering": 0,
//         "accepted": 1,
//         "rejected": 0
//     }
// };