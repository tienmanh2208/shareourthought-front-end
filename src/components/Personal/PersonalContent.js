import React from 'react';
import PersonalCategory from './SmallComponent/PersonalCategory';
import { PERSONAL_CATEGORIES } from '../../constants.js';
import PersonalDashboard from './PersonalDashboard';
import PersonalCoinManagement from './PersonalCoinManagement';
import PersonalQuestionAndAnswer from './PersonalQuestionAndAnswer';
import PersonalInfo from './PersonalInfo';
import PersonalGroupManagement from './PersonalGroupManagement';

class PersonalContent extends React.Component {
    render() {
        return (
            <div style={{ marginTop: '100px' }}>
                <PersonalCategory categoryName={this.props.categoryName} />
                {
                    this.props.categoryName === PERSONAL_CATEGORIES.dashboard ? <PersonalDashboard />
                        : this.props.categoryName === PERSONAL_CATEGORIES.coinManagement ? <PersonalCoinManagement />
                            : this.props.categoryName === PERSONAL_CATEGORIES.questionAndAnswer ? <PersonalQuestionAndAnswer />
                                : this.props.categoryName === PERSONAL_CATEGORIES.editInfo ? <PersonalInfo />
                                    : this.props.categoryName === PERSONAL_CATEGORIES.groupManagement ? <PersonalGroupManagement />
                                        : ''
                }
            </div>
        )
    }
}

export default PersonalContent;