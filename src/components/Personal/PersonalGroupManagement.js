import React from 'react';
import PersonalYourGroup from './SmallComponent/PersonalYourGroup';
import PersonalJoinedGroup from './SmallComponent/PersonalJoinedGroup';

class PersonalGroupManagement extends React.Component {
    render() {
        let yourGroups = [];
        let yourJoinedGroups = [];

        for (let i = 0; i < 4; ++i) {
            yourGroups.push(<PersonalYourGroup />)
        }

        for (let i = 0; i < 4; ++i) {
            yourJoinedGroups.push(<PersonalJoinedGroup />)
        }

        return (
            <div style={{ padding: '20px 50px' }}>
                <div style={{ textAlign: 'center', fontSize: '30px' }}>Your Groups</div>
                <div className={'tm-pygs-frame'}>
                    {yourGroups}
                </div>
                <div style={{ textAlign: 'center', fontSize: '30px' }}>Your Groups</div>
                <div className={'tm-pygs-frame'}>
                    {yourJoinedGroups}
                </div>
            </div>
        )
    }
}

export default PersonalGroupManagement;