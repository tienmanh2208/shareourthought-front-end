import React from 'react';
import PersonalChartDashboard from './SmallComponent/PersonalChartDashboard';

class PersonalDashboard extends React.Component {
    render() {
        return (
            <div>
                <PersonalChartDashboard />
            </div>
        )
    }
}

export default PersonalDashboard;