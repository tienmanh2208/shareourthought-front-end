import React from 'react';
import { useParams } from 'react-router';
import Header from '../Dashboard/Header';
import GlobalContent from './GlobalContent';
import Footer from '../Dashboard/Footer';

class GlobalPage extends React.Component {
    render() {
        return (
            <div>
                <Header isGlobal={true} />
                <GlobalContentPage />
                <Footer />
            </div>
        )
    }
}

export default GlobalPage;

const GlobalContentPage = () => {
    let { globalPage } = useParams();
    return <GlobalContent page={globalPage} />;
}
