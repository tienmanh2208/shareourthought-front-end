import React from 'react';
import NewestQuestion from '../Dashboard/Content/NewestQuestion';
import Topics from '../Dashboard/Content/Topics';

class GlobalDashboard extends React.Component {
    render() {
        return (
            <div>
                <NewestQuestion page={'global-dashboard'} />
                <Topics page={'global-dashboard'} />
            </div>
        )
    }
}

export default GlobalDashboard;