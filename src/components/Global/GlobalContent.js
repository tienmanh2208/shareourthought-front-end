import React from 'react';
import GlobalDashboard from './GlobalDashboard';
import SidebarRight from '../Dashboard/Sidebar/SidebarRight';
import SearchQuestion from '../Dashboard/SearchQuestion';

class GlobalContent extends React.Component {
    render() {
        console.log(this.props.page);

        switch (this.props.page) {
            case GroupPage.DASHBOARD:
                return (
                    <div className={'tm-db-content-frame'}>
                        <GlobalDashboard />
                        <SidebarRight />
                    </div>
                )
            case GroupPage.FIND_QUESTION:
                return (
                    <div className={'tm-db-content-frame'}>
                        <SearchQuestion page={'global-search'}/>
                        <SidebarRight />
                    </div>
                );
            default:
                return (
                    <div className={'tm-db-content-frame'}>
                        <GlobalDashboard />
                        <SidebarRight />
                    </div>
                )
        }
    }
}

export default GlobalContent;

const GroupPage = {
    DASHBOARD: 'dashboard',
    FIND_QUESTION: 'find-question',
}