import React from 'react';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import FormLoading from '../../General/FormLoading';
import FormNotification from '../../General/FormNotification';
import Axios from 'axios';
import {
    USER_SERVICE_DOMAIN,
    API_USER_QUESTION_UPDATE_CONTENT
} from '../../../constants';

class FormEditContentQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            question: {
                title: '',
                content: '',
            },
            isLoading: false,
            formNotification: {
                isDisplay: false,
                title: '',
                content: '',
            },
            closeFormWhenSuccess: false,
        }
        this.callToUpdateContentOfQuestion = this.callToUpdateContentOfQuestion.bind(this);
        this.updateContentSuccessfully = this.updateContentSuccessfully.bind(this);
        this.updateContentFailed = this.updateContentFailed.bind(this);
        this.updateContent = this.updateContent.bind(this);
    }

    render() {
        if (this.state.formNotification.isDisplay) {
            return <FormNotification title={this.state.formNotification.title} content={this.state.formNotification.content} action={this.closeFormNotification.bind(this)} />;
        }

        if (this.state.closeFormWhenSuccess) {
            this.props.closeForm(true);
        }

        let style = {
            margin: 'auto',
            padding: '20px',
            backgroundColor: 'white',
            borderRadius: '10px',
            display: 'flex',
            // width: '600px',
            flexDirection: 'column',
            justifyContent: 'center'
        };

        let styleTitle = {
            fontSize: '20px',
            lineHeight: '35px',
            margin: '10px',
            textAlign: 'center'
        }

        let styleDeco = {
            width: '100%',
            borderStyle: 'solid',
            borderRadius: '2px',
            borderWidth: '1px',
            borderColor: 'green',
        };

        return (
            <div className={'tm-form-container'}>
                <div style={style}>
                    <div style={styleTitle}>Sửa nội dung câu hỏi</div>
                    <div style={styleDeco} />
                    <div class={'qd-form-edit-frame'}>
                        <input value={this.state.question.title} onChange={this.updateTitle.bind(this)} className={'tm-newqt-title'} type={'input'} placeholder={'Tiêu đề bài viết'} />
                        <CKEditor
                            editor={ClassicEditor}
                            data={this.props.questionInfo.content}
                            onInit={editor => {
                                // You can store the "editor" and use when it is needed.
                                // console.log('Editor is ready to use!', editor);
                            }}
                            onChange={(event, editor) => {
                                const data = editor.getData();
                                this.updateContent(data);
                                // console.log({event, editor, data});
                            }}
                            onBlur={(event, editor) => {
                                // console.log('Blur.', editor);
                            }}
                            onFocus={(event, editor) => {
                                // console.log('Focus.', editor);
                            }}
                        />
                    </div>
                    <div class={'qd-form-edit-button-frame'}>
                        <button onClick={this.updateContentForQuestion.bind(this)} class={'tm-btn tm-btn-success'}>Lưu</button>
                        <button onClick={this.props.closeForm} class={'tm-btn tm-btn-cancel'}>Hủy</button>
                    </div>
                </div>
                {this.state.isLoading ? <FormLoading /> : ''}
            </div>
        )
    }

    componentDidMount = () => {
        this.setState({
            question: this.props.questionInfo
        })
    }

    updateContentForQuestion = () => {
        this.setState({
            isLoading: true,
        })

        this.callToUpdateContentOfQuestion(this.updateContentSuccessfully, this.updateContentFailed);
    }

    callToUpdateContentOfQuestion = (callBack, callBackWhenFail) => {
        Axios.post(USER_SERVICE_DOMAIN + API_USER_QUESTION_UPDATE_CONTENT, {
            content: this.state.question,
            question_id: this.props.questionId
        }, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 203) {
                callBack(res.data.message);
            } else {
                callBackWhenFail(res.data.message);
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail('Lỗi hệ thống');
        })
    }

    updateContentSuccessfully = (message) => {
        this.setState({
            isLoading: false,
            formNotification: {
                isDisplay: true,
                title: 'Thông báo',
                content: message,
            },
            closeFormWhenSuccess: true,
        })
    }

    updateContentFailed = (message) => {
        this.setState({
            isLoading: false,
            formNotification: {
                isDisplay: true,
                title: 'Thông báo',
                content: message,
            },
        })
    }

    closeFormNotification = () => {
        this.setState({
            formNotification: {
                isDisplay: false,
                title: '',
                content: '',
                action: null
            }
        })
    }

    updateTitle = (event) => {
        this.setState({
            question: Object.assign(this.state.question, { title: event.target.value })
        })
    }

    updateContent = (data) => {
        this.setState({
            question: Object.assign(this.state.question, { content: data })
        })
    }
}

export default FormEditContentQuestion;
