import React from 'react';
import { SECTION_IMAGES } from '../../../imagesConstant';
import FormLoading from '../../General/FormLoading';
import FormNotification from '../../General/FormNotification';
import Axios from 'axios';
import {
    USER_SERVICE_DOMAIN,
    API_USER_QUESTION_UPDATE_COIN
} from '../../../constants';

class FormEditPriceQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            price: null,
            isLoading: false,
            formNotification: {
                isDisplay: false,
                title: '',
                content: '',
            },
            closeFormWhenSuccess: false,
        }
        this.callToUpdatePriceOfQuestion = this.callToUpdatePriceOfQuestion.bind(this);
        this.updatePriceSuccessfully = this.updatePriceSuccessfully.bind(this);
        this.updatePriceFailed = this.updatePriceFailed.bind(this);
    }

    render() {
        if (this.state.formNotification.isDisplay) {
            return <FormNotification title={this.state.formNotification.title} content={this.state.formNotification.content} action={this.closeFormNotification.bind(this)} />;
        }

        if (this.state.closeFormWhenSuccess) {
            this.props.closeForm(true);
        }

        let style = {
            margin: 'auto',
            padding: '20px',
            backgroundColor: 'white',
            borderRadius: '10px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center'
        };

        let styleTitle = {
            fontSize: '20px',
            lineHeight: '35px',
            margin: '10px',
            textAlign: 'center'
        }

        let styleDeco = {
            width: '100%',
            borderStyle: 'solid',
            borderRadius: '2px',
            borderWidth: '1px',
            borderColor: 'green',
        };

        return (
            <div className={'tm-form-container'}>
                <div style={style}>
                    <div style={styleTitle}>Sửa coin cho câu hỏi</div>
                    <div style={styleDeco} />
                    <div class={'qd-form-edit-frame'}>
                        <input onChange={this.updateCoin.bind(this)} class={'qd-form-edit-select-box'} value={this.state.price} type={'number'} />
                    </div>
                    <div class={'qd-form-edit-button-frame'}>
                        <button onClick={this.updatePrice.bind(this)} class={'tm-btn tm-btn-success'}>Lưu</button>
                        <button onClick={this.props.closeForm} class={'tm-btn tm-btn-cancel'}>Hủy</button>
                    </div>
                </div>
                {this.state.isLoading ? <FormLoading /> : ''}
            </div>
        )
    }

    componentDidMount = () => {
        this.setState({
            price: this.props.currentCoin
        })
    }

    updatePrice = () => {
        this.setState({
            isLoading: true,
        })

        this.callToUpdatePriceOfQuestion(this.updatePriceSuccessfully, this.updatePriceFailed);
    }

    updateCoin = (event) => {
        this.setState({
            price: event.target.value
        })
    }

    callToUpdatePriceOfQuestion = (callBack, callBackWhenFail) => {
        Axios.post(USER_SERVICE_DOMAIN + API_USER_QUESTION_UPDATE_COIN, {
            price: this.state.price,
            question_id: this.props.questionId
        }, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 203) {
                callBack(res.data.message);
            } else {
                callBackWhenFail(res.data.message);
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail('Lỗi hệ thống');
        })
    }

    updatePriceSuccessfully = (message) => {
        this.setState({
            isLoading: false,
            formNotification: {
                isDisplay: true,
                title: 'Thông báo',
                content: message,
            },
            closeFormWhenSuccess: true,
        })
    }

    updatePriceFailed = (message) => {
        this.setState({
            isLoading: false,
            formNotification: {
                isDisplay: true,
                title: 'Thông báo',
                content: message,
            },
        })
    }

    closeFormNotification = () => {
        this.setState({
            formNotification: {
                isDisplay: false,
                title: '',
                content: '',
                action: null
            }
        })
    }
}

export default FormEditPriceQuestion;
