import React from "react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import CKEditor from "@ckeditor/ckeditor5-react";
import { BASE_URL_IMAGE } from '../../../constants.js';

class CreateAnswer extends React.Component {
    render() {
        return (
            <div>
                <div className={'tm-flex-row-spacebtw-nowrap tm-margin-top20'}>
                    <div>
                        <img className={'tm-qd-ca-avatar'} src={BASE_URL_IMAGE + '/avatar/user.svg'} alt={'Avatar'} />
                    </div>
                    <div className={'tm-qd-ca-editor'} style={{ zIndex: this.props.editorIndex }}>
                        <CKEditor
                            editor={ClassicEditor}
                            onInit={editor => {
                                // You can store the "editor" and use when it is needed.
                                console.log('Editor is ready to use!', editor);
                            }}
                            onChange={(event, editor) => {
                                const data = editor.getData();
                                this.props.updateContent(data);
                            }}
                            onBlur={(event, editor) => {
                                console.log('Blur.', editor);
                            }}
                            onFocus={(event, editor) => {
                                console.log('Focus.', editor);
                            }}
                        />
                    </div>
                </div>
                <div className={'tm-flex-row-reverse tm-margin-top10'}>
                    <button onClick={this.props.submitAnswer} className={'tm-btn tm-btn-success'}>Submit</button>
                    <select onClick={this.props.updateInputType} className={'tm-btn'}>
                        <option value={1}>Câu trả lời</option>
                        <option value={2}>Bình luận</option>
                    </select>
                </div>
            </div>
        );
    }
}

export default CreateAnswer;