import React from "react";
import { BASE_URL_IMAGE } from '../../../constants.js';
import ReactHtmlParser from 'react-html-parser';

class QuestionComment extends React.Component {
    render() {
        return (
            <div className={'tm-flex-row-spacebtw-nowrap tm-margin-top20'}>
                <div>
                    <img className={'tm-qd-ca-avatar'} src={BASE_URL_IMAGE + '/avatar/user.svg'} alt={'avatar'} />
                </div>
                <div className={'tm-qd-ca-content'}>
                    <div className={'tm-qd-qa-info'}>
                        <div className={'tm-qd-qa-name'}>{this.props.comment.commentator_name}</div>
                        <div className={'tm-qd-qa-type'}>Comment</div>
                    </div>
                    <div className={'tm-qd-qc-content'}>{ReactHtmlParser(this.props.comment.content)}</div>
                    <div className={'tm-flex-row-left-nowrap'}>
                        {/* <div className={'tm-qd-ct-info-title'}>Vote <span
                            className={'tm-qd-ct-info-number'}>{ANSWER_INFO.vote}</span></div> */}
                        <div className={'tm-qd-qa-report'}>REPORT</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default QuestionComment;

// const example_comment = {
//     commentator_id: 2,
//     commentator_name: 'Nguyen Trung Anh',
//     content: "Cau nay kho the :/",
//     time: "2020-05-04T08:06:49.101Z"
// }