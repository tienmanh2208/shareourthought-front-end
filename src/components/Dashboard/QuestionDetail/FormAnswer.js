import React from "react";
import {
    ANSWER_STATUS,
    USER_ROLE,
    ANSWER_REPORT_STATUS,
    BASE_URL_IMAGE,
    USER_SERVICE_DOMAIN,
    API_GET_ANSWER_DETAIL,
    API_UPDATE_STATUS_ANSWER,
    API_USER_REPORT_REJECTED_ANSWER,
    API_GROUP_UPDATE_STATUS_ANSWER
} from '../../../constants.js';
import ReactHtmlParser from 'react-html-parser';
import FormLoadingSmall from "../../General/FormLoadingSmall.js";
import Axios from "axios";

class FormAnswer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleForm: false,
            isLoading: true,
            answer: null,
            role: null,
            errorInfo: {
                status: false,
                message: '',
            }
        }
        this.updateAnswerInfo = this.updateAnswerInfo.bind(this);
        this.updateError = this.updateError.bind(this);
        this.turnOffIsLoading = this.turnOffIsLoading.bind(this);
        this.reloadAnswerInfo = this.reloadAnswerInfo.bind(this);
        this.callToReportContent = this.callToReportContent.bind(this);
    }

    render() {
        if (this.state.isLoading) {
            return (
                <div className={'tm-form-container'}>
                    <div className={'tm-form-frame'} style={this.state.visibleForm ? { maxWidth: '900px', opacity: 1 } : { maxWidth: '900px' }}>
                        <FormLoadingSmall />
                    </div>
                </div>
            )
        }

        if (this.state.errorInfo.status) {
            return (
                <div className={'tm-form-container'}>
                    <div className={'tm-form-frame'} style={this.state.visibleForm ? { maxWidth: '900px', opacity: 1 } : { maxWidth: '900px' }}>
                        <h1 style={{ textAlign: 'center' }}>Thông báo</h1>
                        <div style={{ textAlign: 'center', margin: '20px' }}>{this.state.errorInfo.message}</div>
                        <div className={'tm-flex-row-center'} style={{ width: '100%' }}>
                            <button onClick={this.props.closeForm} className={'tm-btn tm-btn-warning'}>Close</button>
                        </div>
                    </div>
                </div>
            );
        }

        return (
            <div className={'tm-form-container'}>
                <div className={'tm-form-frame'} style={this.state.visibleForm ? { maxWidth: '900px', opacity: 1 } : { maxWidth: '900px' }}>
                    <div className={'tm-flex-row-spacebtw-nowrap'}>
                        <div>
                            <img className={'tm-form-avatar'} src={BASE_URL_IMAGE + '/avatar/user.svg'} alt={'avatar'} />
                        </div>
                        <div className={'tm-form-info'}>
                            <div className={'tm-form-name'}>{this.state.answer.user_name}</div>
                            <div className={'tm-form-statistic'}>
                                Số câu trả lời đã đưa ra: <span className={'tm-form-count'}>{this.state.answer.total_answers}</span>
                                Thời gian trả lời: <span className={'tm-form-count'}>{this.state.answer.created_at}</span>
                                Trạng thái: <span className={'tm-form-count'}>{getAnswerStatus(this.state.answer.status)}</span>
                                Báo cáo: <span className={'tm-form-count'}>{this.state.answer.report_status === ANSWER_REPORT_STATUS.RESOLVED_BY_ADMIN
                                    ? 'Đã xử lý xong'
                                    : this.state.answer.report_status === ANSWER_REPORT_STATUS.REPORTED
                                        ? 'Đã báo cáo'
                                        : 'Chưa báo cáo'}</span>
                            </div>
                        </div>
                    </div>
                    <div className={'tm-form-content'}>
                        {ReactHtmlParser(this.state.answer.content)}
                    </div>
                    <div className={'tm-flex-row-reverse'}>
                        <button onClick={this.props.closeForm} className={'tm-btn tm-btn-warning'}>Close</button>
                        {(this.state.role === USER_ROLE.QUESTIONER && (this.state.answer.status === ANSWER_STATUS.WAITING || this.state.answer.status === ANSWER_STATUS.CONSIDERING))
                            ? <button onClick={this.handleUpdateStatus.bind(this, ANSWER_STATUS.ACCEPTED)} className={'tm-btn tm-btn-success'}>Accept</button>
                            : ''}
                        {(this.state.role === USER_ROLE.QUESTIONER && (this.state.answer.status === ANSWER_STATUS.WAITING || this.state.answer.status === ANSWER_STATUS.CONSIDERING))
                            ? <button onClick={this.handleUpdateStatus.bind(this, ANSWER_STATUS.REJECTED)} className={'tm-btn tm-btn-cancel'}>Reject</button>
                            : ''}
                        {(this.state.answer.report_status != ANSWER_REPORT_STATUS.REPORTED
                            && this.state.answer.report_status != ANSWER_REPORT_STATUS.RESOLVED_BY_ADMIN
                            && this.state.role === USER_ROLE.ANSWERER
                            && (this.state.answer.status === ANSWER_STATUS.ACCEPTED || this.state.answer.status === ANSWER_STATUS.REJECTED))
                            ? <button onClick={this.handleReport.bind(this)} className={'tm-btn tm-btn-cancel'}>Report</button>
                            : ''}
                    </div>
                </div>
            </div >
        );
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                visibleForm: true,
            })
        }, 200)

        this.callToGetAnswerDetail(this.props.answerId, this.updateAnswerInfo, this.updateError);
    }

    handleUpdateStatus = (status) => {
        this.setState({
            isLoading: true,
        })

        if (this.props.groupId === undefined) {
            this.callToUpdateStatusAnswer(status, this.reloadAnswerInfo, this.turnOffIsLoading);
        } else {
            this.callToUpdateStatusAnswerGroup(status, this.reloadAnswerInfo, this.turnOffIsLoading);
        }
    }

    handleReport = () => {
        this.setState({
            isLoading: true,
        })

        this.callToReportContent(this.reloadAnswerInfo, this.turnOffIsLoading);
    }

    reloadAnswerInfo = () => {
        this.callToGetAnswerDetail(this.props.answerId, this.updateAnswerInfo, this.updateError);
    }

    callToGetAnswerDetail = (answerId, callBack, callBackWhenFail) => {
        Axios.get(USER_SERVICE_DOMAIN + API_GET_ANSWER_DETAIL + answerId, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                callBack(res.data.data);
            } else {
                callBackWhenFail(res.data.message);
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail('Có lỗi xảy ra, xin vui lòng thử lại sau');
        })
    }

    callToUpdateStatusAnswerGroup = (status, callBack, callBackWhenFail) => {
        Axios.post(USER_SERVICE_DOMAIN + API_GROUP_UPDATE_STATUS_ANSWER, {
            answer_id: this.state.answer._id,
            question_id: this.props.questionId,
            status: status,
            group_id: this.props.groupId
        }, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 203) {
                callBack();
            } else {
                console.log(res.data);
                callBackWhenFail();
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail();
        })
    }

    callToUpdateStatusAnswer = (status, callBack, callBackWhenFail) => {
        Axios.post(USER_SERVICE_DOMAIN + API_UPDATE_STATUS_ANSWER, {
            answer_id: this.state.answer._id,
            question_id: this.props.questionId,
            status: status
        }, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 203) {
                callBack();
            } else {
                console.log(res.data);
                callBackWhenFail();
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail();
        })
    }

    callToReportContent = (callBack, callBackWhenFail) => {
        Axios.post(USER_SERVICE_DOMAIN + API_USER_REPORT_REJECTED_ANSWER, {
            answerId: this.state.answer._id,
            reason: 'Người dùng nhận xét sai câu trả lời'
        }, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 201) {
                callBack();
            } else {
                console.log(res.data);
                callBackWhenFail();
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail();
        })
    }

    updateAnswerInfo = (data) => {
        this.setState({
            isLoading: false,
            answer: data.answer,
            role: data.role,
            errorInfo: {
                status: false,
                message: '',
            }
        })
    }

    updateError = (message) => {
        this.setState({
            isLoading: false,
            errorInfo: {
                status: true,
                message: message,
            }
        })
    }

    turnOffIsLoading = () => {
        this.setState({
            isLoading: false,
        })
    }
}

export default FormAnswer;

const getAnswerStatus = (status) => {
    switch (status) {
        case ANSWER_STATUS.REJECTED:
            return 'REJECTED';
        case ANSWER_STATUS.CONSIDERING:
            return 'CONSIDERING';
        case ANSWER_STATUS.ACCEPTED:
            return 'ACCEPTED';
        default:
            return 'OPEN'
    }
}

// const exampleResponse = {
//     "answer": {
//         "_id": "5ed67959d2e07864e10c40d2",
//         "user_id": 2,
//         "question_id": "5ed0d8e275f4260f552d3392",
//         "content": "<p>Mình tưởng psychopath và sociopath giống nhau?</p>",
//         "status": 1,
//         "report_status": 1,
//         "updated_at": "2020-06-02T16:07:53.624000Z",
//         "created_at": "2020-06-02T16:07:53.624000Z",
//         "user_name": "NguyễnTrung Anh",
//         "total_answers": 0,
//         "total_questions": 0
//     },
//     "role": 2
// };