import React from "react";
import { BASE_URL_IMAGE, ANSWER_STATUS } from '../../../constants.js';

class QuestionAnswer extends React.Component {
    render() {
        let answer = this.props.answer;

        return (
            <div className={'tm-flex-row-spacebtw-nowrap tm-margin-top20'}>
                <div>
                    <img className={'tm-qd-ca-avatar'} src={BASE_URL_IMAGE + '/avatar/user.svg'} alt={'avatar'} />
                </div>
                <div className={'tm-qd-ca-content'}>
                    <div className={'tm-qd-qa-info'}>
                        <div className={'tm-qd-qa-name'}>{answer.user_name}</div>
                        <div className={'tm-qd-qa-type'}>Answer {answer.created_at}</div>
                    </div>
                    <div onClick={this.props.clickForm.bind(this, answer._id)} className={'tm-qd-qa-btn'}>Bấm vào để xem nội dung câu trả lời</div>
                    {/*<div className={'tm-qd-qa-btn'}>Click to open the answer</div>*/}
                    <div className={'tm-flex-row-left-nowrap'}>
                        {/* <div className={'tm-qd-ct-info-title'}>Vote <span
                            className={'tm-qd-ct-info-number'}>{ANSWER_INFO.vote}</span></div> */}
                        <div className={'tm-qd-ct-info-title'}>Xem bởi người hỏi: <span
                                className={'tm-qd-ct-info-number'}>{answer.status === 1 ? 'Chưa' : 'Rồi'}</span>
                        </div>
                        <div
                            className={'tm-qd-ct-info-title'}>Trạng thái: <span
                                className={'tm-qd-ct-info-number'}>{getAnswerStatus(answer.status)}</span>
                        </div>
                        <div className={'tm-qd-qa-report'}>BÁO CÁO</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default QuestionAnswer;

const getAnswerStatus = (status) => {
    switch (status) {
        case ANSWER_STATUS.REJECTED:
            return 'REJECTED';
        case ANSWER_STATUS.CONSIDERING:
            return 'CONSIDERING';
        case ANSWER_STATUS.ACCEPTED:
            return 'ACCEPTED';
        default:
            return 'OPEN'
    }
}

// const example_answer = {
//     _id: "5ec63ac03de60c585a1e2852",
//     user_id: 3,
//     question_id: "5ea6e3670dddd565555bb8a2",
//     status: 1,
//     user_name: 'Nguyen Hong Van',
//     total_answers: 21,
//     total_questions: 32,
//     created_at: "2020-05-21T08:24:32.256000Z"
// }