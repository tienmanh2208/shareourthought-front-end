import React from "react";
import { BASE_URL_IMAGE } from '../../../constants.js';
import FormLoadingSmall from '../../General/FormLoadingSmall';
import Tag from "../../General/Tag";
import ReactHtmlParser from 'react-html-parser';
import { getNameOfField, getQuestionStatus, getQuestionVisibility } from "../../../FunctionConstants.js";
import FormEditContentQuestion from "./FormEditContentQuestion.js";
import FormEditPriceQuestion from "./FormEditPriceQuestion.js";
import FormEditVisibilityQuestion from "./FormEditVisibilityQuestion.js";
import FormEditTagsQuestion from "./FormEditTagsQuestion.js";
import FormEditFieldQuestion from "./FormEditFieldQuestion.js";

class QuestionContent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formEditDisplay: {
                editField: false,
                editContent: false,
                editPrice: false,
                editVisibility: false,
                editTags: false,
            }
        }
    }

    render() {
        if (this.props.isLoading) {
            return (
                <div>
                    <FormLoadingSmall />
                </div>
            )
        }

        let info = this.props.questionDetail;
        console.log(info);

        let tags = info.tags.map(function (tagName) {
            return <Tag name={tagName} displayDelete={false} />
        });

        return (
            <div>
                <div className={'tm-flex-row-spacebtw-nowrap'}>
                    <div className={'tm-qd-ct-avatar-frame'}>
                        <img className={'tm-qd-ct-avatar'} src={BASE_URL_IMAGE + '/avatar/user.svg'} alt={'Avatar'} />
                    </div>
                    <div className={'tm-flex-column tm-qd-ct-info-frame'}>
                        <div className={'tm-qd-ct-name'}>{info.user_name}</div>
                        <div className={'tm-flex-row-left-nowrap'}>
                            {/* <div className={'tm-qd-ct-info-title'}>Trả lời: <span
                                className={'tm-qd-ct-info-number'}>{POST_INFO.replied}</span></div> */}
                            <div className={'tm-qd-ct-info-title'}>Trạng thái: <span
                                className={'tm-qd-ct-info-number'}>{getQuestionStatus(info.status)}</span></div>
                            <div className={'tm-qd-ct-info-title'}>Số coin: <span onClick={this.displayFormEditPrice.bind(this)}
                                className={'tm-qd-ct-info-number tm-qd-editable-content'}>{info.price} <i class="far fa-edit"></i></span></div>
                            <div className={'tm-qd-ct-info-title'}>Hiển thị: <span onClick={this.displayFormEditVisibility.bind(this)}
                                className={'tm-qd-ct-info-number tm-qd-editable-content'}>{getQuestionVisibility(info.visibility)} <i class="far fa-edit"></i></span></div>
                            <div onClick={this.displayFormEditContent.bind(this)} className={'tm-qd-ct-info-title tm-qd-editable-content'}>
                                SỬA NỘI DUNG CÂU HỎI
                            </div>
                        </div>
                    </div>
                    <div className={'tm-flex-column'}>
                        <button onClick={this.displayFormEditField.bind(this)} className={'tm-btn-warning tm-btn'}>{getNameOfField(info.field_id)} <i class="far fa-edit"></i></button>
                    </div>
                </div>
                <div>
                    <div className={'tm-qd-ct-title'}>{info.question_content.title}</div>
                    <div className={'tm-qd-ct-question'}>{ReactHtmlParser(info.question_content.content)}</div>
                </div>
                <div className={'tm-flex-row-left-nowrap'}>
                    <div className={'tm-qd-ct-info-title'}>Ngày tạo: <span
                        className={'tm-qd-ct-info-number'}>{info.created_at}</span></div>
                    <div className={'tm-qd-ct-info-title'}>Bình luận <span
                        className={'tm-qd-ct-info-number'}>{this.props.commentCount}</span></div>
                    {/* <div className={'tm-qd-ct-info-title'}>Upvote <span
                        className={'tm-qd-ct-info-number'}>{POST_INFO.upvote}</span></div> */}
                </div>
                <div className={'tm-newqt-tags'}>
                    <div className={'tm-newqt-ctg-title'}>Tags</div>
                    <div className={'tm-newqt-tags-list'}>
                        {tags}
                        <i onClick={this.displayFormEditTags.bind(this)} class="far fa-edit tm-qd-editable-content-icon"></i>
                    </div>
                </div>
                {this.state.formEditDisplay.editContent ? <FormEditContentQuestion questionInfo={info.question_content} closeForm={this.closeFormEdit.bind(this)} questionId={info._id} /> : ''}
                {this.state.formEditDisplay.editPrice ? <FormEditPriceQuestion currentCoin={info.price} closeForm={this.closeFormEdit.bind(this)} questionId={info._id} /> : ''}
                {this.state.formEditDisplay.editVisibility ? <FormEditVisibilityQuestion currentVisibility={info.visibility} closeForm={this.closeFormEdit.bind(this)} questionId={info._id} /> : ''}
                {this.state.formEditDisplay.editTags ? <FormEditTagsQuestion tags={info.tags} closeForm={this.closeFormEdit.bind(this)} questionId={info._id} /> : ''}
                {this.state.formEditDisplay.editField ? <FormEditFieldQuestion currentFieldId={info.field_id} closeForm={this.closeFormEdit.bind(this)} questionId={info._id} /> : ''}
            </div>
        );
    }

    displayFormEditField = () => {
        this.props.toggleEditorIndex();
        this.setState({
            formEditDisplay: Object.assign(this.state.formEditDisplay, { editField: true })
        })
    }

    displayFormEditVisibility = () => {
        this.props.toggleEditorIndex();
        this.setState({
            formEditDisplay: Object.assign(this.state.formEditDisplay, { editVisibility: true })
        })
    }

    displayFormEditPrice = () => {
        this.props.toggleEditorIndex();
        this.setState({
            formEditDisplay: Object.assign(this.state.formEditDisplay, { editPrice: true })
        })
    }

    displayFormEditTags = () => {
        this.props.toggleEditorIndex();
        this.setState({
            formEditDisplay: Object.assign(this.state.formEditDisplay, { editTags: true })
        })
    }

    displayFormEditContent = () => {
        this.props.toggleEditorIndex();
        this.setState({
            formEditDisplay: Object.assign(this.state.formEditDisplay, { editContent: true })
        })
    }

    closeFormEdit = (updateStatus) => {
        this.props.toggleEditorIndex();
        this.setState({
            formEditDisplay: {
                editField: false,
                editContent: false,
                editPrice: false,
                editVisibility: false,
                editTags: false,
            }
        })

        if (updateStatus === true) {
            this.props.updateContentQuestion();
        }
    }
}

export default QuestionContent;