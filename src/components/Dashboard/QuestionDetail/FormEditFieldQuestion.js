import React from 'react';
import { SECTION_IMAGES } from '../../../imagesConstant';
import FormLoading from '../../General/FormLoading';
import FormNotification from '../../General/FormNotification';
import Axios from 'axios';
import {
    USER_SERVICE_DOMAIN,
    API_USER_QUESTION_UPDATE_FIELD
} from '../../../constants';

class FormEditFieldQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedField: null,
            isLoading: false,
            formNotification: {
                isDisplay: false,
                title: '',
                content: '',
            },
            closeFormWhenSuccess: false,
        }
        this.callToUpdateFieldOfQuestion = this.callToUpdateFieldOfQuestion.bind(this);
        this.updateFieldSuccessfully = this.updateFieldSuccessfully.bind(this);
        this.updateFieldFailed = this.updateFieldFailed.bind(this);
    }

    render() {
        if (this.state.formNotification.isDisplay) {
            return <FormNotification title={this.state.formNotification.title} content={this.state.formNotification.content} action={this.closeFormNotification.bind(this)} />;
        }

        if (this.state.closeFormWhenSuccess) {
            this.props.closeForm(true);
        }

        let style = {
            margin: 'auto',
            padding: '20px',
            backgroundColor: 'white',
            borderRadius: '10px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center'
        };

        let styleTitle = {
            fontSize: '20px',
            lineHeight: '35px',
            margin: '10px',
            textAlign: 'center'
        }

        let styleDeco = {
            width: '100%',
            borderStyle: 'solid',
            borderRadius: '2px',
            borderWidth: '1px',
            borderColor: 'green',
        };

        let options = SECTION_IMAGES.map((item) => {
            return <option value={item.field_id}>{item.title}</option>;
        });

        return (
            <div className={'tm-form-container'}>
                <div style={style}>
                    <div style={styleTitle}>Sửa lĩnh vực câu hỏi</div>
                    <div style={styleDeco} />
                    <div class={'qd-form-edit-frame'}>
                        <select onChange={this.updateSelectedField.bind(this)} class={'qd-form-edit-select-box'} value={this.state.selectedField}>
                            {options}
                        </select>
                    </div>
                    <div class={'qd-form-edit-button-frame'}>
                        <button onClick={this.updateFieldId.bind(this)} class={'tm-btn tm-btn-success'}>Lưu</button>
                        <button onClick={this.props.closeForm} class={'tm-btn tm-btn-cancel'}>Hủy</button>
                    </div>
                </div>
                {this.state.isLoading ? <FormLoading /> : ''}
            </div>
        )
    }

    componentDidMount = () => {
        this.setState({
            selectedField: this.props.currentFieldId
        })
    }

    updateFieldId = () => {
        this.setState({
            isLoading: true,
        })

        this.callToUpdateFieldOfQuestion(this.updateFieldSuccessfully, this.updateFieldFailed);
    }

    updateSelectedField = (event) => {
        this.setState({
            selectedField: event.target.value
        })
    }

    callToUpdateFieldOfQuestion = (callBack, callBackWhenFail) => {
        Axios.post(USER_SERVICE_DOMAIN + API_USER_QUESTION_UPDATE_FIELD, {
            field_id: this.state.selectedField,
            question_id: this.props.questionId
        }, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 203) {
                callBack(res.data.message);
            } else {
                callBackWhenFail(res.data.message);
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail('Lỗi hệ thống');
        })
    }

    updateFieldSuccessfully = (message) => {
        this.setState({
            isLoading: false,
            formNotification: {
                isDisplay: true,
                title: 'Thông báo',
                content: message,
            },
            closeFormWhenSuccess: true,
        })
    }

    updateFieldFailed = (message) => {
        this.setState({
            isLoading: false,
            formNotification: {
                isDisplay: true,
                title: 'Thông báo',
                content: message,
            },
        })
    }

    closeFormNotification = () => {
        this.setState({
            formNotification: {
                isDisplay: false,
                title: '',
                content: '',
                action: null
            }
        })
    }
}

export default FormEditFieldQuestion;
