import React from 'react';
import { SECTION_IMAGES } from '../../../imagesConstant';
import FormLoading from '../../General/FormLoading';
import FormNotification from '../../General/FormNotification';
import Axios from 'axios';
import {
    USER_SERVICE_DOMAIN,
    API_USER_QUESTION_UPDATE_TAGS
} from '../../../constants';
import Tag from '../../General/Tag';
import { removeElementInArrayByValue } from '../../../FunctionConstants';

class FormEditTagsQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input: '',
            tags: [],
            isLoading: false,
            formNotification: {
                isDisplay: false,
                title: '',
                content: '',
            },
            closeFormWhenSuccess: false,
        }
        this.callToUpdateTagsOfQuestion = this.callToUpdateTagsOfQuestion.bind(this);
        this.updateTagsSuccessfully = this.updateTagsSuccessfully.bind(this);
        this.updateTagsFailed = this.updateTagsFailed.bind(this);
    }

    render() {
        if (this.state.formNotification.isDisplay) {
            return <FormNotification title={this.state.formNotification.title} content={this.state.formNotification.content} action={this.closeFormNotification.bind(this)} />;
        }

        if (this.state.closeFormWhenSuccess) {
            this.props.closeForm(true);
        }

        let style = {
            margin: 'auto',
            padding: '20px',
            backgroundColor: 'white',
            borderRadius: '10px',
            display: 'flex',
            minWidth: '300px',
            maxWidth: '500px',
            flexDirection: 'column',
            justifyContent: 'center'
        };

        let styleTitle = {
            fontSize: '20px',
            lineHeight: '35px',
            margin: '10px',
            textAlign: 'center'
        }

        let styleDeco = {
            width: '100%',
            borderStyle: 'solid',
            borderRadius: '2px',
            borderWidth: '1px',
            borderColor: 'green',
        };

        let tags = this.state.tags.map((item) => {
            return <Tag displayDelete={true} name={item} actionDelete={this.removeElementInTags.bind(this)} />
        });

        return (
            <div className={'tm-form-container'}>
                <div style={style}>
                    <div style={styleTitle}>Sửa thẻ của câu hỏi</div>
                    <div style={styleDeco} />
                    <div class={'qd-form-edit-frame'}>
                        <input onKeyDown={this.saveTag.bind(this)} onChange={this.updateInput.bind(this)} value={this.state.input} class={'qd-form-edit-select-box'} />
                    </div>
                    <div className={'tm-flex-row-left-wrap'}>{tags}</div>
                    <div class={'qd-form-edit-button-frame'}>
                        <button onClick={this.updateTags.bind(this)} class={'tm-btn tm-btn-success'}>Lưu</button>
                        <button onClick={this.props.closeForm} class={'tm-btn tm-btn-cancel'}>Hủy</button>
                    </div>
                </div>
                {this.state.isLoading ? <FormLoading /> : ''}
            </div>
        )
    }

    componentDidMount = () => {
        this.setState({
            tags: this.props.tags
        })
    }

    updateTags = () => {
        this.setState({
            isLoading: true,
        })

        this.callToUpdateTagsOfQuestion(this.updateTagsSuccessfully, this.updateTagsFailed);
    }

    saveTag = (event) => {
        if (event.key === 'Enter') {
            let tags = this.state.tags;
            tags.push(this.state.input);
            this.setState({
                tags: tags,
                input: ''
            })
        }
    };

    updateInput = (event) => {
        this.setState({
            input: event.target.value
        })
    }

    removeElementInTags = (name) => {
        let tags = this.state.tags;
        removeElementInArrayByValue(tags, name);
        this.setState({
            tags: tags
        })
    }

    callToUpdateTagsOfQuestion = (callBack, callBackWhenFail) => {
        Axios.post(USER_SERVICE_DOMAIN + API_USER_QUESTION_UPDATE_TAGS, {
            tags: this.state.tags,
            question_id: this.props.questionId
        }, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 203) {
                callBack(res.data.message);
            } else {
                callBackWhenFail(res.data.message);
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail('Lỗi hệ thống');
        })
    }

    updateTagsSuccessfully = (message) => {
        this.setState({
            isLoading: false,
            formNotification: {
                isDisplay: true,
                title: 'Thông báo',
                content: message,
            },
            closeFormWhenSuccess: true,
        })
    }

    updateTagsFailed = (message) => {
        this.setState({
            isLoading: false,
            formNotification: {
                isDisplay: true,
                title: 'Thông báo',
                content: message,
            },
        })
    }

    closeFormNotification = () => {
        this.setState({
            formNotification: {
                isDisplay: false,
                title: '',
                content: '',
                action: null
            }
        })
    }
}

export default FormEditTagsQuestion;
