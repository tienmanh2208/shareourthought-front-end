import React from "react";
import UserInfo from "./UserInfo";
import GroupInfo from "./GroupInfo";

class SidebarLeft extends React.Component {
    render() {
        return (
            <div className={'tm-sidebar-left-frame'}>
                <UserInfo/>
                <GroupInfo/>
            </div>
        );
    }
}

export default SidebarLeft;