import React from 'react';
import GroupAddMember from '../Group/GroupAddMember';
import GroupNewestMembers from '../Group/GroupNewestMembers';

class GroupRight extends React.Component {
    render() {
        return (
            <div className={'tm-sidebar-right-frame'}>
                <GroupAddMember groupId={this.props.groupId} />
                <GroupNewestMembers groupId={this.props.groupId} />
            </div>
        )
    }
}

export default GroupRight;