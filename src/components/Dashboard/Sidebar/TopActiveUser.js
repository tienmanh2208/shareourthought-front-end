import React from 'react';
import { BASE_URL_IMAGE, USER_SERVICE_DOMAIN, API_GET_TOP_USERS } from '../../../constants.js';
import Axios from 'axios';
import FormLoadingSmall from '../../General/FormLoadingSmall.js';

class TopActiveUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            topUsers: [],
            isFetching: true,
        }
    }

    componentDidMount() {
        Axios.get(USER_SERVICE_DOMAIN + API_GET_TOP_USERS).then(res => {
            if (res.data.code === 200) {
                let topUsers = res.data.data.map(userInfo => {
                    userInfo.avatar = BASE_URL_IMAGE + '/icon/star.svg';
                    return userInfo;
                }) 

                this.setState({
                    topUsers: topUsers,
                    isFetching: false
                })
            }
        }).catch(error => {
            console.log('Error when fetch top users');
            console.log(error);
        })
    }

    render() {
        let content = this.state.topUsers.map(generateTopActiveUser);
        return (
            <div className={'tm-sidebar-frame'}>
                <div className={'tm-sidebar-right-title'}>Người dùng tích cực</div>
                <div className={'tm-sidebar-right-deco'} />
                {this.state.isFetching ? <FormLoadingSmall style={{marginTop: '10px'}}/> : content}
            </div>
        );
    }
}

function generateTopActiveUser(userInfo) {
    return (
        <div key={userInfo.id} className={'tm-float-clear tm-margin-10'}>
            <img className={'tm-topuser-img'} src={userInfo.avatar} alt={'Avatar'} />
            <div className={'tm-topuser-name'}>{userInfo.full_name}</div>
        </div>
    );
}

export default TopActiveUser;

// Sample response
// const USER = [
//     {
//         id: 1,
//         full_name: 'Nguyen Tien Manh',
//         avatar: BASE_URL_IMAGE + '/icon/double-decker.svg'
//     }
// ];