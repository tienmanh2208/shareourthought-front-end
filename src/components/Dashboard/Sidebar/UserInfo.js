import React from "react";
import { BASE_URL_IMAGE, USER_SERVICE_DOMAIN, API_GET_BASIC_INFO } from '../../../constants.js';
import { Link, Redirect } from "react-router-dom";
import FormLoadingSmall from "../../General/FormLoadingSmall.js";
import Axios from "axios";

class UserInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            userInfo: {
                fullName: '',
                coinRemain: '',
                totalAnswers: '',
                totalQuestions: '',
            },
            redirectTo: null,
        }
    }

    componentDidMount() {
        Axios.get(USER_SERVICE_DOMAIN + API_GET_BASIC_INFO, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                this.setState({
                    isLoading: false,
                    userInfo: {
                        fullName: res.data.data.full_name,
                        coinRemain: res.data.data.coin_remain,
                        totalAnswers: res.data.data.total_answers,
                        totalQuestions: res.data.data.total_questions,
                    }
                })
            }
        }).catch(error => {
            console.log(error);
            if (error.response.status === 401) {
                localStorage.removeItem('sot-token');
                this.setState({
                    redirectTo: '/login'
                })
            };
        })
    }

    render() {
        if (this.state.redirectTo !== null) {
            return <Redirect to={this.state.redirectTo} />
        }

        return (
            <div className={'tm-flex-column-center'}>
                <Link to={'/personal/dashboard'}>
                    <img className={'tm-avatar-sidebar'} src={BASE_URL_IMAGE + '/avatar/user.svg'} alt={'Avatar'} />
                </Link>
                {this.state.isLoading ? <FormLoadingSmall style={{ margin: '10px' }} /> : <Link to={'/personal/dashboard'} className={'tm-sidebar-name'}>{this.state.userInfo.fullName}</Link>}
                {this.state.isLoading ? <FormLoadingSmall style={{ margin: '10px' }} /> : <div className={'tm-sidebar-detail-info'}>
                    <div className={'tm-sidebar-number'}>{this.state.userInfo.coinRemain}</div>
                    <div className={'tm-sidebar-title'}><span className={'tm-sb-span'}>coin</span></div>
                </div>}
                {this.state.isLoading ? <FormLoadingSmall style={{ margin: '10px' }} /> : <div className={'tm-sidebar-question-info'}>
                    <div className={'tm-sidebar-number'}>{this.state.userInfo.totalQuestions}</div>
                    <div className={'tm-sidebar-title'}><span className={'tm-sb-span'}>Câu hỏi</span></div>
                </div>}
                {this.state.isLoading ? <FormLoadingSmall style={{ margin: '10px' }} /> : <div className={'tm-sidebar-answer-info'}>
                    <div className={'tm-sidebar-number'}>{this.state.userInfo.totalAnswers}</div>
                    <div className={'tm-sidebar-title'}>Trả lời</div>
                </div>}
            </div>
        );
    }
}

export default UserInfo;
