import React from "react";
import TopActiveUser from "./TopActiveUser";
import TopSection from "./TopSection";

class SidebarRight extends React.Component {
    render() {
        return (
            <div className={'tm-sidebar-right-frame'}>
                <TopActiveUser/>
                <TopSection/>
            </div>
        );
    }
}

export default SidebarRight;