import React from "react";
import { SECTION_IMAGES } from '../../../imagesConstant';
import { USER_SERVICE_DOMAIN, API_GET_TOP_FIELDS } from '../../../constants.js';
import Axios from "axios";
import FormLoadingSmall from "../../General/FormLoadingSmall";

class TopSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sections: [],
            isFetching: true,
        }
    }

    componentDidMount() {
        Axios.get(USER_SERVICE_DOMAIN + API_GET_TOP_FIELDS).then(res => {
            if (res.data.code === 200) {
                let convertedData = res.data.data.map(info => {
                    info.icon = getIcon(info.id);
                    return info;
                });

                this.setState({
                    sections: convertedData,
                    isFetching: false,
                })
            }
        }).catch(error => {
            console.log('Fail when fetching data form top fields');
            console.log(error);
        })
    }

    render() {

        let content = this.state.sections.map(generateTopActiveUser);
        return (
            <div className={'tm-sidebar-frame'}>
                <div className={'tm-sidebar-right-title'}>Danh mục hot</div>
                <div className={'tm-sidebar-right-deco'} />
                {this.state.isFetching ? <FormLoadingSmall style={{marginTop: '10px'}} /> : content}
            </div>
        );
    }
}

function generateTopActiveUser(sectionInfo) {
    return (
        <div key={sectionInfo.id} className={'tm-float-clear tm-margin-10'}>
            <img className={'tm-topuser-img'} src={sectionInfo.icon} alt={'Avatar'} />
            <div className={'tm-topuser-name'}>{sectionInfo.name}</div>
        </div>
    );
}

export default TopSection;

const getIcon = (fieldId) => {
    for (let i = 0; i < SECTION_IMAGES.length; ++i) {
        if (SECTION_IMAGES[i].field_id === fieldId) {
            return SECTION_IMAGES[i].icon;
        }
    }
}
