import React from 'react';
import FormJoinGroup from '../Group/FormJoinGroup';
import { Link } from 'react-router-dom';
import { BASE_URL_IMAGE, USER_SERVICE_DOMAIN, API_GET_LIST_GROUPS_OF_CURRENT_USER } from '../../../constants.js';
import Axios from 'axios';
import FormLoadingSmall from '../../General/FormLoadingSmall';
import { ICONS } from '../../../imagesConstant.js';
import { randomInt } from '../../../FunctionConstants.js';

class GroupInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectTo: '',
            displayFormJoinGroup: false,
            isLoading: true,
            listGroup: []
        }
    }

    render() {
        // if (this.state.redirectTo !== '') {
        //     return <Redirect to={this.state.redirectTo} />
        // }

        let content = this.state.listGroup.map(generateGroups);

        return (
            <div className={'tm-sidebar-frame'}>
                <div className={'tm-sidebar-right-title'}>Nhóm của bạn</div>
                <div className={'tm-sidebar-right-deco'} />
                {this.state.isLoading ? <FormLoadingSmall /> : content}
                <div style={{ display: 'flex' }}>
                    <Link to={'/create-group'}
                        className={'tm-btn tm-btn-warning'}
                        style={{ width: '100%', margin: '5px 0', textAlign: 'center' }}>
                        Tạo nhóm
                    </Link>
                </div>
                <button onClick={this.toggleForm.bind(this)} className={'tm-btn tm-btn-warning'} style={{ width: '100%', margin: '5px 0' }}>Tham gia nhóm</button>
                {this.state.displayFormJoinGroup ? <FormJoinGroup closeForm={this.toggleForm.bind(this)} /> : ''}
            </div>
        );
    }

    componentDidMount() {
        Axios.get(USER_SERVICE_DOMAIN + API_GET_LIST_GROUPS_OF_CURRENT_USER, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                let groupInfos = res.data.data.map(groupInfo => {
                    groupInfo.avatar = ICONS[randomInt(0, ICONS.length - 1)].path;
                    return groupInfo;
                })

                this.setState({
                    isLoading: false,
                    listGroup: groupInfos,
                })
            }
        }).catch(error => {
            console.log('error');
            console.log(error);
        })
    }

    handleRedirect(route) {
        this.setState({
            redirectTo: route,
        })
    }

    toggleForm = () => {
        this.setState({
            displayFormJoinGroup: !this.state.displayFormJoinGroup
        })
    }
}

function generateGroups(groupInfo) {
    return (
        <div key={groupInfo.group_infos_id} className={'tm-float-clear tm-margin-10'}>
            <img className={'tm-topuser-img'} src={BASE_URL_IMAGE + groupInfo.avatar} alt={'Avatar'} />
            <Link to={'/group-dashboard/' + groupInfo.group_infos_id} className={'tm-topuser-name'}>{groupInfo.title}</Link>
        </div>
    );
}

export default GroupInfo;

// const GROUP_INFOS = [
//     {
//         name: 'Học tập',
//         avatar: BASE_URL_IMAGE + '/icon/double-decker.svg'
//     },
//     {
//         name: 'Nhóm học toán',
//         avatar: BASE_URL_IMAGE + '/icon/gears.svg'
//     }
// ];

// const responseSample = {
//     "code": 200,
//     "data": [
//         {
//             "group_infos_id": 5,
//             "users_id": 1,
//             "role": 1,
//             "permission": 4,
//             "creator": 2,
//             "title": "Marketing FTU",
//             "privacy": 1
//         }
//     ]
// }