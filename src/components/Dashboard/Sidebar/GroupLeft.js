import React from 'react';
import GroupInfoDetail from '../Group/GroupInfoDetail';
import GroupSections from '../Group/GroupSections';

class GroupLeft extends React.Component {
    render() {
        return (
            <div className={'tm-sidebar-left-frame'}>
                <GroupInfoDetail groupId={this.props.groupId} />
                <GroupSections groupId={this.props.groupId} />
            </div>
        )
    }
}

export default GroupLeft;