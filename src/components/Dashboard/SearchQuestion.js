import React from 'react';
import {
    SECTION_IMAGES
} from '../../imagesConstant';
import Axios from 'axios';
import {
    USER_SERVICE_DOMAIN,
    API_GLOBAL_FIND_QUESTION
} from '../../constants';
import Pagination from '../General/Pagination';
import FormLoadingSmall from '../General/FormLoadingSmall';
import Question from './Question';

class SearchQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            title: '',
            content: '',
            tags: '',
            price: {
                from: 0,
                to: 0,
            },
            field: 0,
            currentPage: 1,
            lastPage: 10,
            listQuestion: [],
            isFetching: false,
        }
        this.callToFindQuestion = this.callToFindQuestion.bind(this);
        this.updateListQuestion = this.updateListQuestion.bind(this);
        this.findQuestionFail = this.findQuestionFail.bind(this);
    }

    render() {
        console.log(this.state);

        let fields = SECTION_IMAGES.map((item) => {
            return <option value={item.field_id}>{item.title}</option>;
        });

        let content = this.state.listQuestion.map(questionInfo => {
            return <Question questionInfo={questionInfo} style={{ width: '100%' }} />;
        });

        return (
            <div className={'tm-db-ct-yq-frame'} style={this.props.page === 'global-search' ? { margin: 'unset', width: '80%' } : {}}>
                <div className={''}>
                    <div className={''}>
                        <div className={'tm-flex-row-left-nowrap'}>
                            <div className={'qd-form-edit-select-box'}>Danh mục</div>
                            <select onChange={this.inputField.bind(this)} value={this.state.field} className={'qd-form-edit-select-box'}>
                                <option value={0}>{'Tất cả'}</option>
                                {fields}
                            </select>
                        </div>
                        <div className={'tm-flex-row-left-nowrap'}>
                            <div className={'qd-form-edit-select-box'}>Người dùng</div>
                            <input onChange={this.inputUsername.bind(this)} value={this.state.username} className={'qd-form-edit-select-box'} placeholder={'User name của người dùng'} />
                        </div>
                    </div>
                    <div className={'tm-flex-row-left-nowrap'}>
                        <div className={'qd-form-edit-select-box'}>Tiêu đề</div>
                        <input onChange={this.inputTitle.bind(this)} value={this.state.title} className={'qd-form-edit-select-box'} placeholder='Key word trong tiêu đề' />
                    </div>
                    <div className={'tm-flex-row-left-nowrap'}>
                        <div className={'qd-form-edit-select-box'}>Nội dung</div>
                        <input onChange={this.inputContent.bind(this)} value={this.state.content} className={'qd-form-edit-select-box'} placeholder='Key word trong nội dung' />
                    </div>
                    <div className={'tm-flex-row-left-nowrap'}>
                        <div className={'qd-form-edit-select-box'}>Số coin</div>
                        <div className={'qd-form-edit-select-box'}>Từ</div>
                        <input onChange={this.inputPriceFrom.bind(this)} value={this.state.price.from} className={'qd-form-edit-select-box'} type='number' />
                        <div className={'qd-form-edit-select-box'}>Đến</div>
                        <input onChange={this.inputPriceTo.bind(this)} value={this.state.price.to} className={'qd-form-edit-select-box'} type='number' />
                    </div>
                    <div className={'tm-flex-row-left-nowrap'}>
                        <div className={'qd-form-edit-select-box'}>Thẻ</div>
                        <input onChange={this.inputTags.bind(this)} value={this.state.tags} className={'qd-form-edit-select-box'} />
                    </div>
                    <div className={'tm-flex-row-reverse'}>
                        <button onClick={this.findQuestion.bind(this)} className={'tm-btn tm-btn-success'}>Tìm kiếm</button>
                        <button onClick={this.clearInput.bind(this)} className={'tm-btn tm-btn-cancel'}>Xóa điều kiện</button>
                    </div>
                </div>
                {this.state.isFetching ? <FormLoadingSmall />
                    : this.state.listQuestion.length === 0 ? 'Không có kết quả để hiển thị'
                        : <div>
                            <div className={'tm-flex-row-spacebtw-wrap'}>
                                {content}
                            </div>
                            <Pagination index={this.state.currentPage} max={this.state.lastPage} switchPage={this.changeCurrentPage.bind(this)} />
                        </div>
                }
            </div>
        )
    }

    componentDidUpdate = (prevProps, prevState) => {
        if (this.state.currentPage !== prevState.currentPage) {
            this.setState({
                isFetching: true
            })
            this.callToFindQuestion(this.updateListQuestion, this.findQuestionFail);
        }
    }

    findQuestion = () => {
        this.setState({
            isFetching: true
        })

        this.callToFindQuestion(this.updateListQuestion, this.findQuestionFail);
    }

    callToFindQuestion = (callBack, callBackWhenFail) => {
        Axios.post(USER_SERVICE_DOMAIN + API_GLOBAL_FIND_QUESTION + '?page=' + this.state.currentPage, {
            title: this.state.title,
            content: this.state.content,
            tags: this.state.tags,
            price_from: this.state.price.from,
            price_to: this.state.price.to,
            username: this.state.username,
            field_id: this.state.field
        }).then(res => {
            if (res.data.code === 200) {
                callBack(res.data.data);
            } else {
                console.log(res.data);
                callBackWhenFail();
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail();
        })
    }

    updateListQuestion = (data) => {
        this.setState({
            isFetching: false,
            listQuestion: data.data,
            lastPage: data.last_page
        })
    }

    findQuestionFail = () => {
        this.setState({
            isFetching: false
        })
    }

    changeCurrentPage = (page) => {
        this.setState({
            currentPage: page
        })
    }

    inputUsername = (event) => {
        this.setState({
            username: event.target.value,
        })
    }

    inputTitle = (event) => {
        this.setState({
            title: event.target.value
        })
    }

    inputContent = (event) => {
        this.setState({
            content: event.target.value
        })
    }

    inputTags = (event) => {
        this.setState({
            tags: event.target.value
        })
    }

    inputPriceFrom = (event) => {
        this.setState({
            price: Object.assign(this.state.price, { from: event.target.value })
        })
    }

    inputPriceTo = (event) => {
        this.setState({
            price: Object.assign(this.state.price, { to: event.target.value })
        })
    }

    inputField = (event) => {
        this.setState({
            field: event.target.value
        })
    }

    clearInput = () => {
        this.setState({
            username: '',
            title: '',
            content: '',
            tags: '',
            price: {
                from: 0,
                to: 0,
            },
            field: 0
        })
    }
}

export default SearchQuestion;