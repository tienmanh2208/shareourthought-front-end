import React from "react";
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Link, Redirect } from "react-router-dom";
import { SECTION_IMAGES } from '../../imagesConstant.js';
import Tag from "../General/Tag";
import { removeElementInArrayByValue } from '../../FunctionConstants.js';
import { QUESTION_VISIBILITY, USER_SERVICE_DOMAIN, API_CREATE_QUESTION } from '../../constants.js';
import Axios from "axios";
import FormLoading from "../General/FormLoading.js";
import FormNotification from "../General/FormNotification.js";

class NewQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            question: {
                title: '',
                content: ''
            },
            price: '',
            visibility: 1,
            fieldId: 1,
            listTags: [],
            input: '',
            isLoading: false,
            isDisplayForm: false,
            formContent: {
                title: '',
                content: ''
            },
            redirectTo: null,
        };
        this.handleInputTag = this.handleInputTag.bind(this);
        this.handleInputContent = this.handleInputContent.bind(this);
        this.callToCreateQuestion = this.callToCreateQuestion.bind(this);
        this.displayFormNotification = this.displayFormNotification.bind(this);
        this.updateRedirectTo = this.updateRedirectTo.bind(this);
    }

    render() {
        if (this.state.redirectTo !== null) {
            return <Redirect to={this.state.redirectTo} />
        }

        let listCategories = SECTION_IMAGES.map(function (item) {
            return <option value={item.field_id}>{item.title}</option>
        });

        let tags = this.state.listTags.map(item => {
            return <Tag displayDelete={true} name={item} actionDelete={this.handleDeleteTags.bind(this)} />
        });

        return (
            <div className={'tm-db-ct-yq-frame'}>
                <input value={this.state.question.title} onChange={this.handleInputTitle.bind(this)} className={'tm-newqt-title'} type={'input'} placeholder={'Tiêu đề bài viết'} />
                <CKEditor
                    editor={ClassicEditor}
                    onInit={editor => {
                        // You can store the "editor" and use when it is needed.
                        console.log('Editor is ready to use!', editor);
                    }}
                    onChange={(event, editor) => {
                        this.handleInputContent(editor.getData())
                    }}
                // onChange={(event, editor) => {
                //     const data = editor.getData();
                //     console.log({ event, editor, data });
                //     this.handleInputContent.bind(this, data);
                // }}
                // onBlur={(event, editor) => {
                //     console.log('Blur.', editor);
                // }}
                // onFocus={(event, editor) => {
                //     console.log('Focus.', editor);
                // }}
                />
                <div className={'tm-newqt-category'}>
                    <div className={'tm-newqt-ctg-title'}>Danh mục</div>
                    <select onChange={this.handleSelectBox.bind(this)} className={'tm-newqt-select-box'}>
                        {listCategories}
                    </select>
                </div>
                <div className={'tm-newqt-setcoin'}>
                    <div className={'tm-newqt-coin-title'}>Số coin cho câu hỏi</div>
                    <input value={this.state.price} onChange={this.handleInputCoin.bind(this)} type={'number'} />
                </div>
                <div className={'tm-newqt-setcoin'}>
                    <div className={'tm-newqt-coin-title'}>Khả năng hiển thị</div>
                </div>
                <form className={'tm-db-gr-cg-form-frame'}>
                    <div className={'tm-flex-row-spacebtw-nowrap tm-margin-top10'}>
                        <input onClick={this.handleVisibility.bind(this)} type="radio" name="gender" value={QUESTION_VISIBILITY.ONLY_ME} />
                        <div><i class="fas fa-lock tm-db-gr-cg-privacy-icon" /></div>
                        <label className={'tm-db-gr-cg-radio-content'} for={QUESTION_VISIBILITY.ONLY_ME}>
                            <div className={'tm-db-gr-cg-privacy-title'}>Private</div>
                            Chỉ bạn mới có thể xem được câu hỏi
                        </label>
                    </div>
                    <div className={'tm-flex-row-spacebtw-nowrap tm-margin-top10'}>
                        <input onClick={this.handleVisibility.bind(this)} type="radio" name="gender" value={QUESTION_VISIBILITY.OPEN} />
                        <div><i class="fas fa-globe-europe tm-db-gr-cg-privacy-icon" /></div>
                        <label className={'tm-db-gr-cg-radio-content'} for={QUESTION_VISIBILITY.OPEN}>
                            <div className={'tm-db-gr-cg-privacy-title'}>Public</div>
                            Mọi người dùng trong hệ thống có thể xem được câu hỏi
                        </label>
                    </div>
                </form>
                <div className={'tm-newqt-tags'}>
                    <div className={'tm-newqt-ctg-title'}>Tags</div>
                    <div className={'tm-newqt-tags-list'}>
                        {tags}
                        <input
                            value={this.state.input}
                            type={'input'}
                            className={'tm-tag-frame tm-tag-input'}
                            placeholder={'Enter your tag'}
                            onKeyDown={this.handleEnter.bind(this)}
                            onChange={this.handleChange.bind(this)}
                        />
                    </div>
                </div>
                <div className={'tm-newqt-btn-frame'}>
                    <button onClick={this.handleCreateQuestion.bind(this)} className={'tm-btn tm-btn-success tm-float-right '}>Tạo</button>
                    <Link to={'/dashboard'} className={'tm-btn tm-btn-cancel tm-float-right '}>Hủy</Link>
                </div>
                {this.state.isLoading ? <FormLoading /> : ''}
                {this.state.isDisplayForm ? <FormNotification title={this.state.formContent.title} content={this.state.formContent.content} action={this.closeForm.bind(this)} /> : ''}
            </div>
        );
    }

    closeForm = () => {
        this.setState({
            isDisplayForm: false,
            formContent: {
                title: '',
                content: ''
            }
        })
    }

    handleCreateQuestion = () => {
        this.setState({
            isLoading: true,
        })

        this.callToCreateQuestion(this.updateRedirectTo, this.displayFormNotification);
    }

    callToCreateQuestion = (callback, callbackWhenFail) => {
        Axios.post(USER_SERVICE_DOMAIN + API_CREATE_QUESTION, {
            question_content: {
                title: this.state.question.title,
                content: this.state.question.content
            },
            price: this.state.price,
            visibility: this.state.visibility,
            field_id: this.state.fieldId,
            tags: this.state.listTags
        }, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 201) {
                callbackWhenFail('Thông báo', res.data.message, callback, res.data.data.question_id);
            } else {
                callbackWhenFail('Chú ý', res.data.message);
            }
        }).catch(error => {
            callbackWhenFail('Chú ý', error.message)
        })
    }

    displayFormNotification = (title, content, callback = null, questionId = null) => {
        this.setState({
            isLoading: false,
            isDisplayForm: true,
            formContent: {
                title: title,
                content: content
            }
        })

        if (callback !== null) {
            setTimeout(() => {
                callback('/question-detail/' + questionId);
            }, 2000);
        }
    }

    updateRedirectTo = (uri) => {
        this.setState({
            redirectTo: uri
        })
    }

    handleVisibility = (event) => {
        this.setState({
            visibility: event.target.value
        })
    }

    handleSelectBox = (event) => {
        this.setState({
            fieldId: event.target.value
        })
    }

    handleDeleteTags = (value) => {
        let arr = this.state.listTags;
        removeElementInArrayByValue(arr, value);

        this.setState({
            listTags: arr
        })
    }

    handleInputCoin = (event) => {
        this.setState({
            price: parseInt(event.target.value)
        })
    }

    handleInputContent = (data) => {
        let question = { ...this.state.question };
        question.content = data;
        this.setState({
            question: question
        });
    }

    handleInputTitle = (event) => {
        let question = { ...this.state.question };
        question.title = event.target.value;
        this.setState({
            question: question
        });
    }

    handleInputTag = () => {
        this.setState({
            listTags: this.state.listTags.concat([this.state.input]),
            input: ''
        })
    };

    handleEnter = (event) => {
        if (event.key === 'Enter') {
            this.handleInputTag(event);
        }
    };

    handleChange = (event) => {
        this.setState({
            input: event.target.value,
        })
    };
}

export default NewQuestion;