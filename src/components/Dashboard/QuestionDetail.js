import React from "react";
import QuestionContent from "./QuestionDetail/QuestionContent";
import QuestionAnswer from "./QuestionDetail/QuestionAnswer";
import QuestionComment from "./QuestionDetail/QuestionComment";
import CreateAnswer from "./QuestionDetail/CreateAnswer";
import FormAnswer from "./QuestionDetail/FormAnswer";
import Axios from "axios";
import {
    USER_SERVICE_DOMAIN, API_GET_QUESTION_DETAIL,
    API_GET_LIST_ANSWER_OF_QUESTION, API_CREATE_ANSWER, QUESTION_COMPONENT_TYPE
} from '../../constants';
import FormLoadingSmall from "../General/FormLoadingSmall";
import FormLoading from '../General/FormLoading';
import FormNotification from '../General/FormNotification';

class QuestionDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            form_visible: false,
            editor_index: 0,
            question_detail: null,
            answers: [],
            comments: [],
            inputComment: {
                isLoading: false,
                inputType: 1,
                content: '',
            },
            loadingStatus: {
                isLoadingQuestionDetail: true,
                isLoadingAnswers: true,
                isLoadingComment: true,
            },
            formInfo: {
                visible: false,
                title: '',
                content: '',
            },
            answerid: null,
        };
        this.toggleForm = this.toggleForm.bind(this);
        this.updateQuestionDetail = this.updateQuestionDetail.bind(this);
        this.updateAnswers = this.updateAnswers.bind(this);
        this.displayFormInfo = this.displayFormInfo.bind(this);
        this.callBackAddAnswer = this.callBackAddAnswer.bind(this);
        this.openFormAnswer = this.openFormAnswer.bind(this);
        this.closeFormAnswer = this.closeFormAnswer.bind(this);
    }

    render() {
        let questionComments = this.state.comments.map(comment => {
            return <QuestionComment comment={comment} isLoading={this.state.loadingStatus.isLoadingQuestionDetail} />;
        })

        let questionAnswers = this.state.answers.map(answer => {
            return <QuestionAnswer answer={answer} clickForm={this.openFormAnswer} isLoading={this.state.loadingStatus.isLoadingAnswers} />;
        })

        return (
            <div className={'tm-db-ct-yq-frame'}>
                <QuestionContent
                    questionDetail={this.state.question_detail}
                    commentCount={this.state.comments.length}
                    isLoading={this.state.loadingStatus.isLoadingQuestionDetail}
                    toggleEditorIndex={this.toggleEditorIndex.bind(this)}
                    updateContentQuestion={this.updateContentQuestion.bind(this)}
                />
                <div className={'tm-qd-ca-deco'} />
                <CreateAnswer
                    submitAnswer={this.handleCreateAnswer.bind(this)}
                    commentInfo={this.state.inputComment}
                    updateInputType={this.updateInputTypeOfInputComment.bind(this)}
                    updateContent={this.updateContentOfInputComment.bind(this)}
                    editorIndex={this.state.editor_index}
                />
                {this.state.loadingStatus.isLoadingAnswers ? <FormLoadingSmall style={{ marginTop: '10px' }} /> : questionAnswers}
                {this.state.loadingStatus.isLoadingComment ? <FormLoadingSmall style={{ marginTop: '10px' }} /> : questionComments}
                {this.state.form_visible ? <FormAnswer questionId={this.state.question_detail._id} answerId={this.state.answerId} closeForm={this.closeFormAnswer} groupId={this.props.groupId} /> : <div />}
                {this.state.inputComment.isLoading ? <FormLoading /> : ''}
                {this.state.formInfo.visible ? <FormNotification title={this.state.formInfo.title} content={this.state.formInfo.content} action={this.closeFormInfo.bind(this)} /> : ''}
            </div>
        );
    }

    componentDidMount = () => {
        this.callToGetQuestionDetail(this.props.questionId, this.updateQuestionDetail);
        this.callToGetAnswers(this.props.questionId, this.updateAnswers);
    }

    updateContentQuestion = () => {
        this.setState({
            loadingStatus: Object.assign(this.state.loadingStatus, { isLoadingQuestionDetail: true })
        })
        this.callToGetQuestionDetail(this.props.questionId, this.updateQuestionDetail);
    }

    openFormAnswer = (answerId) => {
        this.setState({
            answerId: answerId,
            editor_index: -1,
            form_visible: true
        })
    }

    closeFormAnswer = () => {
        this.setState({
            editor_index: 0,
            form_visible: false
        })
    }

    toggleEditorIndex = () => {
        this.setState({
            editor_index: this.state.editor_index === 0 ? -1 : 0
        })
    }

    toggleForm() {
        this.setState({
            editor_index: !this.state.form_visible ? -1 : 0,
            form_visible: !this.state.form_visible,
        });
    }

    callToGetAnswers = (questionId, callBack, callBackWhenFail = null) => {
        Axios.get(USER_SERVICE_DOMAIN + API_GET_LIST_ANSWER_OF_QUESTION + questionId, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                callBack(res.data.data);
            }
        }).catch(error => {
            console.log(error);
        })
    }

    updateAnswers = (data) => {
        this.setState({
            answers: data
        })
    }

    callToGetQuestionDetail = (questionId, callBack, callBackWhenFail = null) => {
        Axios.get(USER_SERVICE_DOMAIN + API_GET_QUESTION_DETAIL + questionId, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                callBack(res.data.data);
            }
        }).catch(error => {
            console.log(error);
        })
    }

    updateQuestionDetail = (data) => {
        this.setState({
            question_detail: {
                _id: data._id,
                user_id: data.user_id,
                user_name: data.user_name,
                price: data.price,
                visibility: data.visibility,
                status: data.status,
                upvotes: data.upvotes,
                downvotes: data.downvotes,
                field_id: data.field_id,
                group: {
                    group_id: data.group.group_id,
                    group_section_id: data.group.group_section_id
                },
                tags: data.tags,
                question_content: {
                    title: data.question_content.title,
                    content: data.question_content.content
                },
                updated_at: data.updated_at,
                created_at: data.created_at,
            },
            comments: data.comments,
            loadingStatus: {
                isLoadingQuestionDetail: false,
                isLoadingAnswers: false,
                isLoadingComment: false,
            },
        })
    }

    handleCreateAnswer = () => {
        let tempObj = this.state.inputComment;
        tempObj.isLoading = true;

        this.setState({
            inputComment: tempObj
        })

        this.callToAddAnswer(this.callBackAddAnswer);
    }

    callToAddAnswer = (callBack, callBackWhenFail = null) => {
        Axios.post(USER_SERVICE_DOMAIN + API_CREATE_ANSWER, {
            question_id: this.state.question_detail._id,
            content: this.state.inputComment.content,
            type: this.state.inputComment.inputType
        }, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 201) {
                callBack(this.state.inputComment.inputType);
            }
        }).catch(error => {
            console.log(error);
        })
    }

    callBackAddAnswer = (answerType) => {
        this.displayFormInfo('Thông báo', 'Thêm câu trả lời thành công');

        this.setState({
            inputComment: {
                isLoading: false,
                inputType: 1,
                content: '',
            },
        })

        if (answerType === 1) {
            this.callToGetAnswers(this.state.question_detail._id, this.updateAnswers);
        } else {
            this.callToGetQuestionDetail(this.state.question_detail._id, this.updateQuestionDetail);
        }
    }

    displayFormInfo = (title, content) => {
        this.setState({
            formInfo: {
                visible: true,
                title: title,
                content: content,
            }
        })
    }

    closeFormInfo = () => {
        this.setState({
            formInfo: {
                visible: false,
                title: '',
                content: '',
            }
        })
    }

    updateContentOfInputComment = (data) => {
        let tempObj = this.state.inputComment;
        tempObj.content = data;

        this.setState({
            inputComment: tempObj
        })
    }

    updateInputTypeOfInputComment = (event) => {
        let tempObj = this.state.inputComment;
        tempObj.inputType = event.target.value;

        this.setState({
            inputComment: tempObj
        })
    }
}

export default QuestionDetail;

// const example_question = {
//     _id: "5ecd359c4b87e05a256d7b22",
//     user_id: 1,
//     user_name: 'Nguyen Tien Manh',
//     price: 2000,
//     visibility: 1,
//     status: 1,
//     upvotes: 0,
//     downvotes: 0,
//     field_id: 5,
//     group: {
//         group_id: null,
//         group_section_id: null
//     },
//     tags: [
//         "English"
//     ],
//     question_content: {
//         title: "So sánh giữa hiện tại hoàn thành và hiện tại hoàn thành tiếp diễn",
//         content: "<p>So sánh giúp mình giữa hiện tại hoàn thành và hiện tại hoàn thành tiếp diễn với ạ.</p>"
//     },
//     updated_at: "2020-05-26T15:28:28.107000Z",
//     created_at: "2020-05-26T15:28:28.107000Z"
// };