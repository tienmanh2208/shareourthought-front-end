import React from "react";

class Footer extends React.Component {
    render() {
        return (
            <footer className={'tm-footer-frame'}>
                <div className={'tm-flex-column tm-footer-icon-frame'}>
                    <div className={'tm-footer-icon-row tm-flex-row-center'}>
                        <div className={'tm-footer-icon tm-footer-icon-fb'}><i className="fab fa-facebook-f"/></div>
                        <div className={'tm-footer-icon tm-footer-icon-twitter'}><i className="fab fa-twitter"/></div>
                        <div className={'tm-footer-icon tm-footer-icon-youtube'}><i className="fab fa-youtube"/></div>
                        <div className={'tm-footer-icon tm-footer-icon-ig'}><i className="fab fa-instagram"/></div>
                        <div className={'tm-footer-icon tm-footer-icon-mail'}><i className="far fa-envelope"/></div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;