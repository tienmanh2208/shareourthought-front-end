import React from "react";
import Infos from "./Header/Infos";
import Board from "./Header/Board";

class Header extends React.Component {
    render() {
        return (
            <div>
                <Infos isGlobal={this.props.isGlobal}/>
                <Board/>
            </div>
        )
    }
}

export default Header;