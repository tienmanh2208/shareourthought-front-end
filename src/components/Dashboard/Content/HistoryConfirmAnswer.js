import React from 'react';
import Axios from 'axios';
import {
    USER_SERVICE_DOMAIN, API_USER_HISTORY_CONFIRM_ANSWER, ANSWER_STATUS, CONFIRM_ANSWER_ACTION
} from '../../../constants';
import Pagination from '../../General/Pagination';
import FormLoadingSmall from '../../General/FormLoadingSmall';
import { Link } from 'react-router-dom';

class HistoryConfirmAnswer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetching: true,
            currentPage: 1,
            lastPage: 1,
            listHistories: []
        }
        this.updateHistories = this.updateHistories.bind(this);
        this.callApiFail = this.callApiFail.bind(this);
    }

    render() {
        if (this.state.listHistories.length === 0) {
            return '';
        }

        let histories = this.state.listHistories.map(history => {
            return <div className={'hca-frame'}>
                <div className={'hca-status'}>{generateHistory(history.current_status)}</div>
                <div className={'hca-action'}>{generateAction(history.action)}</div>
                <div className={'hca-time'}>{history.created_at}</div>
                <Link className={'hca-link'} to={'/question-detail/' + history.question_id}>Đi tới câu hỏi <i class="far fa-arrow-alt-circle-right"></i></Link>
            </div>
        })

        return (
            <div className={'tm-db-ct-yq-frame'}>
                <div className={'tm-db-ct-yq-title'}>Lịch sử đánh giá câu trả lời</div>
                <div className={'tm-flex-column'}>
                    <div className={'hca-frame'} style={{background: 'linear-gradient(to top, #2E456A, #3a5688)', color: 'white'}}>
                        <div className={'hca-status-header'}>Hành động</div>
                        <div className={'hca-action-header'}>Người thực hiện</div>
                        <div className={'hca-time-header'}>Thời gian</div>
                        <div className={'hca-link-header'}>Câu hỏi</div>
                    </div>
                    {this.state.isFetching ? <FormLoadingSmall /> : histories}
                </div>
                <Pagination index={this.state.currentPage} max={this.state.lastPage} switchPage={this.changeCurrentPage.bind(this)} />
            </div>
        )
    }

    componentDidMount() {
        this.callToGetHistoryConfirmAnswer(this.updateHistories, this.callApiFail)
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.currentPage !== prevState.currentPage) {
            this.callToGetHistoryConfirmAnswer(this.updateHistories, this.callApiFail)
        }
    }

    callToGetHistoryConfirmAnswer = (callBack, callBackWhenFail) => {
        Axios.get(USER_SERVICE_DOMAIN + API_USER_HISTORY_CONFIRM_ANSWER + '?page=' + this.state.currentPage, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                callBack(res.data.data);
            } else {
                console.log(res);
                callBackWhenFail();
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail();
        })
    }

    updateHistories = (data) => {
        this.setState({
            listHistories: data.data,
            lastPage: data.last_page,
            isFetching: false,
        })
    }

    callApiFail = () => {
        this.setState({
            isFetching: false,
        })
    }

    changeCurrentPage = (page) => {
        this.setState({
            currentPage: page
        })
    }
}

export default HistoryConfirmAnswer;

const generateHistory = (answerStatus) => {
    switch (answerStatus) {
        case ANSWER_STATUS.ACCEPTED:
            return 'Câu trả lời được chấp nhận';
        default:
            return 'Câu trả lời bị từ chối';
    }
}

const generateAction = (actionId) => {
    switch (actionId) {
        case CONFIRM_ANSWER_ACTION.BY_ADMIN:
            return 'Quản trị viên'
        default:
            return 'Người dùng'
    }
}

const exampleResponse = {
    "id": 1,
    "answer_id": "5ed7b0a6d348fe1aac65c492",
    "previous_status": 4,
    "current_status": 4,
    "user_id": 3,
    "action": 2,
    "answerer_id": 1,
    "created_at": "2020-06-30 19:33:49",
    "updated_at": "2020-06-30 19:33:49",
    "question_id": "5ed0d8e275f4260f552d3392"
}