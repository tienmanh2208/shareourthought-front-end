import React from "react";
import { Link } from "react-router-dom";

class CreateQuestion extends React.Component {
    render() {
        return (
            <div className={'tm-db-ct-yq-frame'}>
                <div className={'tm-db-newqt-title'}>Bạn muốn đặt câu hỏi không?</div>
                <div className={'tm-flex-row-center'}>
                    <Link to={'/question'} className={'tm-db-newqt-btn'}>Đặt câu hỏi</Link>
                </div>
            </div>
        );
    }
}

export default CreateQuestion;