import React from "react";
import {
    USER_SERVICE_DOMAIN,
    API_GLOBAL_GET_LIST_FIELDS,
    BASE_URL_IMAGE
} from '../../../constants.js';
import FormLoadingSmall from '../../General/FormLoadingSmall';
import Axios from "axios";
import { getImageOfField, getNameOfField } from "../../../FunctionConstants.js";
import { Link } from "react-router-dom";

class Topics extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetching: true,
            listField: []
        }
    }

    render() {
        let totalQuestion = 0;

        let content = this.state.listField.map(function (value) {
            totalQuestion += value.question_count;

            return (
                <div key={'topics' + value.id} className={'tm-db-topic-catalogue tm-flex-row-center-nowrap'}>
                    <div className={'tm-db-tp-ctl-icon'}>
                        <img className={'tm-img-tp-ctl-icon'} src={getImageOfField(value.id)} alt={value.name} />
                    </div>
                    <div className={'tm-db-topic-ctl-content tm-flex-column'}>
                        <div className={'tm-db-topic-ctl-ct-title'}>{getNameOfField(value.id)}</div>
                        <div className={'tm-db-topic-ctl-ct-quantity'}>{value.question_count} câu hỏi</div>
                    </div>
                </div>
            );
        });

        let searchButton = (
            <div key={'search-question'} className={'tm-db-topic-catalogue tm-flex-row-center-nowrap'}>
                <div className={'tm-db-tp-ctl-icon'}>
                    <img className={'tm-img-tp-ctl-icon'} src={BASE_URL_IMAGE + '/icon/search.svg'} alt={'Search'} />
                </div>
                <Link to={this.props.page === 'global-dashboard' ? '/global/find-question' : '/dashboard-search'} className={'tm-db-topic-ctl-content tm-flex-column'}>
                    <div className={'tm-db-topic-ctl-ct-title'}>Tìm kiếm</div>
                    <div className={'tm-db-topic-ctl-ct-quantity'}>{totalQuestion} câu hỏi</div>
                </Link>
            </div>
        );

        return (
            <div className={'tm-db-ct-yq-frame'} style={this.props.page === 'global-dashboard' ? { margin: 'unset', width: '80%' } : {}}>
                <div className={'tm-db-ct-topics-title'}>Topics</div>
                <div className={'tm-flex-row-spacebtw-wrap'}>
                    {this.state.isFetching ? <FormLoadingSmall /> : content}
                </div>
                <div className={'tm-flex-row-center'}>
                    {searchButton}
                </div>
            </div>
        );
    }

    componentDidMount = () => {
        Axios.get(USER_SERVICE_DOMAIN + API_GLOBAL_GET_LIST_FIELDS).then(res => {
            if (res.data.code === 200) {
                this.setState({
                    isFetching: false,
                    listField: res.data.data
                })
            } else {
                console.log(res);
            }
        }).catch(error => {
            console.log(error);
        })
    }
}

export default Topics;

// const exampleResponse = [
//     {
//         "id": 1,
//         "name": "math",
//         "question_count": 0,
//         "created_at": "2020-03-21 07:20:37",
//         "updated_at": "2020-03-21 07:20:37"
//     },
//     {
//         "id": 2,
//         "name": "physic",
//         "question_count": 0,
//         "created_at": "2020-03-21 07:20:43",
//         "updated_at": "2020-03-21 07:20:43"
//     },
//     {
//         "id": 3,
//         "name": "chemistry",
//         "question_count": 3,
//         "created_at": "2020-03-21 07:20:49",
//         "updated_at": "2020-05-29 07:47:55"
//     },
//     {
//         "id": 4,
//         "name": "economic",
//         "question_count": 0,
//         "created_at": "2020-03-21 07:20:56",
//         "updated_at": "2020-03-21 07:20:56"
//     },
//     {
//         "id": 5,
//         "name": "english",
//         "question_count": 0,
//         "created_at": "2020-03-21 07:21:00",
//         "updated_at": "2020-03-21 07:21:00"
//     },
//     {
//         "id": 6,
//         "name": "languages",
//         "question_count": 0,
//         "created_at": "2020-03-21 07:21:08",
//         "updated_at": "2020-03-21 07:21:08"
//     },
//     {
//         "id": 7,
//         "name": "backend",
//         "question_count": 0,
//         "created_at": "2020-03-21 07:21:14",
//         "updated_at": "2020-03-21 07:21:14"
//     },
//     {
//         "id": 8,
//         "name": "frontend",
//         "question_count": 0,
//         "created_at": "2020-03-21 07:21:20",
//         "updated_at": "2020-03-21 07:21:20"
//     },
//     {
//         "id": 9,
//         "name": "other",
//         "question_count": 0,
//         "created_at": "2020-03-21 07:21:25",
//         "updated_at": "2020-03-21 07:21:25"
//     }
// ];