import React from 'react';
import Question from "../Question";
import Topics from "../Topics";
import Pagination from '../../General/Pagination';
import Axios from 'axios';
import { USER_SERVICE_DOMAIN, API_GET_ALL_QUESTION_OF_USER, API_USER_GET_LIST_FIELD } from '../../../constants';
import { getNameOfField } from '../../../FunctionConstants';

class YourQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            currentPage: 1,
            lastPage: 1,
            currentField: 0,
            listQuestion: [],
            listField: []
        }
        this.updateListQuestion = this.updateListQuestion.bind(this);
        this.updateListField = this.updateListField.bind(this);
    }

    render() {
        let questions = this.state.listQuestion.map(question => {
            return <Question questionInfo={question} />
        })

        return (
            <div className={'tm-db-ct-yq-frame'}>
                <div className={'tm-db-ct-yq-title'}>Câu hỏi của bạn</div>
                <Topics listField={this.state.listField} currentField={this.state.currentField} changeField={this.changeCurrentField.bind(this)} />
                <div className={'tm-flex-row-spacebtw-wrap'}>
                    {questions}
                </div>
                <Pagination index={this.state.currentPage} max={this.state.lastPage} switchPage={this.changeCurrentPage.bind(this)} />
            </div>
        );
    }

    componentDidUpdate = (prevProps, prevState) => {
        if (this.state.currentField !== prevState.currentField || this.state.currentPage !== prevState.currentPage) {
            this.callToGetListQuestion(this.state.currentPage, this.state.currentField, this.updateListQuestion);
        }
    }

    componentDidMount() {
        this.callToGetListField(this.updateListField);
        this.callToGetListQuestion(this.state.currentPage, this.state.currentField, this.updateListQuestion);
    }

    callToGetListQuestion = (page, fieldId, callBack) => {
        let fieldIdInfo = fieldId !== 0 ? '&field_id=' + fieldId : '';

        Axios.get(USER_SERVICE_DOMAIN + API_GET_ALL_QUESTION_OF_USER + '?page=' + page + fieldIdInfo, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                callBack(res.data.data);
            } else {
                console.log(res.data);
            }
        }).catch(error => {
            console.log(error);
        })
    }

    callToGetListField = (callBack) => {
        Axios.get(USER_SERVICE_DOMAIN + API_USER_GET_LIST_FIELD, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                let fieldAll = [0];
                let listFieldId = fieldAll.concat(res.data.data);
                let listField = listFieldId.map(fieldId => {
                    return {
                        fieldId: fieldId,
                        name: getNameOfField(fieldId)
                    }
                })

                callBack(listField);
            } else {
                console.log(res.data);
            }
        }).catch(error => {
            console.log(error);
        })
    }

    updateListField = (data) => {
        this.setState({
            listField: data
        })
    }

    updateListQuestion = (data) => {
        this.setState({
            listQuestion: data.data,
            lastPage: data.last_page
        })
    }

    changeCurrentPage = (pageNumber) => {
        this.setState({
            currentPage: pageNumber
        })
    }

    changeCurrentField = (fieldId) => {
        this.setState({
            currentField: fieldId,
            currentPage: 1,
        })
    }
}

export default YourQuestion;

// const responseQuestion = {
//     "_id": "5ec8ae112c539a3b0d473dc3",
//     "user_id": 1,
//     "price": 5000,
//     "visibility": 1,
//     "status": 1,
//     "upvotes": 0,
//     "downvotes": 0,
//     "field_id": 1,
//     "group": {
//         "group_id": null,
//         "group_section_id": null
//     },
//     "tags": [
//         "Math"
//     ],
//     "comments": [],
//     "question_content": {
//         "title": "Cách giải phương trình bậc 4 trùng phương",
//         "content": "<p>Cách giải tổng quát phương trình bậc 4 trùng phương là gì ?</p>"
//     },
//     "updated_at": "2020-05-23T05:01:05.637000Z",
//     "created_at": "2020-05-23T05:01:05.637000Z"
// };