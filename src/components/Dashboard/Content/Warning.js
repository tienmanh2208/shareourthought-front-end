import React from 'react';
import Axios from 'axios';
import {
    USER_SERVICE_DOMAIN, ANSWER_STATUS, CONFIRM_ANSWER_ACTION, API_USER_GET_WARNING, API_USER_COMPENSATE
} from '../../../constants';
import Pagination from '../../General/Pagination';
import FormLoadingSmall from '../../General/FormLoadingSmall';
import { Link } from 'react-router-dom';
import FormLoading from '../../General/FormLoading';
import FormNotification from '../../General/FormNotification';

class Warning extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            isFetching: true,
            currentPage: 1,
            lastPage: 1,
            listWarning: [],
            formInfo: {
                isDisplaying: false,
                title: '',
                content: '',
            }
        }
        this.callToGetListWarning = this.callToGetListWarning.bind(this);
        this.callApiFail = this.callApiFail.bind(this);
    }

    render() {
        if (this.state.listWarning.length === 0) {
            return '';
        }

        let warnings = this.state.listWarning.map(warning => {
            return <div className={'hca-frame'}>
                <div className={'hca-status'}>Đánh giá câu trả lời sai</div>
                <div className={'hca-action'}>Quản trị viên</div>
                <div onClick={this.compensate.bind(this, warning.id, warning.question_id)} className={'hca-make-up'}>Đền bù <i class="fas fa-sync-alt"></i></div>
                <Link className={'hca-link'} to={'/question-detail/' + warning.question_id}>Đi tới câu hỏi <i class="far fa-arrow-alt-circle-right"></i></Link>
            </div>
        })

        return (
            <div className={'tm-db-ct-yq-frame'}>
                <div className={'tm-db-ct-yq-title'}>Cảnh báo</div>
                <div className={'tm-flex-column'}>
                    <div className={'hca-frame'} style={{ background: 'linear-gradient(to top, #2E456A, #3a5688)', color: 'white' }}>
                        <div className={'hca-status-header'}>Lý do</div>
                        <div className={'hca-action-header'}>Xác nhận bởi</div>
                        <div className={'hca-time-header'}>Hành động</div>
                        <div className={'hca-link-header'}>Câu hỏi</div>
                    </div>
                    {this.state.isFetching ? <FormLoadingSmall /> : warnings}
                </div>
                <Pagination index={this.state.currentPage} max={this.state.lastPage} switchPage={this.changeCurrentPage.bind(this)} />
                {this.state.isLoading ? <FormLoading /> : ''}
                {this.state.formInfo.isDisplaying ? <FormNotification title={this.state.formInfo.title} content={this.state.formInfo.content} action={this.closeForm.bind(this)} /> : ''}
            </div>
        )
    }

    componentDidMount() {
        this.callToGetListWarning(this.updateListWarning, this.callApiFail)
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.currentPage !== prevState.currentPage) {
            this.callToGetListWarning(this.updateListWarning, this.callApiFail)
        }
    }

    compensate = (warningId, questionId) => {
        this.setState({
            isLoading: true
        })
        this.callToCompensateAnswer(warningId, questionId, this.compensateSuccessfully, this.compensateFailed);
    }

    callToCompensateAnswer = (warningId, questionId, callBack, callBackWhenFail) => {
        Axios.post(USER_SERVICE_DOMAIN + API_USER_COMPENSATE, {
            warning_id: warningId,
            question_id: questionId
        }, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 203) {
                callBack(res.data.message);
            } else {
                console.log(res);
                callBackWhenFail(res.data.message);
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail('Không thể đền bù');
        })
    }

    compensateSuccessfully = (message) => {
        this.setState({
            isLoading: false,
            formInfo: {
                isDisplaying: true,
                title: 'Thông báo',
                content: message,
            },
            isFetching: true,
        })

        setTimeout(this.callToGetListWarning(this.updateListWarning, this.callApiFail), 4000);
    }

    compensateFailed = (message) => {
        this.setState({
            isLoading: false,
            formInfo: {
                isDisplaying: true,
                title: 'Thông báo',
                content: message,
            },
        })
    }

    closeForm = () => {
        this.setState({
            formInfo: {
                isDisplaying: false,
                title: '',
                content: '',
            },
        })
    }

    callToGetListWarning = (callBack, callBackWhenFail) => {
        Axios.get(USER_SERVICE_DOMAIN + API_USER_GET_WARNING + '?page=' + this.state.currentPage, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                callBack(res.data.data);
            } else {
                console.log(res);
                callBackWhenFail();
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail();
        })
    }

    updateListWarning = (data) => {
        this.setState({
            listWarning: data.data,
            lastPage: data.last_page,
            isFetching: false,
        })
    }

    callApiFail = () => {
        this.setState({
            isFetching: false,
        })
    }

    changeCurrentPage = (page) => {
        this.setState({
            currentPage: page
        })
    }
}

export default Warning;

const exampleResponse = {
    "id": 1,
    "user_id": 2,
    "warning_type": 1,
    "reporter_id": 3,
    "question_id": "question_id_test",
    "answer_id": "answer_id_test",
    "comment_id": null,
    "created_at": "2020-06-30 19:34:48",
    "updated_at": "2020-06-30 19:34:48"
}