import React from "react";
import Question from "../Question";
import Topics from "../Topics";
import Pagination from "../../General/Pagination";
import Axios from "axios";
import { USER_SERVICE_DOMAIN, API_GET_TOP_FIELDS, API_GET_NEWEST_QUESTIONS } from "../../../constants";
import { getNameOfField } from '../../../FunctionConstants';

class NewestQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            currentPage: 1,
            lastPage: 1,
            currentField: 0,
            listField: [],
            listQuestion: []
        }
        this.changeListField = this.changeListField.bind(this);
        this.updateListQuestion = this.updateListQuestion.bind(this);
    }

    render() {
        let content = this.state.listQuestion.map(questionInfo => {
            return <Question questionInfo={questionInfo} />;
        });

        return (
            <div className={'tm-db-ct-yq-frame'} style={this.props.page === 'global-dashboard' ? { margin: 'unset', width: '80%' } : {}}>
                <div className={'tm-db-ct-yq-title'}>Câu hỏi mới nhất</div>
                <Topics currentField={this.state.currentField} listField={this.state.listField} changeField={this.changeField.bind(this)} />
                <div className={'tm-flex-row-spacebtw-wrap'}>
                    {content}
                </div>
                <Pagination index={this.state.currentPage} max={this.state.lastPage} switchPage={this.changeCurrentPage.bind(this)} />
            </div>
        );
    }

    componentDidUpdate = (prevProps, prevState) => {
        if (this.state.currentField !== prevState.currentField || this.state.currentPage !== prevState.currentPage) {
            this.getNewestQuestion(this.state.currentPage, this.state.currentField, this.updateListQuestion);
        }
    }

    componentDidMount = () => {
        this.getNewestQuestion(this.state.currentPage, this.state.currentField, this.updateListQuestion);
        this.getTopField(this.changeListField);
    }

    getNewestQuestion = (page, fieldId, callBack) => {
        let fieldIdInfo = fieldId !== 0 ? '&field_id=' + fieldId : '';

        Axios.get(USER_SERVICE_DOMAIN + API_GET_NEWEST_QUESTIONS + '?page=' + page + fieldIdInfo).then(res => {
            if (res.data.code === 200) {
                callBack(res.data.data);
            }
        }).catch(error => {
            console.log('Fail when fetching data form top fields');
            console.log(error);
        })
    }

    updateListQuestion = (dataListQuestion) => {
        this.setState({
            lastPage: dataListQuestion.last_page,
            listQuestion: dataListQuestion.data,
        })
    }

    getTopField = (callBack) => {
        Axios.get(USER_SERVICE_DOMAIN + API_GET_TOP_FIELDS).then(res => {
            if (res.data.code === 200) {
                let convertedData = res.data.data.map(info => {
                    return {
                        name: getNameOfField(info.id),
                        fieldId: info.id
                    };
                });

                convertedData.unshift({
                    name: 'Tất cả',
                    fieldId: 0
                })

                callBack(convertedData);
            }
        }).catch(error => {
            console.log('Fail when fetching data form top fields');
            console.log(error);
        })
    }

    changeListField = (listField) => {
        this.setState({
            listField: listField
        })
    }

    changeField = (fieldId) => {
        this.setState({
            currentField: fieldId,
            currentPage: 1
        })
    }

    changeCurrentPage = (pageNumber) => {
        this.setState({
            currentPage: pageNumber
        })
    }
}

export default NewestQuestion;

// const questionInfo = {
//     title: 'This is question',
//     content: 'Trung khắc họa đời sống thường nhật của người dân ở miền Nam nước Mỹ. Một gia đình nhỏ trong một chuyến đi đến Florida; hai ông cháu từ miền quê lần đầu đi tàu ra thành phố, lần đầu nhìn thấy những người da màu; một anh chàng bán Kinh thánh rong nay đây mai đó; một gia đình người tị nạn đến giúp việc ở một trang trại nhỏ… Những câu chuyện rất đỗi bình thường ấy lại ẩn chứa những kịch tính bất ngờ, nơi yếu tố bạo lực làm rúng động đức tin, nơi sự tàn nhẫn của con người sinh ra từ mông muội, nơi cái ác được thực thi một cách thản nhiên do những ghen tị nhỏ nhen mù quáng…',
//     created_at: '2020-03-02',
//     view: 2310,
//     replied: 2,
//     upvote: 0,
//     status: 0
// };

// const responseQuestion = {
//     "_id": "5ec8ae112c539a3b0d473dc3",
//     "user_id": 1,
//     "price": 5000,
//     "visibility": 1,
//     "status": 1,
//     "upvotes": 0,
//     "downvotes": 0,
//     "field_id": 1,
//     "group": {
//         "group_id": null,
//         "group_section_id": null
//     },
//     "tags": [
//         "Math"
//     ],
//     "comments": [],
//     "question_content": {
//         "title": "Cách giải phương trình bậc 4 trùng phương",
//         "content": "<p>Cách giải tổng quát phương trình bậc 4 trùng phương là gì ?</p>"
//     },
//     "updated_at": "2020-05-23T05:01:05.637000Z",
//     "created_at": "2020-05-23T05:01:05.637000Z"
// };