import React from "react";
import Header from "./Header";
import Content from "./Content";
import Footer from "./Footer";
import './css/dashboard.css';
import './css/group.css';

class MainPage extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <Content type={this.props.type}/>
                <Footer/>
            </div>
        );
    }
}

export default MainPage;