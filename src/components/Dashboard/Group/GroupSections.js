import React from 'react';
import FormJoinGroup from '../Group/FormJoinGroup';
import { Link } from 'react-router-dom';
import { BASE_URL_IMAGE, USER_SERVICE_DOMAIN, API_GROUP_GET_LIST_SECTIONS } from '../../../constants.js';
import { randomInt, getImageOfField } from '../../../FunctionConstants';
import FormLoadingSmall from '../../General/FormLoadingSmall';
import Axios from 'axios';

class GroupSections extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectTo: '',
            displayFormJoinGroup: false,
            listSections: [],
            isFetching: true,
        }
    }

    handleRedirect(route) {
        this.setState({
            redirectTo: route,
        })
    }

    toggleForm = () => {
        this.setState({
            displayFormJoinGroup: !this.state.displayFormJoinGroup
        })
    }

    render() {
        // if (this.state.redirectTo !== '') {
        //     return <Redirect to={this.state.redirectTo} />
        // }

        let content = this.state.listSections.map(generateGroups);

        return (
            <div className={'tm-sidebar-frame'}>
                <div className={'tm-sidebar-right-title'}>Sections</div>
                <div className={'tm-sidebar-right-deco'} />
                {this.state.isFetching ? <FormLoadingSmall style={{ marginTop: '10px' }} /> : content}
                <button onClick={this.toggleForm.bind(this)} className={'tm-btn tm-btn-warning'} style={{ width: '100%', margin: '5px 0' }}><i class='fas fa-cog'></i> Setting</button>
                {this.state.displayFormJoinGroup ? <FormJoinGroup closeForm={this.toggleForm.bind(this)} /> : ''}
            </div>
        );
    }

    componentDidMount() {
        Axios.get(USER_SERVICE_DOMAIN + API_GROUP_GET_LIST_SECTIONS + '?group_id=' + this.props.groupId, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                let listSection = res.data.data.map(section => {
                    return Object.assign(section, {
                        avatar: getImageOfField(randomInt(1, 9))
                    })
                })

                this.setState({
                    isFetching: false,
                    listSections: listSection
                })
            } else {
                console.log('Error when fetching data of group sections');
                console.log(res.data);
            }
        }).catch(error => {
            console.log('Error when fetching data of group sections');
            console.log(error);
        })
    }
}

function generateGroups(groupInfo) {
    return (
        <div key={'sidebar-group-' + groupInfo.id} className={'tm-float-clear tm-margin-10'}>
            <img className={'tm-topuser-img'} src={groupInfo.avatar} alt={'Avatar'} />
            <Link to={'/group-dashboard/' + groupInfo.group_infos_id} className={'tm-topuser-name'}>{groupInfo.name}</Link>
        </div>
    );
}

export default GroupSections;

// const GROUP_INFOS = [
//     {
//         name: 'Giải tích 1',
//         avatar: BASE_URL_IMAGE + '/icon/double-decker.svg'
//     },
//     {
//         name: 'Giải tích 2',
//         avatar: BASE_URL_IMAGE + '/icon/gears.svg'
//     }
// ];

// const exampleResponse = [
//     {
//         "id": 1,
//         "group_infos_id": 5,
//         "name": "Team 01",
//         "description": "This section does not have description yet"
//     },
//     {
//         "id": 2,
//         "group_infos_id": 5,
//         "name": "Team 02",
//         "description": "This section does not have description yet"
//     },
//     {
//         "id": 3,
//         "group_infos_id": 5,
//         "name": "Team 03",
//         "description": "This section does not have description yet"
//     }
// ];