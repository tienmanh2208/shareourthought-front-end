import React from "react";
import Question from "../Question";
import Pagination from "../../General/Pagination";
import {
    USER_SERVICE_DOMAIN,
    API_GROUP_GET_LIST_QUESTION,
    QUESTION_COMPONENT_TYPE
} from '../../../constants';
import Axios from "axios";

class GroupNewestQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: 1,
            lastPage: 10,
            listQuestion: [],
        }
        this.updateListQuestion = this.updateListQuestion.bind(this);
    }

    render() {
        let content = this.state.listQuestion.map(questionInfo => {
            return <Question questionInfo={questionInfo} style={{ width: '100%' }} componentType={QUESTION_COMPONENT_TYPE.GROUP} />;
        });

        return (
            <div style={{ clear: 'both' }}>
                <div className={'tm-group-area-title'}>Câu hỏi mới nhất</div>
                <div className={'tm-flex-row-spacebtw-wrap'}>
                    {content}
                </div>
                <Pagination index={this.state.currentPage} max={this.state.lastPage} switchPage={this.changeCurrentPage.bind(this)} />
            </div>
        );
    }

    componentDidMount() {
        Axios.get(USER_SERVICE_DOMAIN + API_GROUP_GET_LIST_QUESTION + this.props.groupId, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                this.updateListQuestion(res.data.data);
            } else {
                console.log(res.data);
            }
        }).catch(error => {
            console.log(error);
        })
    }

    updateListQuestion = (data) => {
        this.setState({
            listQuestion: data.data,
            lastPage: data.last_page,
        })
    }

    changeCurrentPage = (nextPage) => {
        this.setState({
            currentPage: nextPage
        })
    }
}

export default GroupNewestQuestion;

// const questionInfo = {
//     "_id": "5ec8ae112c539a3b0d473dc3",
//     "user_id": 1,
//     "price": 5000,
//     "visibility": 1,
//     "status": 1,
//     "upvotes": 0,
//     "downvotes": 0,
//     "field_id": 1,
//     "group": {
//         "group_id": null,
//         "group_section_id": null
//     },
//     "tags": [
//         "Math"
//     ],
//     "comments": [],
//     "question_content": {
//         "title": "Cách giải phương trình bậc 4 trùng phương",
//         "content": "<p>Cách giải tổng quát phương trình bậc 4 trùng phương là gì ?</p>"
//     },
//     "updated_at": "2020-05-23T05:01:05.637000Z",
//     "created_at": "2020-05-23T05:01:05.637000Z"
// };