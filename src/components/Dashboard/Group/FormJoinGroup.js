import React from "react";
import ReactLoading from 'react-loading';
import FormLoading from '../../General/FormLoading';
import FormNotification from '../../General/FormNotification';
import { BASE_URL_IMAGE, USER_SERVICE_DOMAIN, API_GROUP_GET_INFO_BY_INVITED_KEY, API_GROUP_JOIN_BY_INVITED_KEY } from '../../../constants.js';
import Axios from "axios";
import { Redirect } from "react-router";

class FormJoinGroup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchStatus: SEARCH_STATUS.typing,
            input: '',
            searchedInfo: {},
            visibleForm: false,
            warning: 'Thông tin nhóm sẽ được hiển thị ở đây',
            isLoading: false,
            formInfo: {
                needDisplay: false,
                title: '',
                content: '',
                action: null,
            },
            redirectTo: null,
        };
        this.updateWarningTextWhenGroupNotFound = this.updateWarningTextWhenGroupNotFound.bind(this);
        this.updateGroupInfo = this.updateGroupInfo.bind(this);
        this.callToGetGroupInfo = this.callToGetGroupInfo.bind(this);
        this.joinGroupFail = this.joinGroupFail.bind(this);
        this.joinGroupSuccess = this.joinGroupSuccess.bind(this);
    }

    render() {
        if (this.state.formInfo.needDisplay) {
            return <FormNotification title={this.state.formInfo.title} content={this.state.formInfo.content} action={this.closeFormNotification.bind(this)} />
        }

        if (this.state.redirectTo !== null) {
            return <Redirect to={this.state.redirectTo} />
        }

        return (
            <div className={'tm-form-container'}>
                <div className={'tm-form-frame'} style={this.state.visibleForm ? { width: '70%', maxWidth: '400px', opacity: 1 }
                    : { width: '70%', maxWidth: '400px' }}>
                    <div className={'tm-flex-row-spacebtw-nowrap'}>
                        <input value={this.state.input} onChange={this.handleInput.bind(this)} onKeyDown={this.handleEnter.bind(this)} className={'tm-gr-jg-search'} placeholder={'Type name or key to join group'} />
                        <button onClick={this.handleSearch.bind(this)} className={'tm-btn tm-btn-warning'} style={{ padding: '0 5px', margin: '10px 0 10px 10px', height: '28px', width: '25%' }}>Search</button>
                    </div>
                    <div className={'tm-form-content'}>
                        {
                            this.state.searchStatus === SEARCH_STATUS.typing ? this.waitingFrame(this.state.warning)
                                : this.state.searchStatus === SEARCH_STATUS.searching ? <LoadingPage />
                                    : <SearchedGroupInfo info={this.state.searchedInfo} />
                        }
                    </div>
                    <div className={'tm-flex-row-reverse'}>
                        <button onClick={this.handleJoin.bind(this)} className={'tm-btn tm-btn-success'}>Join</button>
                        <button onClick={this.props.closeForm} className={'tm-btn tm-btn-cancel'}>Cancel</button>
                    </div>
                    {this.state.isLoading ? <FormLoading /> : ''}
                </div>
            </div>
        );
    }

    handleJoin = () => {
        this.setState({
            isLoading: true,
        })

        this.callToJoinGroup(this.joinGroupSuccess, this.joinGroupFail);
    }

    callToJoinGroup = (callBack, callBackWhenFail) => {
        Axios.post(USER_SERVICE_DOMAIN + API_GROUP_JOIN_BY_INVITED_KEY, {
            invited_key: this.state.input
        }, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 203) {
                callBack(res.data.message);
            } else {
                callBackWhenFail(res.data.message);
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail("Lỗi hệ thống");
        })
    }

    joinGroupSuccess = (message) => {
        this.setState({
            isLoading: false,
            formInfo: {
                needDisplay: true,
                title: 'Thông báo',
                content: message,
            },
            redirectTo: '/group-dashboard/' + this.state.searchedInfo.id,
        })
    }

    joinGroupFail = (message) => {
        this.setState({
            isLoading: false,
            formInfo: {
                needDisplay: true,
                title: 'Thông báo',
                content: message,
            },
        })
    }

    handleInput = (e) => {
        this.setState({
            input: e.target.value
        });
    }

    handleEnter = (e) => {
        if (e.key === 'Enter') {
            this.setState({
                searchStatus: SEARCH_STATUS.searching
            });

            this.callToGetGroupInfo(this.updateGroupInfo, this.updateWarningTextWhenGroupNotFound);
        }
    }

    handleSearch = () => {
        this.setState({
            searchStatus: SEARCH_STATUS.searching
        });

        this.callToGetGroupInfo(this.updateGroupInfo, this.updateWarningTextWhenGroupNotFound);
    }

    callToGetGroupInfo = (callback, callBackWhenFail) => {
        Axios.get(USER_SERVICE_DOMAIN + API_GROUP_GET_INFO_BY_INVITED_KEY + '?invitedKey=' + this.state.input, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                res.data.data.avatar = BASE_URL_IMAGE + '/icon/double-decker.svg';
                callback(res.data.data);
            } else {
                console.log(res.data);
                callBackWhenFail(res.data.message);
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail("Lỗi hệ thống");
        })
    }

    updateGroupInfo = (groupInfo) => {
        this.setState({
            searchedInfo: groupInfo,
            searchStatus: SEARCH_STATUS.done
        })
    }

    updateWarningTextWhenGroupNotFound = (text) => {
        this.setState({
            searchStatus: SEARCH_STATUS.typing,
            warning: text
        })
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                visibleForm: true,
            })
        }, 200);
    }

    closeFormNotification = () => {
        this.setState({
            formInfo: {
                needDisplay: false,
                title: '',
                content: '',
            },
        })
    }

    waitingFrame = (data) => {
        return (
            <div className={'tm-waiting-frame'}>
                {data}
            </div>
        );
    }
}

const SearchedGroupInfo = (props) => {
    return (
        <div className={'tm-sg-result'}>
            <img className={'tm-sg-result-avatar'} src={props.info.avatar} alt={'Avatar'} />
            <div className={'tm-sg-result-content'}>
                <div className={'tm-sg-result-grName'}>{props.info.title}</div>
                <div className={'tm-sg-result-detail'}>
                    <i className="fas fa-users" />
                    <span className={'tm-sg-result-detail-bold'}>{props.info.total_member}</span>
                    Created by: <span className={'tm-sg-result-detail-bold'}>{props.info.creator_name}</span>
                </div>
            </div>
        </div>
    );
}

const LoadingPage = () => {
    return (
        <div className={'tm-sg-loading-frame'}>
            <ReactLoading type={'spin'} color={'gray'} height={30} width={30} />
        </div>
    );
}

const SEARCH_STATUS = {
    typing: 0,
    searching: 1,
    done: 2
}

export default FormJoinGroup;

// const exampleResponse = {
//     id: 2,
//     creator: 1,
//     group_id: "tm_advanced_math",
//     title: "Advanced Math",
//     default_coin: 2000,
//     privacy: 2,
//     key: "11591346273",
//     description: "",
//     created_at: "2020-03-23 09:01:27",
//     updated_at: "2020-06-05 08:37:52",
//     total_member: 20,
//     creator_name: "abc"
// };