import React from 'react';
import GroupNewestQuestion from './GroupNewestQuestion';
import GroupDashboardHint from './GroupDashboardHint';
import GroupAskCreateQuestion from './GroupAskCreateQuestion';

class GroupDashboard extends React.Component {
    render() {
        return (
            <div className={'tm-db-ct-yq-frame'}>
                <GroupAskCreateQuestion groupId={this.props.groupId} />
                <GroupNewestQuestion groupId={this.props.groupId} />
                <GroupDashboardHint groupId={this.props.groupId} />
            </div>
        )
    }
}

export default GroupDashboard;