import React from "react";
import GroupFormCreateQuestion from "./GroupFormCreateQuestion";

class GroupAskCreateQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formVisible: false,
        }
    }

    toggleForm() {
        this.setState({
            formVisible: !this.state.formVisible,
        });
    }

    render() {
        return (
            <div className={'tm-background-radius-20'}>
                <div className={'tm-db-newqt-title'}>Want to ask something?</div>
                <div className={'tm-flex-row-center'}>
                    <div onClick={this.toggleForm.bind(this)} className={'tm-db-newqt-btn'}>Write a new quetion</div>
                </div>
                {this.state.formVisible ? <GroupFormCreateQuestion groupId={this.props.groupId} toggleForm={this.toggleForm.bind(this)} /> : ''}
            </div>
        );
    }
}

export default GroupAskCreateQuestion;