import React from "react";
import { BASE_URL_IMAGE, USER_SERVICE_DOMAIN, API_GROUP_GET_GROUP_DETAIL_INFO } from '../../../constants.js';
import { getPrivacyOfGroup } from '../../../FunctionConstants.js';
import FormLoadingSmall from '../../General/FormLoadingSmall';
import Axios from "axios";
import { Link, Redirect } from "react-router-dom";

class GroupInfoDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            invitedKey: randomInt(10000000, 99999999),
            refreshInvitedKey: false,
            isFetching: true,
            groupInfo: null,
            redirectTo: null,
        }
    }

    handleRefreshKey = () => {
        this.setState({
            refreshInvitedKey: true,
            invitedKey: "Loading ..."
        })

        setTimeout(() => {
            this.setState({
                invitedKey: randomInt(10000000, 99999999),
                refreshInvitedKey: false,
            })
        }, 2000);
    }

    render() {
        if (this.state.redirectTo !== null) {
            return <Redirect to={this.state.redirectTo} />
        }
        return (
            <div className={'tm-flex-column-center'}>
                {this.state.isFetching ? <FormLoadingSmall style={{ marginTop: '10px' }} />
                    : <Link to={'/group-dashboard/' + this.state.groupInfo.id}><img className={'tm-avatar-sidebar'} src={BASE_URL_IMAGE + '/icon/maxresdefault.jpg'} alt={'Avatar'} /></Link>}
                {this.state.isFetching ? <FormLoadingSmall style={{ marginTop: '10px' }} />
                    : <div className={'tm-sidebar-name'}>{this.state.groupInfo.title}</div>}
                {this.state.isFetching ? <FormLoadingSmall style={{ marginTop: '10px' }} />
                    : <div className={'tm-group-sidebar-info tm-group-info-bg-orange'}>
                        <div className={'tm-group-sidebar-icon'}><i class="fas fa-users" /></div>
                        <div className={'tm-group-sidebar-title'}><div><span className={'tm-group-sb-span'}>{this.state.groupInfo.total_member}</span> members</div></div>
                    </div>}
                {this.state.isFetching ? <FormLoadingSmall style={{ marginTop: '10px' }} />
                    : <div className={'tm-group-sidebar-info tm-group-info-bg-ocean'}>
                        <div className={'tm-group-sidebar-icon'}><i class="fas fa-question" /></div>
                        <div className={'tm-group-sidebar-title'}><div><span className={'tm-group-sb-span'}>{this.state.groupInfo.total_question}</span> questions</div></div>
                    </div>}
                {this.state.isFetching ? <FormLoadingSmall style={{ marginTop: '10px' }} />
                    : <div className={'tm-group-sidebar-info tm-group-info-bg-green'}>
                        <div className={'tm-group-sidebar-icon'}><i class="fas fa-user-secret" /></div>
                        <div className={'tm-group-sidebar-title'}><div><span className={'tm-group-sb-span'}><i class="fas fa-unlock-alt" /></span> {getPrivacyOfGroup(this.state.groupInfo.privacy)}</div></div>
                    </div>}
                {this.state.isFetching ? <FormLoadingSmall style={{ marginTop: '10px' }} />
                    : <div className={'tm-group-sidebar-info tm-group-info-bg-purple'}>
                        <div className={'tm-group-sidebar-icon'}><i class="fas fa-key" /></div>
                        <div className={'tm-group-sidebar-title'}><div><span className={'tm-group-sb-span'}>{this.state.groupInfo.key}</span></div></div>
                        <div className={'tm-group-sidebar-refresh'} onClick={this.handleRefreshKey.bind(this)}>
                            <i className={"fas fa-sync-alt " + (this.state.refreshInvitedKey ? "tm-group-sidebar-refresh-icon" : "")}></i>
                        </div>
                    </div>}
            </div>
        );
    }

    componentDidMount() {
        Axios.get(USER_SERVICE_DOMAIN + API_GROUP_GET_GROUP_DETAIL_INFO + '?group_id=' + this.props.groupId, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                this.setState({
                    isFetching: false,
                    groupInfo: res.data.data
                })
            } else {
                console.log('Error when fetching data of group info');
                console.log(res.data);
            }
        }).catch(error => {
            console.log('Error when fetching data of group info');
            console.log(error);
            if (error.response.status === 401) {
                localStorage.removeItem('sot-token');
                this.setState({
                    redirectTo: '/login'
                })
            };
        })
    }
}

export default GroupInfoDetail;

function randomInt(min, max) {
    return min + Math.floor((max - min) * Math.random());
}

// const exampleResponseData = {
//     "id": 2,
//     "creator": 1,
//     "group_id": "tm_advanced_math",
//     "title": "Advanced Math",
//     "default_coin": 2000,
//     "privacy": 2,
//     "key": "11591346273",
//     "description": "",
//     "created_at": "2020-03-23 09:01:27",
//     "updated_at": "2020-06-05 08:37:52",
//     "total_member": 1,
//     "total_question": 0
// };