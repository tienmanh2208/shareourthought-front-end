import React from "react";
import { BASE_URL_IMAGE } from '../../../constants.js';

class GroupDashboardHint extends React.Component {
    render() {
        let content = INFOS.map(function (value, index) {
            return (
                <div className={'tm-db-topic-catalogue tm-flex-row-center-nowrap'}>
                    <div className={'tm-db-tp-ctl-icon'}>
                        <img className={'tm-img-tp-ctl-icon'} src={value.icon} alt={value.title} />
                    </div>
                    <div className={'tm-db-topic-ctl-content tm-flex-column'}>
                        <div className={'tm-db-topic-ctl-ct-title'}>{value.title}</div>
                        <div className={'tm-db-topic-ctl-ct-quantity'}>{value.quantity} new question</div>
                    </div>
                </div>
            );
        });

        return (
            <div style={{ marginTop: '50px' }}>
                <div className={'tm-db-ct-topics-title tm-margin-top20'}>Tìm kiếm bài đăng</div>
                <div className={'tm-flex-row-spacebtw-wrap'}>
                    {content}
                </div>
            </div>
        );
    }
}

export default GroupDashboardHint;

const INFOS = [
    {
        icon: BASE_URL_IMAGE + '/icon/team.svg',
        title: 'Người dùng',
        quantity: 205,
    },
    {
        icon: BASE_URL_IMAGE + '/icon/iconfinder_bookshelf.svg',
        title: 'Danh mục',
        quantity: 20,
    },
    {
        icon: BASE_URL_IMAGE + '/icon/icons8-tag-window-64.png',
        title: 'Tiêu đề',
        quantity: 2010,
    }
];