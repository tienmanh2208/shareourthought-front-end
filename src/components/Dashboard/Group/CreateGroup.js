import React from "react";
import { GROUP_PRIVACY, USER_SERVICE_DOMAIN, API_CREATE_GROUP } from "../../../constants";
import { Redirect } from "react-router-dom";
import Tag from "../../General/Tag";
import { removeElementInArrayByValue } from '../../../FunctionConstants.js';
import Axios from "axios";
import FormLoading from "../../General/FormLoading";
import FormNotification from "../../General/FormNotification";

class CreateGroup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            groupName: '',
            groupId: '',
            defaultCoin: 0,
            privacy: GROUP_PRIVACY.private,
            description: '',
            inputSection: '',
            sections: [],
            isCancel: false,
            isLoading: false,
            isDisplayForm: false,
            formContent: {
                title: '',
                content: '',
            }
        };
        this.handleSubmitSection = this.handleSubmitSection.bind(this);
        this.callToCreateGroup = this.callToCreateGroup.bind(this);
        this.displayFormNotification = this.displayFormNotification.bind(this);
        this.closeForm = this.closeForm.bind(this);
    }

    render() {
        if (this.state.isCancel) {
            return <Redirect to={'/dashboard'} />
        }

        let tags = this.state.sections.map(value => {
            return <Tag name={value} displayDelete={true} actionDelete={this.handleDeleteSections.bind(this)} />
        })

        return (
            <div className={'tm-db-ct-yq-frame'}>
                <div className={'tm-db-ct-yq-title'}>Create group</div>
                <div className={'tm-db-gr-cg-name'}>Tên group</div>
                <input value={this.state.groupName} className={'tm-db-gr-cg-input'} onChange={this.handleInputGroupName.bind(this)} placeholder={'Enter your group name'} />
                <div className={'tm-db-gr-cg-name'}>Group id</div>
                <input value={this.state.groupId} className={'tm-db-gr-cg-input'} onChange={this.handleInputGroupId.bind(this)} placeholder={'Enter your group id'} />
                <div className={'tm-db-gr-cg-name'}>Default coin</div>
                <input value={this.state.defaultCoin} className={'tm-db-gr-cg-input'} onChange={this.handleInputDefaultCoin.bind(this)} placeholder={'Enter your default coin'} type={'number'} />
                <div className={'tm-db-gr-cg-name'}>Privacy</div>
                <form className={'tm-db-gr-cg-form-frame'}>
                    <div className={'tm-flex-row-spacebtw-nowrap tm-margin-top10'}>
                        <input onClick={this.handleInputPrivacy.bind(this)} type="radio" name="gender" value={GROUP_PRIVACY.private} />
                        <div><i class="fas fa-lock tm-db-gr-cg-privacy-icon" /></div>
                        <label className={'tm-db-gr-cg-radio-content'} for={GROUP_PRIVACY.private}>
                            <div className={'tm-db-gr-cg-privacy-title'}>Private</div>
                            If you set your group's privacy to private, your group will be invisible for everyone. Only you can add member to this group.
                        </label>
                    </div>
                    <div className={'tm-flex-row-spacebtw-nowrap tm-margin-top10'}>
                        <input onClick={this.handleInputPrivacy.bind(this)} type="radio" name="gender" value={GROUP_PRIVACY.protected} />
                        <div><i class="fas fa-user-lock tm-db-gr-cg-privacy-icon" /></div>
                        <label className={'tm-db-gr-cg-radio-content'} for={GROUP_PRIVACY.protected}>
                            <div className={'tm-db-gr-cg-privacy-title'}>Protected</div>
                            If you set your group's privacy to protected, your group will be invisible for everyone. Everyone can join your group by a auto generated key which we will give you.
                        </label>
                    </div>
                    <div className={'tm-flex-row-spacebtw-nowrap tm-margin-top10'}>
                        <input onClick={this.handleInputPrivacy.bind(this)} type="radio" name="gender" value={GROUP_PRIVACY.public} />
                        <div><i class="fas fa-globe-europe tm-db-gr-cg-privacy-icon" /></div>
                        <label className={'tm-db-gr-cg-radio-content'} for={GROUP_PRIVACY.public}>
                            <div className={'tm-db-gr-cg-privacy-title'}>Public</div>
                            If you set your group's privacy to public, your group will be visible for everyone. Everyone can seach and join to your group freely
                        </label>
                    </div>
                </form>
                <div className={'tm-db-gr-cg-name tm-margin-top10'}>Add description</div>
                <textarea value={this.state.description} className={'tm-db-gr-cg-textarea'} onChange={this.handleInputDescription.bind(this)} placeholder={"Enter your group's description"} />
                <div className={'tm-db-gr-cg-name'}>Add sections</div>
                <input value={this.state.inputSection} className={'tm-db-gr-cg-input'} onChange={this.handleInputSection} onKeyDown={this.handleEnter.bind(this)} placeholder={'Enter your sections'} />
                <div className={'tm-tag-frame'}>{tags}</div>
                <div className={'tm-flex-row-reverse'}>
                    <button onClick={this.handleCreateGroup.bind(this)} className={'tm-btn tm-btn-success'}>Create group</button>
                    <button onClick={this.handleCancel} className={'tm-btn tm-btn-warning'}>Cancel</button>
                </div>
                {this.state.isLoading ? <FormLoading /> : ''}
                {this.state.isDisplayForm ? <FormNotification title={this.state.formContent.title} content={this.state.formContent.content} action={this.closeForm.bind(this)} /> : ''}
            </div>
        );
    }

    handleCreateGroup = () => {
        this.setState({
            isLoading: true,
        })

        this.callToCreateGroup(null, this.displayFormNotification);
    }

    callToCreateGroup = (callBack, callBackWhenFail) => {
        Axios.post(USER_SERVICE_DOMAIN + API_CREATE_GROUP, {
            title: this.state.groupName,
            default_coin: this.state.defaultCoin,
            privacy: this.state.privacy,
            sections: this.state.sections,
            group_id: this.state.groupId,
            description: this.state.description
        }, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 201) {
                console.log('Create successfully');
            } else {
                callBackWhenFail('Warning', res.data.message);
            }
        }).catch(error => {
            callBackWhenFail('Warning', error.message);
        })
    }

    displayFormNotification = (title, content) => {
        this.setState({
            isLoading: false,
            isDisplayForm: true,
            formContent: {
                title: title,
                content: content
            }
        })
    }

    closeForm = () => {
        this.setState({
            isDisplayForm: false,
            formContent: {
                title: '',
                content: ''
            }
        })
    }

    handleInputGroupName = (e) => {
        this.setState({
            groupName: e.target.value,
        });
    }

    handleInputGroupId = (e) => {
        this.setState({
            groupId: e.target.value,
        });
    }

    handleInputDefaultCoin = (e) => {
        this.setState({
            defaultCoin: e.target.value,
        });
    }

    handleInputPrivacy = (e) => {
        this.setState({
            privacy: e.target.value,
        });
    }

    handleInputDescription = (e) => {
        this.setState({
            description: e.target.value,
        });
    }

    handleInputSection = (e) => {
        this.setState({
            inputSection: e.target.value,
        })
    }

    handleEnter = (e) => {
        if (e.key === 'Enter') {
            this.handleSubmitSection(e);
        }
    }

    handleSubmitSection = (e) => {
        this.setState({
            sections: this.state.sections.concat([e.target.value]),
            inputSection: '',
        })
    }

    handleCancel = () => {
        this.setState({
            isCancel: true,
        })
    }

    handleDeleteSections = (value) => {
        let arr = this.state.sections;
        removeElementInArrayByValue(arr, value);

        this.setState({
            sections: arr
        })
    }
}

export default CreateGroup;