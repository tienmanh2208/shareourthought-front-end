import React from 'react';
import { BASE_URL_IMAGE, USER_SERVICE_DOMAIN, API_GROUP_GET_NEWEST_MEMBERS } from '../../../constants.js';
import FormLoadingSmall from '../../General/FormLoadingSmall.js';
import Axios from 'axios';

class GroupNewestMembers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetching: true,
            listUser: []
        }
    }

    render() {
        let content = this.state.listUser.map(generateTopActiveUser);
        return (
            <div className={'tm-sidebar-frame'}>
                <div className={'tm-sidebar-right-title'}>Newest users</div>
                <div className={'tm-sidebar-right-deco'} />
                {this.state.isFetching ? <FormLoadingSmall style={{ marginTop: '10px' }} /> : content}
            </div>
        );
    }

    componentDidMount() {
        Axios.get(USER_SERVICE_DOMAIN + API_GROUP_GET_NEWEST_MEMBERS + '?group_id=' + this.props.groupId, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                let listUser = res.data.data.map(item => {
                    return Object.assign(item, { avatar: BASE_URL_IMAGE + '/avatar/user.svg' });
                })

                this.setState({
                    listUser: listUser,
                    isFetching: false,
                })
            } else {
                console.log('Error when get newest member of group');
                console.log(res.data.data);
            }
        }).catch(error => {
            console.log('Error when get newest member of group');
            console.log(error);
        })
    }
}

function generateTopActiveUser(userInfo) {
    return (
        <div key={'group-newest-member-' + userInfo.id} className={'tm-float-clear tm-margin-10'}>
            <img className={'tm-topuser-img'} src={userInfo.avatar} alt={'Avatar'} />
            <div className={'tm-topuser-name'}>{userInfo.full_name}</div>
        </div>
    );
}

export default GroupNewestMembers;

// const exampleResponse = [
//     {
//         "full_name": "Nguyễn Mạnh",
//         "id": 1
//     },
//     {
//         "full_name": "Nguyễn Trung Anh",
//         "id": 2
//     }
// ];

// const USER = [
//     {
//         name: 'Nguyen Tien Manh',
//         avatar: BASE_URL_IMAGE + '/icon/double-decker.svg'
//     },
//     {
//         name: 'Nguyen Thi mung',
//         avatar: BASE_URL_IMAGE + '/icon/double-decker.svg'
//     },
//     {
//         name: 'Bui Van Hung',
//         avatar: BASE_URL_IMAGE + '/icon/double-decker.svg'
//     },
//     {
//         name: 'Ta Thi Bich Ngoc',
//         avatar: BASE_URL_IMAGE + '/icon/double-decker.svg'
//     },
//     {
//         name: 'Phan Thi Hong Nhung',
//         avatar: BASE_URL_IMAGE + '/icon/double-decker.svg'
//     },
// ];