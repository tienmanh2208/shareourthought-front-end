import React from 'react';
import GroupCreateQuestion from './GroupCreateQuestion';

class GroupFormCreateQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleForm: false,
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                visibleForm: true,
            });
        }, 200)
    }

    render() {
        return (
            <div className={'tm-form-container'}>
                <div className={'tm-form-frame'} style={this.state.visibleForm ? { width: '70%', maxWidth: '700px', opacity: 1 }
                    : { width: '70%', maxWidth: '700px' }}>
                    <GroupCreateQuestion groupId={this.props.groupId} toggleForm={this.props.toggleForm} />
                </div>
            </div>
        )
    }
}

export default GroupFormCreateQuestion;