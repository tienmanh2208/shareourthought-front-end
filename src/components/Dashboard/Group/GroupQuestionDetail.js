import React from 'react';
import QuestionDetail from '../QuestionDetail';

class GroupQuestionDetail extends React.Component {
    render() {
        return (
            <QuestionDetail questionId={this.props.questionId} groupId={this.props.groupId} />
        )
    }
}

export default GroupQuestionDetail;