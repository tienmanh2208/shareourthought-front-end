import React from "react";
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { SECTION_IMAGES } from '../../../imagesConstant';
import { removeElementInArrayByValue } from '../../../FunctionConstants';
import Tag from "../../General/Tag";
import { USER_SERVICE_DOMAIN, API_GROUP_GET_LIST_SECTIONS, API_GROUP_CREATE_QUESTION } from '../../../constants';
import Axios from "axios";
import { Redirect } from "react-router";
import FormLoading from "../../General/FormLoading";
import FormNotification from "../../General/FormNotification";

class GroupCreateQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listTags: [],
            input: '',
            question: {
                title: '',
                content: ''
            },
            fieldId: 1,
            sectionId: null,
            isLoading: false,
            isDisplayForm: false,
            listSection: [],
            redirectTo: null,
            formContent: {
                needDisplay: false,
                title: '',
                content: '',
            },
        };
        this.handleInputTag = this.handleInputTag.bind(this);
        this.handleInputContent = this.handleInputContent.bind(this);
        this.updateListSection = this.updateListSection.bind(this);
        this.callToGetListSection = this.callToGetListSection.bind(this);
        this.displayForm = this.displayForm.bind(this);
        this.handleCreateQuestionSuccessfully = this.handleCreateQuestionSuccessfully.bind(this);
    }

    render() {
        if (this.state.formContent.needDisplay) {
            return <FormNotification
                title={this.state.formContent.title}
                content={this.state.formContent.content}
                action={this.closeForm.bind(this)}
            />
        }

        if (this.state.redirectTo !== null) {
            return <Redirect to={this.state.redirectTo} />
        }

        let listCategories = SECTION_IMAGES.map(function (item) {
            return <option value={item.field_id}>{item.title}</option>
        });

        let tags = this.state.listTags.map(item => {
            return <Tag displayDelete={true} name={item} actionDelete={this.handleDeleteTags.bind(this)} />
        });

        let sections = this.state.listSection.map(item => {
            return <option value={item.sectionId}>{item.name}</option>
        })

        return (
            <div>
                <input value={this.state.question.title} onChange={this.handleInputTitle.bind(this)} className={'tm-newqt-title'} type={'input'} placeholder={'Tiêu đề bài viết'} />
                <CKEditor
                    editor={ClassicEditor}
                    onInit={editor => {
                        // You can store the "editor" and use when it is needed.
                        console.log('Editor is ready to use!', editor);
                    }}
                    onChange={(event, editor) => {
                        const data = editor.getData();
                        this.handleInputContent(data);
                        // console.log({event, editor, data});
                    }}
                    onBlur={(event, editor) => {
                        // console.log('Blur.', editor);
                    }}
                    onFocus={(event, editor) => {
                        // console.log('Focus.', editor);
                    }}
                />
                <div className={'tm-newqt-category'}>
                    <div className={'tm-newqt-ctg-title'}>Lĩnh vực</div>
                    <select onChange={this.handleFieldId.bind(this)} className={'tm-newqt-select-box'}>
                        {listCategories}
                    </select>
                </div>
                <div className={'tm-newqt-category'}>
                    <div className={'tm-newqt-ctg-title'}>Danh mục</div>
                    <select onChange={this.handleInputSectionId.bind(this)} className={'tm-newqt-select-box'}>
                        {sections}
                    </select>
                </div>
                <div className={'tm-newqt-tags'}>
                    <div className={'tm-newqt-ctg-title'}>Tags</div>
                    <div className={'tm-newqt-tags-list'}>
                        {tags}
                        <input
                            value={this.state.input}
                            type={'input'}
                            className={'tm-tag-frame tm-tag-input'}
                            placeholder={'Enter your tag'}
                            onKeyDown={this.handleEnter.bind(this)}
                            onChange={this.handleChange.bind(this)}
                        />
                    </div>
                </div>
                <div className={'tm-newqt-btn-frame'}>
                    <button onClick={this.handleCreateQuestion.bind(this)} className={'tm-btn tm-btn-success tm-float-right '}>Tạo mới</button>
                    <button onClick={this.props.toggleForm} className={'tm-btn tm-btn-cancel tm-float-right '}>Hủy bỏ</button>
                </div>
                {this.state.isLoading ? <FormLoading /> : ''}
            </div>
        );
    }

    componentDidMount = () => {
        this.callToGetListSection(this.updateListSection);
    }

    handleCreateQuestion = () => {
        this.setState({
            isLoading: true
        })

        this.callToCreateQuestionInGroup(this.handleCreateQuestionSuccessfully, this.displayForm);
    }

    callToCreateQuestionInGroup = (callBack, callBackWhenFail) => {
        Axios.post(USER_SERVICE_DOMAIN + API_GROUP_CREATE_QUESTION, {
            question_content: this.state.question,
            section_id: this.state.sectionId,
            group_id: this.props.groupId,
            tags: this.state.listTags,
            field_id: this.state.fieldId
        }, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 201) {
                callBack(res.data.message, res.data.data.question_id);
            } else {
                callBackWhenFail('Chú ý', res.data.message);
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail('Chú ý', 'Lỗi khi tạo câu hỏi');
        })
    }

    handleCreateQuestionSuccessfully = (message, questionId) => {
        this.setState({
            isLoading: false,
            formContent: {
                needDisplay: true,
                title: "Thông báo",
                content: message,
            },
            redirectTo: '/group-question/' + this.props.groupId + '/' + questionId
        })
    }

    closeForm = () => {
        this.setState({
            formContent: {
                needDisplay: false,
                title: '',
                content: '',
            }
        })
    }

    displayForm = (title, message) => {
        this.setState({
            isLoading: false,
            formContent: {
                needDisplay: true,
                title: title,
                content: message,
            }
        })
    }

    handleInputSectionId = (event) => {
        this.setState({
            sectionId: event.target.value
        })
    }

    callToGetListSection = (callBack) => {
        Axios.get(USER_SERVICE_DOMAIN + API_GROUP_GET_LIST_SECTIONS + '?group_id=' + this.props.groupId, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                let listSection = res.data.data.map(sectionInfo => {
                    return {
                        sectionId: sectionInfo.id,
                        name: sectionInfo.name
                    };
                })

                callBack(listSection);
            } else {
                console.log(res.data);
            }
        }).catch(error => {
            console.log(error);
        })
    }

    updateListSection = (data) => {
        this.setState({
            listSection: data
        })
    }

    handleDeleteTags = (value) => {
        let arr = this.state.listTags;
        removeElementInArrayByValue(arr, value);

        this.setState({
            listTags: arr
        })
    }

    handleFieldId = (event) => {
        this.setState({
            fieldId: event.target.value
        })
    }

    handleInputContent = (data) => {
        let questionInfo = this.state.question;

        this.setState({
            question: Object.assign(questionInfo, { content: data })
        })
    }

    handleInputTitle = (event) => {
        let questionInfo = this.state.question;

        this.setState({
            question: Object.assign(questionInfo, { title: event.target.value })
        })
    }

    handleInputTag = () => {
        this.setState({
            listTags: this.state.listTags.concat([this.state.input]),
            input: ''
        })
    };

    handleEnter = (event) => {
        if (event.key === 'Enter') {
            this.handleInputTag(event);
        }
    };

    handleChange = (event) => {
        this.setState({
            input: event.target.value,
        })
    };
}

export default GroupCreateQuestion;