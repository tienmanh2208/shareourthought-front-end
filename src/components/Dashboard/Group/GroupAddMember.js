import React from 'react';
import { BASE_URL_IMAGE } from '../../../constants.js';
import FormLoadingSmall from '../../General/FormLoadingSmall.js';

class GroupAddMember extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchStatus: SEARCH_STATUS.typing,
            input: '',
        }
    }

    handleInput = (event) => {
        this.setState({
            searchStatus: event.target.value === '' ? SEARCH_STATUS.typing : this.state.searchStatus,
            input: event.target.value,
        })
    }

    handleEnter = (event) => {
        if (event.key === 'Enter') {
            if (this.state.input === '') {
                this.setState({
                    searchStatus: SEARCH_STATUS.typing
                });
            } else {
                setTimeout(() => {
                    this.setState({
                        searchStatus: SEARCH_STATUS.done
                    })
                }, 3000)

                this.setState({
                    searchStatus: SEARCH_STATUS.searching,
                })
            }
        }
    }

    render() {
        return (
            <div className={'tm-sidebar-frame'}>
                <div className={'tm-sidebar-right-title'}>Add member</div>
                <div className={'tm-sidebar-right-deco'} />
                <div style={{ display: 'flex' }}>
                    <input value={this.state.input}
                        onChange={this.handleInput.bind(this)}
                        onKeyDown={this.handleEnter.bind(this)}
                        className={'tm-sidebar-input'}
                    />
                </div>
                <div>
                    {this.state.searchStatus === SEARCH_STATUS.typing ? ''
                        : this.state.searchStatus === SEARCH_STATUS.searching ? <FormLoadingSmall />
                            : <SearchedMemberInfo info={responseGroupInfo} />}
                </div>
                <div className={'tm-flex-row-reverse'}>
                    <button className={'tm-btn tm-btn-warning'} style={{ width: '100%', margin: '10px 0' }}>Add</button>
                </div>
            </div>
        )
    }
}

const WaitingFrame = () => {
    return (
        <div className={'tm-waiting-frame'}>
            Your search will appeared in hear
        </div>
    );
}

const SearchedMemberInfo = (props) => {
    return (
        <div className={'tm-sg-result'}>
            <img className={'tm-sg-result-avatar'} src={props.info.avatar} alt={'Avatar'} />
            <div className={'tm-sg-result-content'}>
                <div className={'tm-sg-result-grName'} style={{ fontSize: '13px' }}>{props.info.name}</div>
                <div className={'tm-sg-result-detail'}>
                    Joined: <span className={'tm-sg-result-detail-bold'}>{props.info.joined}</span>
                </div>
            </div>
        </div>
    );
}

export default GroupAddMember;

const SEARCH_STATUS = {
    typing: 0,
    searching: 1,
    done: 2
}

const responseGroupInfo = {
    avatar: BASE_URL_IMAGE + '/icon/double-decker.svg',
    joined: '2020-03-21',
    name: 'Nguyễn Thị Thu Trang'
};