import React from "react";
import YourQuestion from "./Content/YourQuestion";
import CreateQuestion from "./Content/CreateQuestion";
import Topics from "./Content/Topics";
import NewestQuestion from "./Content/NewestQuestion";
import SidebarLeft from "./Sidebar/SidebarLeft";
import SidebarRight from "./Sidebar/SidebarRight";
import NewQuestion from "./NewQuestion";
import QuestionDetail from "./QuestionDetail";
import CreateGroup from "./Group/CreateGroup";
import GroupLeft from "./Sidebar/GroupLeft";
import GroupRight from "./Sidebar/GroupRight";
import GroupDashboard from "./Group/GroupDashboard";
import { useParams } from "react-router";
import GroupQuestionDetail from "./Group/GroupQuestionDetail";
import SearchQuestion from "./SearchQuestion";
import HistoryConfirmAnswer from './Content/HistoryConfirmAnswer';
import Warning from "./Content/Warning";

const TYPE = {
    DASHBOARD: 'dashboard',
    QUESTION: 'question',
    DETAIL: 'detail',
    CREATE_GROUP: 'create-group',
    GROUP_DASHBOARD: 'group-dashboard',
    GROUP_QUESTION_DETAIL: 'group-question',
    SEARCH: 'search'
};

class Content extends React.Component {
    render() {
        switch (this.props.type) {
            case TYPE.DASHBOARD:
                return (
                    <div className={'tm-db-content-frame'}>
                        <SidebarLeft />
                        <SidebarRight />
                        <Warning />
                        <HistoryConfirmAnswer />
                        <YourQuestion />
                        <NewestQuestion />
                        <Topics />
                        <CreateQuestion />
                    </div>
                );
            case TYPE.QUESTION:
                return (
                    <div className={'tm-db-content-frame'}>
                        <SidebarLeft />
                        <NewQuestion />
                        <SidebarRight />
                    </div>
                );
            case TYPE.DETAIL:
                return (
                    <div className={'tm-db-content-frame'}>
                        <SidebarLeft />
                        <GetIdQuestion />
                        <SidebarRight />
                    </div>
                );
            case TYPE.SEARCH:
                return (
                    <div className={'tm-db-content-frame'}>
                        <SidebarLeft />
                        <SearchQuestion />
                        <SidebarRight />
                    </div>
                );
            case TYPE.CREATE_GROUP:
                return (
                    <div className={'tm-db-content-frame'}>
                        <SidebarLeft />
                        <CreateGroup />
                        <SidebarRight />
                    </div>
                );
            case TYPE.GROUP_QUESTION_DETAIL:
                return (
                    <GetGroupIdAndQuestionId />
                );
            case TYPE.GROUP_DASHBOARD:
                return (
                    <GetIdGroup />
                );
        }
    }
}

export default Content;

const GetIdQuestion = () => {
    let { questionId } = useParams();
    return <QuestionDetail questionId={questionId} />;
}

const GetIdGroup = () => {
    let { groupId } = useParams();
    return (
        <div className={'tm-db-content-frame'}>
            <GroupLeft groupId={groupId} />
            <GroupDashboard groupId={groupId} />
            <GroupRight groupId={groupId} />
        </div>
    );
}

const GetGroupIdAndQuestionId = () => {
    let { groupId, questionId } = useParams();
    return (
        <div className={'tm-db-content-frame'}>
            <GroupLeft groupId={groupId} />
            <GroupQuestionDetail groupId={groupId} questionId={questionId} />
            <GroupRight groupId={groupId} />
        </div>
    )
}