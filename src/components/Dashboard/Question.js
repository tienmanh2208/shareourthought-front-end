import React from "react";
import { Link } from 'react-router-dom';
import { getImageOfField } from '../../FunctionConstants';
import ReactHtmlParser from 'react-html-parser';
import { QUESTION_COMPONENT_TYPE } from "../../constants";

class Question extends React.Component {
    render() {
        let questionInfo = this.props.questionInfo;
        console.log(questionInfo.question_content.title);
        let uri = this.props.componentType === QUESTION_COMPONENT_TYPE.GROUP
            ? '/group-question/' + questionInfo.group.group_id + '/' + questionInfo._id
            : '/question-detail/' + questionInfo._id;

        return (
            <Link to={uri} key={questionInfo._id} className={'tm-question-frame'} style={this.props.style}>
                <div className={'tm-qt-header tm-flex-row-left-nowrap'}>
                    <div className={'tm-qt-header-icon tm-flex-column'}>
                        <img className={'tm-img-db-icon'} src={getImageOfField(questionInfo.field_id)} alt={'Field icon'} />
                    </div>
                    {/* <div className={'tm-flex-column'} style={{width: "70%"}}> */}
                    <div className={'tm-flex-column tm-qt-info-frame'}>
                        <div className={'tm-qt-title'}>{questionInfo.question_content.title}</div>
                        <div className={'tm-qt-brief-content'}>{ReactHtmlParser(questionInfo.question_content.content)}</div>
                        <div className={'tm-qt-time'}>{questionInfo.created_at}</div>
                    </div>
                </div>
                <div className={'tm-qt-line'} />
                <div className={'tm-qt-info tm-flex-row-left-nowrap'}>
                    <div className={'tm-qt-info-detail'}>
                        <div className={'tm-qt-info-number'}>{questionInfo.price}</div>
                        <div className={'tm-qt-info-name'}>Coin</div>
                    </div>
                    <div className={'tm-qt-info-detail'}>
                        <div className={'tm-qt-info-number'}>{questionInfo.comments.length}</div>
                        <div className={'tm-qt-info-name'}>Bình luận</div>
                    </div>
                    <div className={'tm-qt-info-detail'}>
                        <div className={'tm-qt-info-number'}>{questionInfo.upvotes}</div>
                        <div className={'tm-qt-info-name'}>Thích</div>
                    </div>
                    <div className={'tm-qt-info-detail'}>
                        <div className={'tm-qt-info-number'}>{questionInfo.status}</div>
                        <div className={'tm-qt-info-name'}>Status</div>
                    </div>
                </div>
            </Link>
        );
    }
}

export default Question;