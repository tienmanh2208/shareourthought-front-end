import React from "react";
import { getFontAwsome } from '../../FunctionConstants.js';

class Topics extends React.Component {
    render() {
        let active = 'tm-db-ct-yq-topic tm-topic-active';
        let inactive = 'tm-db-ct-yq-topic tm-topic-inactive';
        let decoActive = 'tm-deco-topics-active';
        let decoInactive = 'tm-deco-topics-inactive';

        let maxLength = this.props.listField.length > 5 ? 5 : this.props.listField.length;

        let customStyle = {
            width: (100 / maxLength - 1) + '%'
        }

        let headers = this.props.listField.map((info, index) => {
            if (index >= 5) {
                return [];
            }

            let classNameOfIcon = getFontAwsome(info.fieldId) + ' tm-db-ct-yq-topic-icon';

            return (
                <div key={'topics-' + index} style={customStyle} onClick={this.props.changeField.bind(this, info.fieldId)}
                    className={this.props.currentField === info.fieldId ? active : inactive}>
                    <i className={classNameOfIcon} />{info.name}
                    <div className={this.props.currentField === info.fieldId ? decoActive : decoInactive} />
                </div>
            )
        })
;
        return (
            <div className={'tm-db-ct-yq-topics-frame tm-flex-row-spacebtw-nowrap'}>
                {headers}
            </div>
        );
    }
};

export default Topics;