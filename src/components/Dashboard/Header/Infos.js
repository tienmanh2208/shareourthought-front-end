import React from "react";
import { connect } from "react-redux";
import { LOGOUT } from "../../../Reducers/actions";
import { Redirect } from "react-router";
import { Link } from "react-router-dom";
import {
    BASE_URL_IMAGE,
    USER_SERVICE_DOMAIN,
    API_GET_BASIC_INFO
} from "../../../constants";
import Axios from "axios";

class InfosComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectTo: '',
            userInfo: null,
            isFetching: true,
        }
    }

    handleSignOut() {
        localStorage.removeItem('sot-token');
        this.props.signOut(LOGOUT);
        this.setState({
            redirectTo: '/login'
        })
    }

    render() {
        if (this.state.redirectTo !== '') {
            return <Redirect to={this.state.redirectTo} />
        }

        console.log(this.props);

        return (
            <div className={'tm-db-hd-info-frame'}>
                <Link to={'/dashboard'} className={'tm-db-hd-nameweb'}>ShareKnowledge</Link>
                <div className={'tm-db-hd-right'}>
                    {/* <div className={'tm-db-hd-notification tm-flex-column'}>
                        <i className="far fa-bell" />
                    </div> */}
                    {this.props.isGlobal === true ? '' : <div className={'tm-db-hd-nameinfo tm-flex-column'}>
                        <Link to={'/personal/dashboard'} className={'tm-db-hd-name'}>{this.state.isFetching ? 'Loading ...' : this.state.userInfo.full_name}</Link>
                        <div className={'tm-db-hd-status'}>Online<i className={'tm-db-hd-status-icon'} /></div>
                    </div>}
                    {this.props.isGlobal === true ? '' : <Link to={'/personal/dashboard'} className={'tm-db-hd-avatar tm-flex-column'}>
                        <img src={BASE_URL_IMAGE + '/avatar/user.svg'} alt={'avatar'} className={'tm-db-hd-image'} />
                    </Link>}
                    {this.props.isGlobal === true ? '' : <div className={'tm-db-hd-signout tm-flex-column'} onClick={this.handleSignOut.bind(this)}>
                        <i className="fas fa-sign-out-alt"></i>
                    </div>}
                    {this.props.isGlobal === true ? <div className={'tm-personal-header-homepage tm-background-white-blur'} onClick={this.handleSignOut.bind(this)}>
                        Đăng nhập
                    </div> : ''}
                </div>
            </div>
        );
    }

    componentDidMount = () => {
        if (this.props.isGlobal === true) {
            return 0;
        }

        Axios.get(USER_SERVICE_DOMAIN + API_GET_BASIC_INFO, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                this.setState({
                    isFetching: false,
                    userInfo: res.data.data
                })
            } else {
                console.log(res);
            }
        }).catch(error => {
            console.log(error);
        })
    }
}

const signOutAction = (action) => {
    return {
        type: action,
    }
};

const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = (dispatch) => {
    return {
        signOut: (action) => {
            dispatch(signOutAction(action))
        }
    }
};

const Infos = connect(mapStateToProps, mapDispatchToProps)(InfosComponent);

export default Infos;