import React from 'react';

import CanvasJSReact from '../../js/chart/canvasjs.react.js';
let CanvasJS = CanvasJSReact.CanvasJS;
let CanvasJSChart = CanvasJSReact.CanvasJSChart;

// let dataPoints = [];
class LineChart extends React.Component {
    render() {
        const options = {
            theme: "light2",
            title: {
                text: "Số coin nhận được theo tháng"
            },
            axisY: {
                title: "Coin",
                prefix: "",
                includeZero: true,
            },
            data: [{
                type: "line",
                xValueFormatString: "MMM YYYY",
                yValueFormatString: "$#,##0",
                dataPoints: this.props.dataToDrawLineChart
            }]
        }
        return (
            <div>
                <CanvasJSChart options={options}
                    onRef={ref => this.chart = ref}
                />
                {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
            </div>
        );
    }

    // componentDidMount() {
        // let chart = this.chart;
        // fetch('https://canvasjs.com/data/gallery/react/nifty-stock-price.json')
        //     .then(function (response) {
        //         return response.json();
        //     })
        //     .then(function (data) {
        //         for (let i = 0; i < data.length; i++) {
        //             dataPoints.push({
        //                 x: new Date(data[i].x),
        //                 y: data[i].y
        //             });
        //         }
        //         chart.render();
        //     });

        // let data = dataToDrawChart;
        // for (let i = 0; i < data.length; i++) {
        //     dataPoints.push({
        //         x: new Date(data[i].x),
        //         y: data[i].y
        //     });
        // }
        // chart.render();
    // }
}

export default LineChart;