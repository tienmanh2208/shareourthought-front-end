import React from 'react';

import CanvasJSReact from '../../js/chart/canvasjs.react.js';
let CanvasJS = CanvasJSReact.CanvasJS;
let CanvasJSChart = CanvasJSReact.CanvasJSChart;

class ColumnChart extends React.Component {
	render() {
		const options = {
			title: {
				text: this.props.dataToDraw.titleChart
			},
			data: [
				{
					// Change type to "doughnut", "line", "splineArea", etc.
					type: "column",
					dataPoints: this.props.dataToDraw.dataToDraw
				}
			]
		}
		return (
			<div>
				<CanvasJSChart options={options}
				/* onRef={ref => this.chart = ref} */
				/>
				{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
			</div>
		);
	}
}

export default ColumnChart;

// Data of props
// const data = {
// 	titleChart: '',
// 	dataToDraw: [
// 		{ label: "Apple", y: 123 },
// 		{ label: "Orange", y: 322 },
// 		{ label: "Banana", y: 120 },
// 		{ label: "Mango", y: 130 },
// 		{ label: "Grape", y: 150 }
// 	]
// };