import React from 'react'

import CanvasJSReact from '../../js/chart/canvasjs.react.js';
let CanvasJS = CanvasJSReact.CanvasJS;
let CanvasJSChart = CanvasJSReact.CanvasJSChart;

class StackedColumnChart extends React.Component {
    constructor() {
        super();
        this.toggleDataSeries = this.toggleDataSeries.bind(this);
    }

    toggleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else {
            e.dataSeries.visible = true;
        }
        this.chart.render();
    }

    render() {
        const options = {
            animationEnabled: true,
            exportEnabled: true,
            title: {
                text: this.props.dataToDraw.titleChart,
                fontFamily: "verdana"
            },
            axisY: {
                title: this.props.dataToDraw.unit.title,
                prefix: this.props.dataToDraw.unit.prefix,
                suffix: this.props.dataToDraw.unit.subfix
            },
            toolTip: {
                shared: true,
                reversed: true
            },
            legend: {
                verticalAlign: "center",
                horizontalAlign: "right",
                reversed: true,
                cursor: "pointer",
                itemclick: this.toggleDataSeries
            },
            data: this.props.dataToDraw.data
        }
        return (
            <div>
                <CanvasJSChart options={options}
                    onRef={ref => this.chart = ref}
                />
                {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
            </div>
        );
    }
}

export default StackedColumnChart;

// Template data
// const dataToDraw = {
//     titleChart: '',
//     unit: {
//         title: 'Coin',
//         prefix: '',
//         subfix: '',
//     },
//     data: [
//         {
//             type: "stackedColumn",
//             name: "General",
//             showInLegend: true,
//             yValueFormatString: "#,###k",
//             dataPoints: [
//                 { label: "Jan", y: 14 },
//                 { label: "Feb", y: 12 },
//                 { label: "Mar", y: 14 },
//                 { label: "Apr", y: 13 },
//                 { label: "May", y: 13 },
//                 { label: "Jun", y: 13 },
//                 { label: "Jul", y: 14 },
//                 { label: "Aug", y: 14 },
//                 { label: "Sept", y: 13 },
//                 { label: "Oct", y: 14 },
//                 { label: "Nov", y: 14 },
//                 { label: "Dec", y: 14 }
//             ]
//         },
//         {
//             type: "stackedColumn",
//             name: "Marketing",
//             showInLegend: true,
//             yValueFormatString: "#,###k",
//             dataPoints: [
//                 { label: "Jan", y: 13 },
//                 { label: "Feb", y: 13 },
//                 { label: "Mar", y: 15 },
//                 { label: "Apr", y: 16 },
//                 { label: "May", y: 17 },
//                 { label: "Jun", y: 17 },
//                 { label: "Jul", y: 18 },
//                 { label: "Aug", y: 18 },
//                 { label: "Sept", y: 17 },
//                 { label: "Oct", y: 18 },
//                 { label: "Nov", y: 18 },
//                 { label: "Dec", y: 18 }
//             ]
//         }]
// }