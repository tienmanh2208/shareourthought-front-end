import React from 'react';
import AdminHeader from './AdminHeader';
import AdminPages from './AdminPages';
import AdminOpenReport from './AdminOpenReport';
import AdminAcceptedReport from './AdminAcceptedReport';
import AdminUsers from './AdminUsers';
import AdminEmployees from './AdminEmployees';

class AdminMainFrame extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: 'openReport',
        }
        this.handleChangePage = this.handleChangePage.bind(this);
    }

    render() {
        return (
            <div>
                <AdminHeader />
                <AdminPages currentPage={this.state.currentPage} changePage={this.handleChangePage} />
                {this.state.currentPage === 'openReport' ? <AdminOpenReport />
                    : this.state.currentPage === 'myReport' ? <AdminAcceptedReport />
                        : this.state.currentPage === 'users' ? <AdminUsers />
                            : <AdminEmployees />}
            </div>
        )
    }

    handleChangePage = (pageId) => {
        this.setState({
            currentPage: pageId
        })
    }
}

export default AdminMainFrame;
