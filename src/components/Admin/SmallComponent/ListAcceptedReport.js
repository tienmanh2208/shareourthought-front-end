import React from 'react';
import AcceptedReport from './AcceptedReport';
import FormLoadingSmall from '../../General/FormLoadingSmall';

class ListAcceptedReport extends React.Component {
    render() {
        if (this.props.isFetching) {
            return <FormLoadingSmall />
        }

        let reports = this.props.listReport.map((item) => {
            return <AcceptedReport info={item} confirmAnswer={this.props.confirmAnswer} />
        })

        let styleHeader = { textAlign: 'center', fontWeight: 'bold', margin: '5px 0', fontSize: '16px' };

        return (
            <div>
                <div className={'admin-accepted-report-frame'}>
                    <div className={'admin-ar-question'} style={styleHeader}>Câu hỏi</div>
                    <div className={'admin-ar-answer'} style={styleHeader}>Câu trả lời</div>
                    <div className={'admin-ar-field'} style={styleHeader}>Danh mục</div>
                    <div className={'admin-ar-button'} style={styleHeader}>Hành động</div>
                </div>
                {reports}
            </div>
        )
    }
}

export default ListAcceptedReport;