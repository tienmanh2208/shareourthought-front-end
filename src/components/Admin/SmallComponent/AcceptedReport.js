import React from 'react';
import { getNameOfField } from '../../../FunctionConstants';
import ReactHtmlParser from 'react-html-parser';

class AcceptedReport extends React.Component {
    render() {
        return (
            <div className={'admin-accepted-report-frame'}>
                <div className={'admin-ar-question'}>
                    <div className={'admin-ar-question-title'}>{this.props.info.content_info.question_info.question_content.title}</div>
                    <div className={'admin-ar-question-content'}>{ReactHtmlParser(this.props.info.content_info.question_info.question_content.content)}</div>
                </div>
                <div className={'admin-ar-answer'}>
                    <div className={'admin-ar-answer-content'}>{ReactHtmlParser(this.props.info.content_info.content)}</div>
                </div>
                <div className={'admin-ar-field'}>
                    <button className={'tm-btn tm-btn-warning'}>{getNameOfField(this.props.info.content_info.question_info.field_id)}</button>
                </div>
                <div className={'admin-ar-button'}>
                    <button onClick={this.props.confirmAnswer.bind(this, this.props.info, 3)} className={'tm-btn tm-btn-success'}><i class="fas fa-check"></i></button>
                    <button onClick={this.props.confirmAnswer.bind(this, this.props.info, 4)} className={'tm-btn tm-btn-cancel'}><i class="fas fa-times"></i></button>
                    <button className={'tm-btn tm-btn-warning'}><i class="fas fa-sync-alt"></i></button>
                </div>
            </div>
        )
    }
}

export default AcceptedReport;