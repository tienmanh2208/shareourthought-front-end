import React from 'react';
import { getNameOfField } from '../../../FunctionConstants';
import ReactHtmlParser from 'react-html-parser';

class OpenReport extends React.Component {
    render() {
        return (
            <div className={'admin-oq-frame'}>
                <div className={'admin-oq-detail'}>
                    <div className={'admin-oq-title'}>{ReactHtmlParser(this.props.info.content_info.question_info.question_content.title)}</div>
                    <div className={'admin-oq-content'}>{ReactHtmlParser(this.props.info.content_info.question_info.question_content.content)}</div>
                </div>
                <div className={'admin-oq-field'}>
                    <button className={'tm-btn tm-btn-warning'} style={{ width: 'fit-content' }}>{getNameOfField(this.props.info.content_info.question_info.field_id)}</button>
                </div>
                <div className={'admin-op-button'}>
                    <button onClick={this.props.acceptReport.bind(this, this.props.info.id)} className={'tm-btn tm-btn-success'} style={{ width: 'fit-content' }}><i class="fas fa-check"></i></button>
                </div>
            </div>
        )
    }
}

export default OpenReport;

// const exampleInfoProp = {
//     "id": 1,
//     "id_content": "5ea7151c0dddd565555bb8a3",
//     "report_type": 1,
//     "reason": "This is the reason",
//     "status": 0,
//     "user_report": 1,
//     "user_resolve": null,
//     "report_info_id": null,
//     "created_at": "2020-06-29 08:50:05",
//     "updated_at": "2020-06-29 08:50:05",
//     "content_info": {
//         "_id": "5ea7151c0dddd565555bb8a3",
//         "user_id": 2,
//         "question_id": "5ea6e3670dddd565555bb8a2",
//         "content": "Đây thực sự là cách kết hợp màu kinh điển. Sự kết hợp màu táo bạo này sẽ giúp bạn chú ý hơn vào phần chính. Màu đỏ tượng trưng cho sự rực rỡ và độc đáo, trong khi màu vàng tạo ra cảm giác hạnh phúc, năng lượng và sự vui tươi. Cách kết hợp màu này thực sự hiệu quả với những sản phẩm hướng tới sự trẻ trung, năng động, nhiệt huyết.",
//         "status": 3,
//         "updated_at": "2020-06-29T07:45:59.811000Z",
//         "created_at": "2020-04-27T17:23:40.253000Z",
//         "report_status": 1,
//         "question_info": {
//             "_id": "5ea6e3670dddd565555bb8a2",
//             "user_id": 1,
//             "price": 5000,
//             "visibility": 1,
//             "status": 2,
//             "upvotes": 0,
//             "downvotes": 0,
//             "field_id": 4,
//             "group": {
//                 "group_id": null,
//                 "group_section_id": null
//             },
//             "tags": [
//                 "Update tag",
//                 "Question",
//                 "Testing"
//             ],
//             "comments": [
//                 {
//                     "commentator_id": 2,
//                     "content": "Cau nay kho the :/",
//                     "time": {
//                         "date": {
//                             "numberLong": "1588579609101"
//                         }
//                     }
//                 },
//                 {
//                     "commentator_id": 3,
//                     "content": "Cau nay de ma",
//                     "time": {
//                         "date": {
//                             "numberLong": "1588579609101"
//                         }
//                     }
//                 }
//             ],
//             "question_content": {
//                 "title": "This is the first question",
//                 "content": "Thứ nhất, rõ ràng tiền là tài sản có tính thanh khoản cao nhất. Một doanh nghiêp có thể có doanh thu rất cao nhưng không thu được tiền từ người mua, sẽ không có khả năng trả các khoản nợ nhà cung cấp, trả lương, trả nợ ngân hàng...Tất cả những điều đó sẽ ảnh hưởng 1 cách trực tiếp và rất nhanh chóng đến hoạt động của doanh nghiệp như nhân viên đình công, không làm việc, nhà cung cấp không bán nguyên vật liệu cho, ngân hàng siết nợ...Khi doanh nghiệp không có khả năng trả nợ, rất có thể dẫn vào tình trạng phá sản -> suy giảm giá trị của doanh nghiệp. Phân tích luồng tiền vào/ra, số tiền mà doanh nghiệp có...có thể đánh giá được chính xác tiềm năng, giá trị của doanh nghiệp ấy"
//             },
//             "updated_at": "2020-06-03T08:38:10.394000Z",
//             "created_at": "2020-04-27T13:51:35.842000Z"
//         }
//     }
// };