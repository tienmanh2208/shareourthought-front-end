import React from 'react';
import OpenReport from './OpenReport';
import FormLoadingSmall from '../../General/FormLoadingSmall';

class ListOpenReport extends React.Component {
    render() {
        let reports = this.props.listReport.map((report) => {
            return <OpenReport info={report} acceptReport={this.props.acceptReport} />
        })

        return (
            <div>
                <div className={'admin-oq-frame'} style={{ textAlign: 'center' }}>
                    <div className={'admin-oq-detail'}>Nội dung báo cáo</div>
                    <div className={'admin-oq-field'}>Lĩnh vực</div>
                    <div className={'admin-op-button'}>Hành động</div>
                </div>
                {this.props.isFetching ? <FormLoadingSmall />
                    : reports.length === 0 ? <div>Không có báo cáo mới nào</div> : reports}
            </div>
        )
    }
}

export default ListOpenReport;