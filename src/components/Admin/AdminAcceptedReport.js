import React from 'react';
import Pagination from '../General/Pagination';
import ListAcceptedReport from './SmallComponent/ListAcceptedReport';
import Axios from 'axios';
import {
    USER_SERVICE_DOMAIN,
    API_ADMIN_GET_ACCEPTED_REPORT,
    API_ADMIN_CONFIRM_REPORT
} from '../../constants';
import FormLoading from '../General/FormLoading';
import FormNotification from '../General/FormNotification';

class AdminAcceptedReport extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: 1,
            lastPage: 1,
            isFetching: true,
            isLoading: false,
            formInfo: {
                display: false,
                title: '',
                content: '',
            },
            listReport: []
        }
        this.callToGetAcceptedReport = this.callToGetAcceptedReport.bind(this);
        this.callToConfirmAnswer = this.callToConfirmAnswer.bind(this);
        this.callBackWhenCallApiSuccessfully = this.callBackWhenCallApiSuccessfully.bind(this);
        this.callBackWhenCallApiFail = this.callBackWhenCallApiFail.bind(this);
        this.udpateInfoListReport = this.udpateInfoListReport.bind(this);
    }

    render() {
        return (
            <div>
                <br />
                <ListAcceptedReport listReport={this.state.listReport} isFetching={this.state.isFetching} confirmAnswer={this.confirmAnswer.bind(this)} />
                <Pagination index={this.state.currentPage} max={this.state.lastPage} switchPage={this.switchPage.bind(this)} />
                {this.state.isLoading ? <FormLoading /> : ''}
                {this.state.formInfo.display ? <FormNotification title={this.state.formInfo.title} content={this.state.formInfo.content} action={this.closeForm.bind(this)} /> : ''}
            </div>
        )
    }

    componentDidMount = () => {
        this.callToGetAcceptedReport(this.udpateInfoListReport);
    }

    componentDidUpdate = (prevProps, prevState) => {
        if (prevState.currentPage !== this.state.currentPage) {
            this.setState({
                isFetching: true,
            })

            this.callToGetAcceptedReport(this.udpateInfoListReport);
        }
    }

    confirmAnswer = (info, action) => {
        this.setState({
            isLoading: true
        })

        this.callToConfirmAnswer(
            info.content_info.user_id,
            info.content_info._id,
            action,
            info.id,
            info.content_info.question_info._id,
            info.content_info.question_info.user_id,
            info.content_info.question_info.price,
            this.callBackWhenCallApiSuccessfully,
            this.callBackWhenCallApiFail
        );
    }

    callToGetAcceptedReport = (callBack) => {
        Axios.get(USER_SERVICE_DOMAIN + API_ADMIN_GET_ACCEPTED_REPORT, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                callBack(res.data.data);
            } else {
                console.log(res);
            }
        }).catch(error => {
            console.log(error);
        })
    }

    callToConfirmAnswer = (answererId, answerId, action, reportId, questionId, questionerId, price, callBack, callBackWhenFail) => {
        Axios.post(USER_SERVICE_DOMAIN + API_ADMIN_CONFIRM_REPORT, {
            answerer_id: answererId,
            answer_id: answerId,
            action: action,
            report_id: reportId,
            question_id: questionId,
            questioner_id: questionerId,
            price: price
        }, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 203) {
                callBack(res.data.message);
            } else {
                callBackWhenFail(res.data.message);
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail('Lỗi hệ thống');
        })
    }

    callBackWhenCallApiSuccessfully = (message) => {
        this.setState({
            isLoading: false,
            fetchingReport: true,
            formInfo: {
                display: true,
                title: 'Thông báo',
                content: message,
            }
        })

        this.callToGetAcceptedReport(this.udpateInfoListReport);
    }

    callBackWhenCallApiFail = (message) => {
        this.setState({
            isLoading: false,
            formInfo: {
                display: true,
                title: 'Chú ý',
                content: message,
            }
        })
    }

    udpateInfoListReport = (data) => {
        this.setState({
            lastPage: data.last_page,
            listReport: data.data,
            isFetching: false
        })
    }

    switchPage = (pageNumber) => {
        this.setState({
            currentPage: pageNumber
        })
    }

    closeForm = () => {
        this.setState({
            formInfo: {
                display: false,
                title: '',
                content: '',
            }
        })
    }
}

export default AdminAcceptedReport;