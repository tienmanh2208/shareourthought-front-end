import React from 'react';

class AdminPages extends React.Component {
    render() {
        let active = 'tm-db-ct-yq-topic tm-topic-active';
        let inactive = 'tm-db-ct-yq-topic tm-topic-inactive';
        let decoActive = 'tm-deco-topics-active';
        let decoInactive = 'tm-deco-topics-inactive';

        let customStyle = {
            width: (100 / 4 - 1) + '%'
        }

        let pages = LIST_ADMIN_PAGE.map((pageInfo) => {
            return (
                <div key={'pages-' + pageInfo.pageId} style={customStyle} onClick={this.props.changePage.bind(this, pageInfo.pageId)}
                    className={this.props.currentPage === pageInfo.pageId ? active : inactive}>
                    {pageInfo.name}
                    <div className={this.props.currentPage === pageInfo.pageId ? decoActive : decoInactive} />
                </div>
            )
        })

        return (
            <div className={'tm-flex-row-spacebtw-nowrap'} style={{borderRadius: '0px', backgroundColor: 'royalblue', fontWeight: 'bold'}}>
                {pages}
            </div>
        )
    }
}

export default AdminPages;

const LIST_ADMIN_PAGE = [
    {
        pageId: "openReport",
        name: "Báo cáo của người dùng"
    },
    {
        pageId: "myReport",
        name: "Báo cáo đã nhận"
    },
    {
        pageId: "users",
        name: "Danh sách người dùng"
    },
    {
        pageId: "admins",
        name: "Danh sách admin"
    }
];