import React from 'react';
import { BASE_URL_IMAGE, USER_SERVICE_DOMAIN, API_ADMIN_GET_INFO_ADMIN } from '../../constants';
import Tag from '../General/Tag';
import '../../css/admin.css';
import { Redirect } from 'react-router';
import FormLoadingSmall from '../General/FormLoadingSmall';
import Axios from 'axios';

class AdminHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectTo: null,
            isFetching: true,
            userInfo: null,
        }
        this.callToGetAdminInfo = this.callToGetAdminInfo.bind(this);
        this.updateInfoUser = this.updateInfoUser.bind(this);
        this.logout.bind(this);
    }

    render() {
        if (this.state.redirectTo !== null) {
            return <Redirect to={this.state.redirectTo} />
        }

        if (this.state.isFetching) {
            return <FormLoadingSmall />;
        }
        let tags = this.state.userInfo.positions.map(function (item) {
            return <Tag displayDelete={false} name={item.position_name} />;
        })

        return (
            <div className={'admin-header-frame'}>
                <div className={'admin-header-logo'}>Share Knowledge <span className={'admin-header-sub-name'}>Admin</span></div>
                <div className={'admin-info-frame'}>
                    <div className={'admin-info-detail'}>
                        <div className={'admin-info-fullname'}>{this.state.userInfo.last_name + ' ' + this.state.userInfo.first_name}</div>
                        <div className={'admin-info-tags'}>{tags}</div>
                    </div>
                    <div className={'admin-img-avatar-frame'}>
                        <img className={'admin-img-avatar'} src={BASE_URL_IMAGE + '/icon/admin-icon.svg'} alt={'Avatar admin'} />
                    </div>
                    <div className={'admin-img-avatar-frame'}>
                        <i onClick={this.logout.bind(this)} class="fas fa-sign-out-alt admin-sign-out"></i>
                    </div>
                </div>
            </div>
        )
    }

    componentDidMount = () => {
        this.callToGetAdminInfo(this.updateInfoUser, this.logout);
    }

    callToGetAdminInfo = (callBack, logOut) => {
        Axios.get(USER_SERVICE_DOMAIN + API_ADMIN_GET_INFO_ADMIN, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                callBack(res.data.data);
            } else if (res.data.code === 403) {
                logOut();
            } else {
                console.log(res);
            }
        }).catch(error => {
            console.log(error);
        })
    }

    updateInfoUser = (data) => {
        this.setState({
            isFetching: false,
            userInfo: data
        })
    }

    logout = () => {
        localStorage.removeItem('sot-token');
        this.setState({
            redirectTo: '/login'
        })
    }
}

export default AdminHeader;

// const exampleResponse = {
//     "id": 3,
//     "username": "admin1",
//     "first_name": "Manh 1",
//     "date_of_birth": "2000-01-16",
//     "last_name": "Admin",
//     "email": "admin1@gmail.com",
//     "role": 2,
//     "account_status": 1,
//     "coin_remain": 0,
//     "created_at": "2020-06-29 02:36:13",
//     "updated_at": "2020-06-29 02:36:13",
//     "user_id": 3,
//     "facebook_account": null,
//     "instagram": null,
//     "introduction": null,
//     "total_upvotes": 0,
//     "total_downvotes": 0,
//     "total_comments": 0,
//     "total_answers": 0,
//     "total_questions": 0,
//     "positions": [
//         {
//             "id": 1,
//             "position_name": "Quản trị câu hỏi"
//         },
//         {
//             "id": 2,
//             "position_name": "Quản trị câu trả lời"
//         },
//         {
//             "id": 3,
//             "position_name": "Quản trị người dùng"
//         },
//         {
//             "id": 4,
//             "position_name": "Quản trị admin"
//         }
//     ]
// };