import React from 'react';
import Pagination from '../General/Pagination';
import ListOpenReport from './SmallComponent/ListOpenReport';
import FormLoading from '../General/FormLoading';
import FormNotification from '../General/FormNotification';
import Axios from 'axios';
import {
    USER_SERVICE_DOMAIN,
    API_ADMIN_GET_ALL_OPEN_REPORT,
    API_ADMIN_ACCEPT_REPORT_TO_RESOLVE
} from '../../constants';

class AdminOpenReport extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: 1,
            lastPage: 1,
            fetchingReport: true,
            isLoading: false,
            formInfo: {
                display: false,
                title: '',
                content: '',
            },
            listReport: []
        }
        this.callToGetAllReport = this.callToGetAllReport.bind(this);
        this.callToAcceptReport = this.callToAcceptReport.bind(this);
        this.callBackAcceptReportSuccessfully = this.callBackAcceptReportSuccessfully.bind(this);
        this.callBackAcceptReportFailed = this.callBackAcceptReportFailed.bind(this);
        this.udpateInfoListReport = this.udpateInfoListReport.bind(this);
    }

    render() {
        return (
            <div>
                <br />
                <ListOpenReport listReport={this.state.listReport} isFetching={this.state.fetchingReport} acceptReport={this.acceptReport.bind(this)} />
                <Pagination index={this.state.currentPage} max={this.state.lastPage} switchPage={this.switchPage.bind(this)} />
                {this.state.isLoading ? <FormLoading /> : ''}
                {this.state.formInfo.display ? <FormNotification title={this.state.formInfo.title} content={this.state.formInfo.content} action={this.hideForm.bind(this)} /> : ''}
            </div>
        )
    }

    componentDidMount = () => {
        this.callToGetAllReport(this.udpateInfoListReport);
    }

    acceptReport = (reportId) => {
        this.setState({
            isLoading: true
        })

        this.callToAcceptReport(reportId, this.callBackAcceptReportSuccessfully, this.callBackAcceptReportFailed);
    }

    callToAcceptReport = (reportId, callBack, callBackWhenFail) => {
        Axios.post(USER_SERVICE_DOMAIN + API_ADMIN_ACCEPT_REPORT_TO_RESOLVE, {
            report_id: reportId
        }, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 203) {
                callBack(res.data.message);
            } else {
                callBackWhenFail(res.data.message);
            }
        }).catch(error => {
            console.log(error);
            callBackWhenFail('Lỗi hệ thống');
        })
    }

    callBackAcceptReportSuccessfully = (message) => {
        this.setState({
            isLoading: false,
            fetchingReport: true,
            formInfo: {
                display: true,
                title: 'Thông báo',
                content: message,
            }
        })

        this.callToGetAllReport(this.udpateInfoListReport);
    }

    callBackAcceptReportFailed = (message) => {
        this.setState({
            isLoading: false,
            formInfo: {
                display: true,
                title: 'Chú ý',
                content: message,
            }
        })
    }

    callToGetAllReport = (callBack) => {
        Axios.get(USER_SERVICE_DOMAIN + API_ADMIN_GET_ALL_OPEN_REPORT, {
            headers: { Authorization: `Bearer ${localStorage.getItem('sot-token')}` }
        }).then(res => {
            if (res.data.code === 200) {
                callBack(res.data.data);
            } else {
                console.log(res);
            }
        }).catch(error => {
            console.log(error);
        })
    }

    udpateInfoListReport = (data) => {
        this.setState({
            fetchingReport: false,
            lastPage: data.last_page,
            listReport: data.data
        })
    }

    switchPage = (page) => {
        this.setState({
            currentPage: page
        })
    }

    hideForm = () => {
        this.setState({
            formInfo: {
                display: false,
                title: '',
                content: '',
            }
        })
    }
}

export default AdminOpenReport;