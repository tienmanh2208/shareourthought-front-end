import React from "react";

class Pagination extends React.Component {
    render() {
        let start = 0;
        let end = 0;
        if (this.props.max <= 5) {
            start = 1;
            end = this.props.max;
        } else if (this.props.max <= this.props.index + 2) {
            start = this.props.max - 4;
            end = this.props.max;
        } else if (this.props.index <= 2) {
            start = 1;
            end = 5;
        } else {
            start = this.props.index - 2;
            end = this.props.index + 2;
        }

        let listPage = [];

        for (let i = start; i <= end; ++i) {
            if (i === this.props.index) {
                listPage.push(<div onClick={this.props.switchPage.bind(this, i)} className={'tm-pagination-page-active'}>{i}</div>);
            } else {
                listPage.push(<div onClick={this.props.switchPage.bind(this, i)} className={'tm-pagination-page'}>{i}</div>);
            }
        }

        return (
            <div className={'tm-pagination-frame'}>
                <div onClick={this.props.switchPage.bind(this, 1)} className={'tm-pagination-page'}><i class="fas fa-angle-double-left"></i></div>
                {listPage}
                <div onClick={this.props.switchPage.bind(this, this.props.max)} className={'tm-pagination-page'}><i class="fas fa-angle-double-right"></i></div>
            </div>
        );
    }
}

export default Pagination;