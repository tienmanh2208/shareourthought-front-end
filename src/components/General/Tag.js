import React from "react";

class Tag extends React.Component {
    render() {
        return (
            <div className={'tm-tag-frame'}>
                {this.props.displayDelete ? <i onClick={this.props.actionDelete.bind(this, this.props.name)} className="fas fa-minus tm-tag-icon"/> : ''}
                <div className={'tm-tag-name'}>{this.props.name}</div>
            </div>
        );
    }
}

export default Tag;