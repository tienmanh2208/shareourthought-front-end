import React from 'react';
import ReactLoading from 'react-loading';

class FormLoading extends React.Component {
    render() {
        return (
            <div className={'tm-form-container'}>
                <div style={{margin: 'auto'}}>
                    <ReactLoading type={'spin'} color={'white'} height={100} width={100} />
                </div>
            </div>
        )
    }
}

export default FormLoading;