import React from 'react';
import ReactLoading from 'react-loading';

class FormLoadingSmall extends React.Component {
    render() {
        return(
            <div className = { 'tm-sg-loading-frame'} style = { this.props.style ? this.props.style : {} } >
                <ReactLoading type={'spin'} color={'gray'} height={30} width={30} />
            </div>
        );
    }
}

export default FormLoadingSmall;
