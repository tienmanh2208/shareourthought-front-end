import React from 'react';
import { BASE_URL_IMAGE } from '../../constants';

class MyAnswer extends React.Component {
    render() {
        return (
            <div key={this.props.keyIndex} className={'tm-myanswer-frame'}>
                <div className={'tm-myanswer-question-frame'}>
                    <div className={'tm-myanswer-question-img-frame'}>
                        <img className={'tm-ma-img-avatar'} src={BASE_URL_IMAGE + '/background/avatar' + (this.props.keyIndex % 5 + 1) + '.jpg'} alt={'Avatar'} />
                        <img className={'tm-ma-img-subject'} src={BASE_URL_IMAGE + '/icon/gdp.svg'} alt={'Mon hoc'} />
                    </div>
                    <div className={'tm-myanswer-question-content-frame'}>
                        <div className={'tm-ma-question-title'}>{this.props.data.question.title}</div>
                        <div className={'tm-ma-question-content'}>{this.props.data.question.content}</div>
                        <div className={'tm-ma-question-info'}>
                            By: <span className={'tm-ma-question-detail-info'}>{this.props.data.question.userName}</span>
                            Posted date: <span className={'tm-ma-question-detail-info'}>{this.props.data.question.postedDate}</span>
                            Upvote: <span className={'tm-ma-question-detail-info'}>{this.props.data.question.upvote}</span>
                        </div>
                    </div>
                </div>
                <div className={'tm-myanswer-answer-frame'}>
                    <div className={'tm-ma-answer-content'}>
                        {this.props.data.answer.content}
                    </div>
                    <div className={'tm-ma-answer-info'}>
                        Status: <span className={'tm-ma-question-detail-info'}>{this.props.data.answer.status === StatusAnswer.open ? 'Open'
                            : this.props.data.answer.status === StatusAnswer.accepted ? 'Accepted'
                                : 'Rejected'}</span>
                        View by quetioner: <span className={'tm-ma-question-detail-info'}>{this.props.data.answer.viewed === 1 ? 'viewed' : 'no'}</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default MyAnswer;

const StatusAnswer = {
    open: 1,
    accepted: 2,
    rejected: 3
};