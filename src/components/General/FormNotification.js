import React from 'react';

class FormNotification extends React.Component {
    render() {
        let style = {
            margin: 'auto',
            padding: '20px',
            backgroundColor: 'white',
            borderRadius: '10px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center'
        };

        let styleDeco = {
            width: '100%',
            borderStyle: 'solid',
            borderRadius: '2px',
            borderWidth: '1px',
            borderColor: 'green',
        };

        let styleTitle = {
            fontSize: '20px',
            lineHeight: '35px',
            margin: '10px',
            textAlign: 'center'
        }

        let styleContent = {
            fontSize: '16px',
            margin: '10px',
            textAlign: 'center'
        }

        return (
            <div className={'tm-form-container'}>
                <div style={style}>
                    <div style={styleTitle}>{this.props.title}</div>
                    <div style={styleDeco} />
                    <div style={styleContent}>{this.props.content}</div>
                    <button onClick={this.props.action} className={'tm-btn tm-btn-warning'}>Close</button>
                </div>
            </div>
        )
    }
}

export default FormNotification;