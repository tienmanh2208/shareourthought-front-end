import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import Auth from "./components/Authenticate/auth";
import MainPage from "./components/Dashboard/MainPage";
import { connect } from "react-redux";
import Personal from './components/Personal/Personal';
import AdminMainFrame from './components/Admin/AdminMainFrame';
import GlobalPage from './components/Global/GlobalPage';

class WebApp extends React.Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route path={'/login'}>
                        <Auth type={'login'} />
                    </Route>
                    <Route path={'/register'}>
                        <Auth type={'register'} />
                    </Route>
                    <Route path={'/dashboard'}>
                        {/* {this.props.authenticated ? <MainPage type={'dashboard'} /> : <Redirect to={'/login'} />} */}
                        {localStorage.getItem('sot-token') !== null ? <MainPage type={'dashboard'} /> : <Redirect to={'/login'} />}
                    </Route>
                    <Route path={'/question'}>
                        {/* {this.props.authenticated ? <MainPage type={'question'} /> : <Redirect to={'/login'} />} */}
                        <MainPage type={'question'} />
                    </Route>
                    <Route path={'/question-detail/:questionId'}>
                        {/* {this.props.authenticated ? <MainPage type={'detail'} /> : <Redirect to={'/login'} />} */}
                        <MainPage type={'detail'} />
                    </Route>
                    <Route path={'/create-group'}>
                        {/* {this.props.authenticated ? <MainPage type={'create-group'} /> : <Redirect to={'/login'} />} */}
                        {localStorage.getItem('sot-token') !== null ? <MainPage type={'create-group'} /> : <Redirect to={'/login'} />}
                    </Route>
                    <Route path={'/group-dashboard/:groupId'}>
                        {/* {this.props.authenticated ? <MainPage type={'group-dashboard'} /> : <Redirect to={'/login'} />} */}
                        {localStorage.getItem('sot-token') !== null ? <MainPage type={'group-dashboard'} /> : <Redirect to={'/login'} />}
                    </Route>
                    <Route path={'/group-question/:groupId/:questionId'}>
                        <MainPage type={'group-question'} />
                    </Route>
                    <Route path={'/personal/:categoryName'}>
                        {/* {this.props.authenticated ? <Personal /> : <Redirect to={'/login'} />} */}
                        {localStorage.getItem('sot-token') !== null ? <Personal /> : <Redirect to={'/login'} />}
                    </Route>
                    <Route path={'/dashboard-search'}>
                        <MainPage type={'search'} />
                    </Route>
                    <Route path={'/global/:globalPage'}>
                        <GlobalPage />
                    </Route>
                    <Route path={'/admin'}>
                        <AdminMainFrame />
                    </Route>
                    <Route path={'/'}>
                        {localStorage.getItem('sot-token') !== null ? <Redirect to={'dashboard'} /> : <Redirect to={'/global/dashboard'} />}
                    </Route>
                </Switch>
            </Router>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        authenticated: state.authenticated
    }
};

const mapDispatchToProps = (dispatch) => {
    return {};
};

const App = connect(mapStateToProps, mapDispatchToProps)(WebApp);

export default App;
