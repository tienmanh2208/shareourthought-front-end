export const BASE_URL_IMAGE = 'http://localhost:3000/shareourthought';
// export const BASE_URL_IMAGE = '/shareourthought';
// export const BASE_URL_IMAGE = '.';

export const USER_SERVICE_DOMAIN = 'https://sot-user-service.herokuapp.com/';
// export const USER_SERVICE_DOMAIN = 'localhost:81';
export const GROUP_PRIVACY = {
    private: 3,
    protected: 2,
    public: 1,
};

export const PERSONAL_CATEGORIES = {
    dashboard: 'dashboard',
    coinManagement: 'coin-management',
    questionAndAnswer: 'question-and-answer',
    editInfo: 'edit',
    groupManagement: 'group-management'
};

export const ACTION_TRANSACTION_USER = {
    receiving: 2,
    spending: 1
}

export const QUESTION_VISIBILITY = {
    OPEN: 1,
    ONLY_ME: 0
}

export const QUESITON_STATUS = {
    OPEN: 1,
    RESOLVED: 2,
    CANCEL: 3,
    DELETED: 4,
}

export const ANSWER_STATUS = {
    WAITING: 1,
    CONSIDERING: 2,
    ACCEPTED: 3,
    REJECTED: 4
}

export const USER_ROLE = {
    QUESTIONER: 1,
    ANSWERER: 2,
    VIEWER: 3
}

export const ANSWER_REPORT_STATUS = {
    NOT_BE_REPORTED: 0,
    REPORTED: 1,
    RESOLVED_BY_ADMIN: 2
}

export const QUESTION_COMPONENT_TYPE = {
    NORMAL: 1,
    GROUP: 2
}

export const CONFIRM_ANSWER_ACTION = {
    BY_QUESTIONER: 1,
    BY_ADMIN: 2
}

export const API_LOG_IN = 'api/login';
export const API_SIGN_UP = 'api/register';
export const API_GET_BASIC_INFO = 'api/users/basic-info';
export const API_GET_TOP_USERS = 'api/global/top-users';
export const API_GET_TOP_FIELDS = 'api/global/top-fields';
export const API_GET_LIST_GROUPS_OF_CURRENT_USER = 'api/users/groups';
export const API_CREATE_GROUP = 'api/groups/create-group';
export const API_CREATE_QUESTION = 'api/users/question';
export const API_GET_NEWEST_QUESTIONS = 'api/global/newest-questions';
export const API_GET_ALL_QUESTION_OF_USER = 'api/users/questions';
export const API_BUY_COIN = 'api/users/buy-coin';
export const API_GET_COIN_INFO = 'api/users/coin-info';
export const API_GET_QUESTION_DETAIL = 'api/users/question/';
export const API_GET_LIST_ANSWER_OF_QUESTION = 'api/users/answers/';
export const API_CREATE_ANSWER = 'api/users/add-comment';
export const API_GET_ANSWER_DETAIL = 'api/users/answer/';
export const API_UPDATE_STATUS_ANSWER = 'api/users/answer/update-status';
export const API_USER_GET_LIST_FIELD = 'api/users/fields';
export const API_USER_REPORT_REJECTED_ANSWER = '/api/users/answer/report-rejected-answer';
export const API_USER_GET_DETAIL_INFO = '/api/users/all-info';
export const API_USER_QUESTION_UPDATE_FIELD = '/api/users/question/update-field';
export const API_USER_QUESTION_UPDATE_VISIBILITY = '/api/users/question/updade-visibility';
export const API_USER_QUESTION_UPDATE_COIN = '/api/users/question/update-price';
export const API_USER_QUESTION_UPDATE_TAGS = '/api/users/question/update-tags';
export const API_USER_QUESTION_UPDATE_CONTENT = '/api/users/question/update-content';
export const API_USER_HISTORY_CONFIRM_ANSWER = '/api/users/history-confirm-answer';
export const API_USER_GET_WARNING = '/api/users/reports';
export const API_USER_COMPENSATE = '/api/users/compensate';
export const API_GROUP_GET_NEWEST_MEMBERS = 'api/groups/newest-member';
export const API_GROUP_GET_GROUP_DETAIL_INFO = 'api/groups/info';
export const API_GROUP_GET_LIST_SECTIONS = 'api/groups/index-sections';
export const API_GROUP_GET_LIST_QUESTION = 'api/groups/questions/';
export const API_GROUP_CREATE_QUESTION = 'api/groups/question';
export const API_GROUP_GET_INFO_BY_INVITED_KEY = 'api/groups/by-invited-key';
export const API_GROUP_JOIN_BY_INVITED_KEY = '/api/groups/join-group';
export const API_GROUP_UPDATE_STATUS_ANSWER = '/api/groups/answer/update-status';
export const API_ADMIN_ACCEPT_REPORT_TO_RESOLVE = '/api/admin/accept-report';
export const API_ADMIN_GET_ALL_OPEN_REPORT = '/api/admin/open-report';
export const API_ADMIN_GET_INFO_ADMIN = '/api/admin/info';
export const API_ADMIN_GET_ACCEPTED_REPORT = '/api/admin/reports';
export const API_ADMIN_CONFIRM_REPORT = '/api/admin/confirm-report';
export const API_GLOBAL_GET_LIST_FIELDS = '/api/global/fields';
export const API_GLOBAL_FIND_QUESTION = '/api/global/find-question';