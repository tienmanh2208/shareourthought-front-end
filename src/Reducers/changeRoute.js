const CHANGE_ROUTE = 'change_route';

function changeRoute(state = '/login', action) {
    switch (action.type) {
        case CHANGE_ROUTE:
            return action.nextRoute;
        default:
            return '/login';
    }
}

export default changeRoute;