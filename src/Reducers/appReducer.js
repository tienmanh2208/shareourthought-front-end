import {combineReducers} from "redux";
import authenticated from "./authenticated";
import changeRoute from "./changeRoute";

const appReducer = combineReducers({
    authenticated: authenticated,
    changeRoute: changeRoute,
});

export default appReducer;