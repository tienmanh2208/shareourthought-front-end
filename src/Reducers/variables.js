export const PAGE_NAME = {
    login: 'login',
    register: 'register',
    dashboard: 'dashboard',
    question: 'question',
    questionDetail: 'questionDetail',
};